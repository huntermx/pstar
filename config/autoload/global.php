<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */

return array(
    // ...
	'db' => array(
				'driver'         => 'Pdo',
				'dsn'            => 'mysql:dbname=focus2000;host=10.20.0.13',
				'driver_options' => array(
				 PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
				),
	),
	


	   'navigation' => array(
        // The DefaultNavigationFactory we configured in (1) uses 'default' as the sitemap key
        'default' => array(
            // And finally, here is where we define our page hierarchy
            //jobhistory
            
            'Inicio' => array(
                'label' => 'Capacity',
                'route' => 'application',
            ),

            'metricas' => array(
                'label' => 'Ps Metrics',
                'route' => 'psmetrics',
            ),

            'monthmet' => array(
                'label' => 'Monthly Metrics',
                'route' => 'monthmetric',
            ),

            'availability' => array(
                'label' => 'Availability',
                'route' => 'availability',
            ),

            'efficiency' => array(
                'label' => 'Efficiency',
                'route' => 'efficiency',
            ),
            
            'utilization' => array(
                'label' => 'Utilization',
                'route' => 'utilization',
            ),
            
            'downtime' => array(
                'label' => 'DownTime ',
                'route' => 'downtime',
            ),

            'dashboards' => array(
                'label' => 'Dashboard',
                'route' => 'dashboards',
            ),
 
        ),
    ),
		
'service_manager' => array(
                'factories' => array(
                        'Zend\Db\Adapter\Adapter'
                        => 'Zend\Db\Adapter\AdapterServiceFactory',
                ),
    ),

	'dbconfigkey2' => array(
    	'driver'         => 'Pdo',
    	'dsn'            => 'mysql:dbname=hunterdb2;host=localhost',
    	'driver_options' => array(
        	PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
    ),
    'username' => 'root',
    'password' => 'hunter',
),



);

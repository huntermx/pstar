<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\MyAdapterFactory;


use Application\Model\Dao\MoldingCapacityDao;
use Application\Model\Dao\MoldeoEficienciaDao;
use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Dao\MachUsageDTotalDao;
use Application\Model\Dao\MoldeoEficienciaHCMDao;
use Application\Model\Dao\MoldeoEficienciaPropDao;
use Application\Model\Dao\MachUsageDailyPropDao;
use Application\Model\Dao\HcmMachinesDao;
use Application\Model\Dao\MachineDailyLog;


use Application\Model\Metrics\UpdateMachUsage;
use Application\Model\PStarDb\PlantstarData;


use Application\Model\Entity\MoldingCapacity;
use Application\Model\Entity\MoldeoEficiencia;
use Application\Model\Entity\MachUsageDaily;
use Application\Model\Entity\MachUsageDTotal;
use Application\Model\Entity\HcmMachines;
use Application\Model\Entity\MachDLog;



class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


 public function initConfig($e)
    {
        $application =  $e->getApplication();
        $services    =  $application->getServiceManager();
        
        $services->setFactory('ConfigIni', function($services){
            $reader = new Ini();
            $data   = $reader->fromFile(__DIR__ . '/config/config.ini');
            return $data;
        });
    } 

    public function getServiceConfig()
    {
        return array(
                'factories' => array(
                'myadapter2'        => new MyAdapterFactory('dbconfigkey2'),
                
                'UpdateMachUsage' => function($sm) {
                        $instance = new UpdateMachUsage($sm);
                 return $instance;
                 },
                'Application\Model\PStarDb\PlantstarData' =>  function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $tableGateway = $sm->get('PlantstarDataTableGateway');
                    $table = new PlantstarData($tableGateway,$dbAdapter);
                    return $table;
                },
                'PlantstarDataTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MoldingCapacity());
                    return new TableGateway('mach_def', $dbAdapter, null, $resultSetPrototype);
                },

                'Application\Model\Dao\MoldingCapacityDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MoldingCapacityDaoGateway');
                    $table = new MoldingCapacityDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MoldingCapacityDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MoldingCapacity());
                    return new TableGateway('molding_capacity', $dbAdapter, null, $resultSetPrototype);
                },

                'Application\Model\Dao\MoldeoEficienciaDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MoldeoEficienciaDaoGateway');
                    $table = new MoldeoEficienciaDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MoldeoEficienciaDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MoldeoEficiencia());
                    return new TableGateway('moldeo_eficiencia', $dbAdapter, null, $resultSetPrototype);
                },

                'Application\Model\Dao\MoldeoEficienciaHCMDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MoldeoEficienciaHCMDaoGateway');
                    $table = new MoldeoEficienciaHCMDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MoldeoEficienciaHCMDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MoldeoEficiencia());
                    return new TableGateway('moldeo_eficiencia_hcm', $dbAdapter, null, $resultSetPrototype);
                },

                'Application\Model\Dao\MoldeoEficienciaPropDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MoldeoEficienciaPropDaoGateway');
                    $table = new MoldeoEficienciaPropDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MoldeoEficienciaPropDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MoldeoEficiencia());
                    return new TableGateway('moldeo_eficiencia_prop', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\Dao\HcmMachinesDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('HcmMachinesDaoGateway');
                    $table = new HcmMachinesDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'HcmMachinesDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new HcmMachines());
                    return new TableGateway('hcm_machines', $dbAdapter, null, $resultSetPrototype);
                },


                 'Application\Model\Dao\MachUsageDailyDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MachUsageDailyDaoGateway');
                    $table = new MachUsageDailyDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MachUsageDailyDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MachUsageDaily());
                    return new TableGateway('log_mach_day', $dbAdapter, null, $resultSetPrototype);
                },
                'Application\Model\Dao\MachUsageDTotalDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MachUsageDTotalDaoGateway');
                    $table = new MachUsageDTotalDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MachUsageDTotalDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MachUsageDTotal());
                    return new TableGateway('machine_usage_daily', $dbAdapter, null, $resultSetPrototype);
                },
                
                'Application\Model\Dao\MachUsageDailyPropDao' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MachUsageDailyPropDaoGateway');
                    $table = new MachUsageDailyPropDao($tableGateway,$dbAdapter);
                    return $table;
                },
                'MachUsageDailyPropDaoGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MachUsageDTotal());
                    return new TableGateway('machine_usage_daily_prop', $dbAdapter, null, $resultSetPrototype);
                },

                 'Application\Model\Dao\MachineDailyLog' =>  function($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $tableGateway = $sm->get('MachineDailyLogGateway');
                    $table = new MachineDailyLog($tableGateway,$dbAdapter);
                    return $table;
                },
                'MachineDailyLogGateway' => function ($sm) {
                    $dbAdapter = $sm->get('myadapter2');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MachDLog());
                    return new TableGateway('machine_daily_log', $dbAdapter, null, $resultSetPrototype);
                },

                )
                );
    }


}

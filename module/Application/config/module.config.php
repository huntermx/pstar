<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Index',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),

        'metrics' => array(
            'type'    => 'segment',
            'options' => array(
            'route'    => '[/:lang]/metrics[/:action][/:id]',
            'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'Application\Controller\Psmetrics',
                'action'     => 'index',
                ),
        ),
        ),  

        'psmetrics' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/psmetrics',
                'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                    'controller' => 'Psmetrics',
                    'action' => 'index',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/psmetrics[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),


    'hcmmachines' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/hcmmachines',
                'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                    'controller' => 'Hcmmachines',
                    'action' => 'index',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/hcmmachines[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                    ),
                    ),
                ),
            ),
     ),


     'hcmmachine' => array(
            'type'    => 'segment',
            'options' => array(
            'route'    => '[/:lang]/hcmmachine[/:action][/:id]',
            'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                    'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'Application\Controller\Hcmmachines',
                'action'     => 'index',
                ),
        ),
        ),  

    'monthjquery' => array(
        'type' => 'Segment',
        'options' => array(
                'route' => '/monthmetric[/:action][/:id]',
                'defaults' => array(
                        'controller' => 'Application\Controller\Monthmetrics',
                        'action' => 'index',
                    ),
            ),

        ),



    'monthmetric' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/monthmetric',
                'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                    'controller' => 'Monthmetrics',
                    'action' => 'index',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/monthmetric[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),

 'monthmet' => array(
         'type'    => 'segment',
         'options' => array(
         'route'    => '[/:lang]/monthmet[/:action][/:id]',
         'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                ),
                'defaults' => array(
                'controller' => 'Application\Controller\Monthmetrics',
                'action'     => 'index',
                ),
        ),
        ),  


       


        'machusage' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/machusage',
                'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                    'controller' => 'Machineusage',
                    'action' => 'index',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/machusage[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),



'machineusage' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/machineusage',
            'defaults' => array(
                '__NAMESPACE__' => 'Application\Controller',
                'controller' => 'Machineusage',
                'action' => 'Index',
                ),
                ),
        'may_terminate' => true,
            'child_routes' => array(
            'default' => array(
                'type' => 'Segment',
                    'options' => array(
                'route'    => '[/:lang]/machineusage[/:action][/:id]',
                'constraints' => array(
                    'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'id'     => '[a-zA-Z0-9][a-zA-Z0-9_-]*',
                    ),
                'defaults' => array(
                ),
                ),
      ),
     ),
     ), 


        'capacity' => array(
            'type'    => 'segment',
            'options' => array(
            'route'    => '[/:lang]/capacity[/:action][/:id]',
            'constraints' => array(
                'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                'id'     => '[0-9]+',
                ),
                'defaults' => array(
                'controller' => 'Application\Controller\Index',
                'action'     => 'editcapacity',
                ),
        ),
        ),  




        ),
    ),




    'service_manager' => array(
         'factories' => array(
           
            'Navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
           
            
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Psmetrics' => 'Application\Controller\PsmetricsController',
            'Application\Controller\Machineusage' => 'Application\Controller\MachineusageController',
            'Application\Controller\Monthmetrics' => 'Application\Controller\MonthmetricsController',
            'Application\Controller\Hcmmachines' => 'Application\Controller\HcmmachinesController',
              
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/new'           => __DIR__ . '/../view/layout/layout_new.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),


    'view_helpers' => array(
        'invokables'=> array( 
            'HourFormatHelper'  => 'Application\View\Helper\HourFormatHelper',
             'FechaHelper'      => 'Application\View\Helper\FechaHelper',
        )
    ),

);

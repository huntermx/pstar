<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;

use Application\Model\Metrics\UpdateMachUsage;

use Application\Forms\FormCapacity;
use Application\Forms\FormHcmMachines;
use Application\Forms\FormMonthlyMetrics;
use Application\Forms\FormMonthlyMetrics2;

use Dashboard\Forms\FormMoldingHCM;
use Dashboard\Forms\FormMoldingHCM2;
use Dashboard\Model\Statics\PercentajeCalculation;

abstract class AbstractApplicationController extends AbstractActionController
{

    protected $moldingCap;
    protected $formCapacity;
    protected $moldEfficDao;
    protected $pstarDao;
    protected $machUsageDao;
    protected $machTotalUsage;
    protected $updateMachUsage;
    protected $moldEfficHCMDao;
    protected $machineUsagePropDao;
    protected $hcmMachinesDao;
    protected $formHcmMachine;
    protected $formHCM;
    protected $formMonthMet;
    protected $formHCM2;
    protected $formMonthMet2;
    protected $machDailyLog;


    public function getHCMForm()
    {
        if (!$this->formHCM) {
            $temp            = new FormMoldingHCM();
            $builder         = new AnnotationBuilder();
            $this->formHCM    = $builder->createForm($temp);
        }
        return $this->formHCM;
    }

    public function getMonthMetForm()
    {
        if (!$this->formMonthMet) {
            $temp            = new FormMonthlyMetrics();
            $builder         = new AnnotationBuilder();
            $this->formMonthMet    = $builder->createForm($temp);
        }
        return $this->formMonthMet;
    }

    public function getHCMForm2()
    {
        if (!$this->formHCM2) {
            $temp            = new FormMoldingHCM2();
            $builder         = new AnnotationBuilder();
            $this->formHCM2    = $builder->createForm($temp);
        }
        return $this->formHCM2;
    }

    public function getMonthMetForm2()
    {
        if (!$this->formMonthMet2) {
            $temp            = new FormMonthlyMetrics2();
            $builder         = new AnnotationBuilder();
            $this->formMonthMet2    = $builder->createForm($temp);
        }
        return $this->formMonthMet2;
    }








    public function getHcmMachineForm()
    {
        if (!$this->formHcmMachine) {
            $temp            = new FormHcmMachines();
            $builder         = new AnnotationBuilder();
            $this->formHcmMachine    = $builder->createForm($temp);
        }
        return $this->formHcmMachine;
    }

    public function getMachineUsagePropDao()
    {
        if (!$this->machineUsagePropDao) {
            $sm = $this->getServiceLocator();
            $this->machineUsagePropDao = $sm->get('Application\Model\Dao\MachUsageDailyPropDao');
        }
       return $this->machineUsagePropDao;
    }

     public function getHcmMachineDao()
    {
        if (!$this->hcmMachinesDao) {
            $sm = $this->getServiceLocator();
            $this->hcmMachinesDao = $sm->get('Application\Model\Dao\HcmMachinesDao');
        }
       return $this->hcmMachinesDao;
    }


    public function getMoldingCapacityDao()
    {
        if (!$this->moldingCap) {
            $sm = $this->getServiceLocator();
            $this->moldingCap = $sm->get('Application\Model\Dao\MoldingCapacityDao');
        }
       return $this->moldingCap;
    }

   public function getCapacityForm()
    {
        if (!$this->formCapacity) {
            $temp            = new FormCapacity();
            $builder         = new AnnotationBuilder();
            $this->formCapacity    = $builder->createForm($temp);
        }
        return $this->formCapacity;
    }

    public function getMoldingEfficenciyDao()
    {
        if (!$this->moldEfficDao) {
            $sm = $this->getServiceLocator();
            $this->moldEfficDao = $sm->get('Application\Model\Dao\MoldeoEficienciaDao');
        }
       return $this->moldEfficDao;
    }

     public function getMoldingEfficenciyHCMDao()
    {
        if (!$this->moldEfficHCMDao) {
            $sm = $this->getServiceLocator();
            $this->moldEfficHCMDao = $sm->get('Application\Model\Dao\MoldeoEficienciaHCMDao');
        }
       return $this->moldEfficHCMDao;
    }

    public function getPstarData()
    {
        if (!$this->pstarDao) {
            $sm = $this->getServiceLocator();
            $this->pstarDao = $sm->get('Application\Model\PStarDb\PlantstarData');
        }
       return $this->pstarDao;
    }

    public function getMachUsageDao()
    {
        if (!$this->machUsageDao) {
            $sm = $this->getServiceLocator();
            $this->machUsageDao = $sm->get('Application\Model\Dao\MachUsageDailyDao');
        }
       return $this->machUsageDao;
    }

    public function getMachTotalUsageDao()
    {
        if (!$this->machTotalUsage) {
            $sm = $this->getServiceLocator();
            $this->machTotalUsage = $sm->get('Application\Model\Dao\MachUsageDTotalDao');
        }
       return $this->machTotalUsage;
    }

  public function getMachDailyLog()
    {
        if (!$this->machDailyLog) {
            $sm = $this->getServiceLocator();
            $this->machDailyLog = $sm->get('Application\Model\Dao\MachineDailyLog');
        }
       return $this->machDailyLog;
    }




   protected function getTjRegistro($weekday,$data,$date)
   {
                $objCount = $this->getMachTotalUsageDao()->getMachNumberByDate($date);
                $mach = $objCount->current();
                $machines = $mach['num'];

                if(empty($machines))
                {
                    $machines = $data['param']['machine']['tj'];
                }

              // print_r($machines);

                if($weekday=="Sun")
                {
                    $data=array("fecha" => $date,
                                "nstatus" => "Holiday",
                                "total_machines" => $machines,
                                "hours" => 0,
                                "total_time" =>  (0*$machines), 
                                );
                }else{

                     $data=array("fecha" => $date,
                                "nstatus" => "Normal",
                                "total_machines" => $machines,
                                "hours" => 24,
                                "total_time" =>  (24*$machines), 
                                );
                }

                return $data;
   }


     protected function getSmRegistro($weekday,$data,$date)
   {
          
                $objCount = $this->getMachTotalUsageDao()->getMachNumberByDate($date);
                $mach = $objCount->current();
                $machines = $mach['num'];
                
                if(empty($machines))
                {
                    $machines = $data['param']['machine']['sm'];
                }

               

                if($weekday=="Sun" || $weekday=="Sat")
                {
                    $data2=array("fecha" => $date,
                                "nstatus" => "Holiday",
                                "total_machines" => $machines,
                                "hours" => 0,
                                "total_time" =>  (0*$machines), );
                }else{

                     $data2=array("fecha" => $date,
                                "nstatus" => "Normal",
                                "total_machines" => $machines,
                                "hours" => 24,
                                "total_time" =>  (24*$machines), );
                }

                return $data2;
   }




    protected function getDownTime($day,$reason)
    {
            $result_setup = $this->getPstarData()->getDownTimeByDatReason($day,$reason);
            $setup_time = 0;
            
            foreach($result_setup as $resetup)
            {
                $setup_time = $setup_time + $resetup['down_time'];

            }

            return $setup_time;
    }


     protected function getVectorSum($sql,$temp_fechas,$orden)
    {
        $total_capacity     = 0;
        $total_sched_run    = 0;
        $total_run          = 0;

        
        $efficiency         = 0;
        $utilization        = 0;

        $down_time          = 0;
        $notTrackDown       = 0;

/*        $setup_time         = 0;
        $mantto_time        = 0;
        $idle_time          = 0;
        $eng_sample_time    = 0;
        $paro_fin_time      = 0;
        $arranque_ini_time      = 0;
        $setup2_time        = 0;*/


       

        while($row=$sql->current())
        {


            $total_capacity     = $total_capacity   + ($row['total_time'] * 3600);
            $total_sched_run    = $total_sched_run   + ($row['total_uptime'] + $row['total_downtime']);
            $total_run          = $total_run        + $row['total_uptime'];
            $down_time          = $down_time        + $row['total_downtime'];
            $notTrackDown       = $notTrackDown     + ($row['mantto_prev_time'] + $row['setup_delay_time'] + $row['eng_sample_time'] + $row['paro_fin_time']+ $row['arranque_ini_time']);


           /* $setup_time         = $setup_time       + $row['setup_time'];
            $mantto_time        = $mantto_time      + $row['mantto_prev_time'];
            $idle_time          = $idle_time        + $row['idle_time'];
            $eng_sample_time    = $eng_sample_time  + $row['eng_sample_time'];
            $paro_fin_time      = $paro_fin_time    + $row['paro_fin_time'];
            $arranque_ini_time  = $arranque_ini_time    + $row['arranque_ini_time'];*/

        }
    
        $objPercentaje  = new PercentajeCalculation(2);

        $objPercentaje->setNumerador($total_run);
        $temp = $down_time - $notTrackDown;
        $objPercentaje->setDenominador(($total_run + $temp));


        $vector = array(
                            'orden' => $orden,
                            'fecha' => $temp_fechas, 
                            'total_capacity' => $total_capacity, 
                            'total_sched_run' => $total_sched_run,
                            'total_run' => $total_run,
                            'total_down_time' => $down_time,
                            'total_not_track_down_time' => $notTrackDown,
                             'efficiency' => $objPercentaje->getPercentaje(),
                );

        return $vector;
    }

     protected function getHCMVectorSum($sql,$temp_fechas,$orden)
    {
        $total_capacity     = 0;
        $total_sched_run    = 0;
        $total_run          = 0;

        
        $efficiency         = 0;
        $utilization        = 0;

        $down_time          = 0;
        $notTrackDown       = 0;

/*        $setup_time         = 0;
        $mantto_time        = 0;
        $idle_time          = 0;
        $eng_sample_time    = 0;
        $paro_fin_time      = 0;
        $arranque_ini_time      = 0;
        $setup2_time        = 0;*/

        while($row=$sql->current())
        {

            $total_capacity     = $total_capacity   + ($row['hours'] * $row['machines']  *  3600);
            $total_sched_run    = $total_sched_run   + ($row['uptime'] + $row['downtime']);
            $total_run          = $total_run        + $row['uptime'];
            $down_time          = $down_time        + $row['downtime'];
            $notTrackDown       = $notTrackDown     + ($row['mantto_prev'] + $row['setup_delay_time'] + $row['eng_sample_time'] + $row['paro_fin_time']+ $row['arranque_ini_time']);



           /* $setup_time         = $setup_time       + $row['setup_time'];
            $mantto_time        = $mantto_time      + $row['mantto_prev_time'];
            $idle_time          = $idle_time        + $row['idle_time'];
            $eng_sample_time    = $eng_sample_time  + $row['eng_sample_time'];
            $paro_fin_time      = $paro_fin_time    + $row['paro_fin_time'];
            $arranque_ini_time  = $arranque_ini_time    + $row['arranque_ini_time'];*/

        }
        $objPercentaje  = new PercentajeCalculation(2);

        $objPercentaje->setNumerador($total_run);
        $temp = $down_time - $notTrackDown;
        $objPercentaje->setDenominador(($total_run + $temp));


        $vector = array(
                            'orden' => $orden,
                            'fecha' => $temp_fechas, 
                            'total_capacity' => $total_capacity, 
                            'total_sched_run' => $total_sched_run,
                            'total_run' => $total_run,
                            'total_down_time' => $down_time,
                            'total_not_track_down_time' => $notTrackDown,
                             'efficiency' => $objPercentaje->getPercentaje(),
                );

        return $vector;
    }


}
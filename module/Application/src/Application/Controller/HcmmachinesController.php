<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Application\Model\TimeClass\ClassDateOp;
use Application\Model\Entity\HcmMachines;



class HcmmachinesController extends AbstractApplicationController
{

    public function indexAction()
    {
    	$arrayme = $this->getHcmMachineDao()->fetchAll();
        $temp = array();

        foreach($arrayme as $me)
        {
            $temp[]= array(
            	'id'	=> $me->id,
                'mach_name' => $me->mach_name,
                'fecha_track' => $me->fecha_track,
                'status' => $me->status,
            );
        }
        return new ViewModel(array(
                'moldingCap' => $temp,
                'titulo' => 'Hcm Machines',
            )); 
    }


    public function AddnewhcmmachineAction()
    {
    	$form       = $this->getHcmMachineForm();
    	$request 	 = $this->getRequest();

    	if($request->isPost())
        {
        	$hcmMachines = new HcmMachines();
            $form->setData($request->getPost());
            
            if($form->isValid())
            {
            	$hcmMachines->exchangeArray($form->getData());
            	$this->getHcmMachineDao()->agregar($hcmMachines);
            	return $this->redirect()->toRoute('hcmmachine',array(
                          'action' =>  'index'));
            }
        }
        else
        {
        	return array(
    	 		      'titulo' => 'Add New HCM Machine',
    	 		      'form' => $form,
    	 	     );
        }
    }


    public function EdithcmmachineAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id || $id == 0) 
        {
        	return $this->redirect()->toRoute('hcmmachine', array('action' => 'index'));
      	}

      	$hcmMachine = $this->getHcmMachineDao()->getRegistro($id);
      	$form  = $this->getHcmMachineForm();
        $form->bind($hcmMachine);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();

        if($request->isPost()) 
      	{
            $form->setData($request->getPost());

            if($form->isValid())
            {
            	$this->getHcmMachineDao()->agregar($form->getData());
            	return $this->redirect()->toRoute('hcmmachine',array(
                          'action' =>  'index'));

            }
        }
         return array(
          'id' => $id,
          'form' => $form,
      );
    }

    public function DeletehcmmachineAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
      	if (!$id) {
        	return $this->redirect()->toRoute('hcmmachine', array('action' => 'index'));
      	}
      
    	$request = $this->getRequest();
      
      	if($request->isPost()) 
      	{
        	$del = $request->getPost('del', 'No');
      
            if ($del == 'Yes') 
            {
                $partes = $this->getHcmMachineDao()->getRegistro($id);
               // $id = (int) $request->getPost('id');
                $this->getHcmMachineDao()->eliminar($id);
              
            }
      
       		return $this->redirect()->toRoute('hcmmachine',array(
                          'action' =>  'index'));
      	}
       
      return array(
          'id'    => $id,
          'parte' => $this->getHcmMachineDao()->getRegistro($id)
      );
      
    }

}

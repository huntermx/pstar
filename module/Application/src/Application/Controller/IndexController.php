<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Application\Model\TimeClass\ClassDateOp;

use Application\Model\Entity\MoldingCapacity;

class IndexController extends AbstractApplicationController
{

    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('capacity', array( 'action'=> 'index'));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('capacity', array( 'action'=> 'Hcmcapacity'));
                }
                return $this->redirect()->toRoute('capacity', array( 'action'=> 'Propcapacity'));
            }
        }
    }

 
    public function indexAction()
    {
        $form          = $this->getHCMForm();


         return new ViewModel(array(
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'moldingCap' => $this->getMoldingCapacityDao()->fetchAll(),
                'titulo' => 'Daily Total Capacity',
              
            )); 
    }


    public function HcmcapacityAction()
    {
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

         return new ViewModel(array(
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'moldingCap' => $this->getHcmCapacityArray($this->getMoldingCapacityDao()->fetchAll()),
                'titulo' => 'Daily Total HCM Capacity',
              
            )); 
    }


    public function PropcapacityAction()
    {
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

         return new ViewModel(array(
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'moldingCap' => $this->getPropCapacityArray($this->getMoldingCapacityDao()->fetchAll()),
                'titulo' => 'Daily Total Proprietary Capacity',
              
            )); 
    }


    private function getPropCapacityArray($queryObj)
    {
        $tempArray = array();
        foreach($queryObj as $molding)
        {
            $queryPDO = $this->getHcmMachineDao()->getNumberHcmMachinesByDate($molding->fecha);
            $totalHcm = $queryPDO->current();
            $totalMachines = $molding->total_machines -  $totalHcm['rows'];
            $total_time = $totalMachines * $molding->hours;
            $tempArray[] = array(
                            'fecha' => $molding->fecha,
                            'nstatus' => $molding->nstatus,
                            'total_machines' => $totalMachines,
                            'hours' => $molding->hours,
                            'total_time' => $total_time,
                );
        }

        return $tempArray;
    }


    private function getHcmCapacityArray($queryObj)
    {
        $tempArray = array();
        foreach($queryObj as $molding)
        {
            $queryPDO = $this->getHcmMachineDao()->getNumberHcmMachinesByDate($molding->fecha);
            $totalHcm = $queryPDO->current();
            $total_time = $totalHcm['rows'] * $molding->hours;
            $tempArray[] = array(
                            'fecha' => $molding->fecha,
                            'nstatus' => $molding->nstatus,
                            'total_machines' => $totalHcm['rows'],
                            'hours' => $molding->hours,
                            'total_time' => $total_time,
                );
        }

        return $tempArray;
    }



    public function editcapacityAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id || $id == 0) {
                return $this->redirect()->toRoute('application', array(
                'action' => 'index'));
        }
        $form           = $this->getCapacityForm();
        $capacity       = $this->getMoldingCapacityDao()->getRegistro($id);
        $request        = $this->getRequest();
        $form->bind($capacity);


        if($request->isPost())
        {

            $form->setData($request->getPost());
            if($form->isValid())
            {

                $this->getMoldingCapacityDao()->agregar($form->getData());
                return $this->redirect()->toRoute('application',array(
                            'action' =>  'index'));
            }
        }

    	return new ViewModel(array(
                'editday' => $capacity->fecha,
                'form' => $form,
                'cap_id' => $id,
                'titulo' => 'Edit Total Capacity',
              
            ));
	}


    public function updateRegAction()
    {
        // Inicializar objeto y revisar el dia de hoy

        $today = date("Y-m-d");
        $objDate = new ClassDateOp($today);
        $i = 0;

        $reader = new Ini();
        $ipserver               =   $_SERVER['HTTP_HOST'];
        $iniFile = "/var/www/pstar/module/Application/config/config.ini";

        if(strcmp($ipserver,"10.40.8.40")==0)
        {
             $iniFile = "/var/www/html/pstar/module/Application/config/config.ini";
        }



        $data   = $reader->fromFile($iniFile);

        do{
            $objDate->getPrevDay();
            $dateObj= $objDate->getShowDate("Y-m-d");
            $i++;

            if($this->getMoldingCapacityDao()->checkRegistroByDate($dateObj)==false)
            {
                $weekday = $objDate->getWeekDay();
            
                if($data['param']['location']== "TJ")
                {
                    $CapacityArray = $this-> getTjRegistro($weekday,$data,$dateObj);
                }else{
                    $CapacityArray = $this-> getSmRegistro($weekday,$data,$dateObj);
                }

                $molding = new MoldingCapacity();
                $molding->exchangeArray($CapacityArray);
                $this->getMoldingCapacityDao()->agregar($molding);
            }

        }while($dateObj> "2012-03-29");


         return $this->redirect()->toRoute('application',array(
                 'action' =>  'index'));

    }


}
 

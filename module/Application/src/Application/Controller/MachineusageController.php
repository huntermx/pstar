<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Application\Model\TimeClass\ClassDateOp;

use Application\Model\Entity\MoldingCapacity;
use Application\Model\Entity\MoldeoEficiencia;

use Application\Model\Metrics\UpdateMetrics;

class MachineusageController extends AbstractApplicationController
{


    public function indexAction()
    {
        $fecha = (string)$this->params()->fromRoute('id');
        
       // $test= $this->getMachTotalUsageDao()->getRegByDate($fecha);


        return new ViewModel(array(
                'machine' => $this->getMachTotalUsageDao()->getRegByDate($fecha),
                'titulo' => 'Day Machine Usage',
                'date'  => $fecha,
              
            )); 
    }


    public function HcmmachinemetricsAction()
    {
         $fecha = (string)$this->params()->fromRoute('id');
        
          return new ViewModel(array(
                'machine' => $this->getMachTotalUsageDao()->getHcmMachinesByDate($fecha),
                'titulo' => 'Day HCM Machine Usage',
                'date'  => $fecha,
              
            )); 

    }

      public function PropmachinemetricsAction()
    {
         $fecha = (string)$this->params()->fromRoute('id');
        
          return new ViewModel(array(
                'machine' => $this->getMachTotalUsageDao()->getPropMachinesByDate($fecha),
                'titulo' => 'Day Proprietary Machine Usage',
                'date'  => $fecha,
              
            )); 

    }

    
}

<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Application\Model\TimeClass\ClassDateOp;

use Application\Model\Entity\MoldingCapacity;
use Application\Model\Entity\MoldeoEficiencia;

use Application\Model\Metrics\UpdateMetrics;

use Application\Model\TimeClass\MonthToDisplay;
use Application\Model\TimeClass\DisplayMenu;
use Application\Model\TimeClass\SemanasByMonth;
use Application\Model\TimeClass\MonthPickerLastDates;

use Application\View\Helper\ClassFecha;
use Dashboard\Model\Statics\PercentajeCalculation;


class MonthmetricsController extends AbstractApplicationController
{

    public function indexAction()
    {
        $fecha = (string)$this->params()->fromRoute('id');
        $request    = $this->getRequest();
        $form_selector = $this->getMonthMetForm2();
        if($request->isPost())
        {
            $post = $request->getPost();
            $form_selector->setData($post);

            if($form_selector->isValid())
            {
                 //$post = $request->getPost();
            }
        }

        if(empty($fecha)){
            $fecha = (string)@$post['fecha_graph'];
        }
    
        if($fecha ==0)
        {
            $year = date('Y');
            $month = date('m');
        }else{
            list($year,$month) = explode("-",$fecha);
        }
        $semanas = new SemanasByMonth($year,$month);
        $vector_sem = $semanas->getSemanas();
        $i = 0;
    	
        $month_capacity = 0;
        $month_sched_run = 0;
        $month_run = 0;
        $month_total_downtime = 0;
        $month_nottrack_downtime = 0;

		foreach($vector_sem as $vec)
        {

        //  echo "<pre>";
          //print_r($vec);
            //exit;

            $objeto_sql = $this->getMoldingEfficenciyDao()->getEficienciaBySemanas($vec['start'],$vec['end'],0);
            $objFechaIni = new ClassFecha($vec['start'],false," - ");
            $objFechaIni->setFormatoMDA();
            $objFechaIni->setTimeFormatAMPM();

            $objFechaFin = new ClassFecha($vec['end'],false," - ");
            $objFechaFin->setFormatoMDA();
            $objFechaFin->setTimeFormatAMPM();


            $temp_fechas = $objFechaIni->getFecha() . '  / ' . $objFechaFin->getFecha();
            $temp_array[$i]= $this->getVectorSum($objeto_sql,$temp_fechas,($i+1));

            $month_capacity = $month_capacity+$temp_array[$i]['total_capacity'];
            $month_sched_run = $month_sched_run+$temp_array[$i]['total_sched_run'];
            $month_run = $month_run+$temp_array[$i]['total_run'];
            $month_total_downtime = $month_total_downtime+$temp_array[$i]['total_down_time'];
            $month_nottrack_downtime = $month_nottrack_downtime+$temp_array[$i]['total_not_track_down_time'];
           
         //   print_r($temp_array[$i]);
         //   echo '<br><br>';
            $i++;
        }
        //exit;

        $objPercentaje  = new PercentajeCalculation(2);

        $objPercentaje->setNumerador($month_run);
        $temp = $month_total_downtime - $month_nottrack_downtime;
        $objPercentaje->setDenominador(($month_run + $temp));

        $temp_array[$i] = array(
                    'orden' => ($i+1),
                    'fecha' => 'Month Total',
                    'total_capacity'  => $month_capacity,
                    'total_sched_run'  => $month_sched_run,
                    'total_run'  => $month_run,
                    'total_down_time'  => $month_total_downtime,
                    'total_not_track_down_time'  => $month_nottrack_downtime,
                    'efficiency'  => $objPercentaje->getPercentaje(),
            );

      
        $form_selector->get('fecha_graph')->setValue($fecha); 
        $form          = $this->getHCMForm2();
        $this->layout('layout/layout');
        $lastDateArray = $this->getMoldingEfficenciyDao()->getLastDataDate();
        $lastDate = $lastDateArray->current();
        $objPicker = new MonthPickerLastDates($lastDate['fecha']);

     	$view = new ViewModel(array(
                'formSelector' => $form_selector,
                'hcmForm' => $form,
                'titulo' => 'Monthly Metrics',
                'action' => 'Hcmindex',
                'result' => $temp_array,
                'lastYear' =>$objPicker->getYear(),
                'monthArray' => json_encode($objPicker->getDisableMonthsArray()),
        )); 

        $view->setTemplate('application/monthmetrics/index.phtml');
        return $view;
    }


    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm2();
        
        if($request->isPost())
        {
            $post = $request->getPost();
              
            $form->setData($post);
            $temp =$post->fecha_graph;
            if(empty($post->fecha_graph)){
                $temp = null;
            }
            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('monthmet', array( 'action'=> 'index','id'=>$temp));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('monthjquery', array( 
                        'controller' => 'monthmetrics',
                        'action'=> 'hcmmonthmetrics',
                        'id' => $temp,
                        ));
                }
                return $this->redirect()->toRoute('monthmet', array( 'action'=> 'Propmonthmetrics', 'id'=>$temp));
            }
            echo "error";
            exit;
        }
    }



    public function HcmmonthmetricsAction()
    {

        $fecha = (string)$this->params()->fromRoute('id');
        $request    = $this->getRequest();

        $form_selector = $this->getMonthMetForm2();
        if($request->isPost())
        {
            $post = $request->getPost();
            $form_selector->setData($post);

            if($form_selector->isValid())
            {
                 //$post = $request->getPost();
            }
        }

        if(empty($fecha)){
            $fecha = (string)@$post['fecha_graph'];
        }

    
        if(empty($fecha))
        {
            $year = date('Y');
            $month = date('m');
        }else{
            list($year,$month) = explode("-",$fecha);
        }
        $semanas = new SemanasByMonth($year,$month);
        $vector_sem = $semanas->getSemanas();
        $i = 0;
        
        $month_capacity = 0;
        $month_sched_run = 0;
        $month_run = 0;
        $month_total_downtime = 0;
        $month_nottrack_downtime = 0;
       // echo "<pre>";
        foreach($vector_sem as $vec)
        {
            $objeto_sql = $this->getMachTotalUsageDao()->getHCMEficienciaBySemanas($vec['start'],$vec['end'],0);

        //   print_r( $objeto_sql->current());

            $objFechaIni = new ClassFecha($vec['start'],false," - ");
            $objFechaIni->setFormatoMDA();
            $objFechaIni->setTimeFormatAMPM();

            $objFechaFin = new ClassFecha($vec['end'],false," - ");
            $objFechaFin->setFormatoMDA();
            $objFechaFin->setTimeFormatAMPM();


            $temp_fechas = $objFechaIni->getFecha() . '  / ' . $objFechaFin->getFecha();

            $temp_array[$i]= $this->getHCMVectorSum($objeto_sql,$temp_fechas,($i+1));

            $month_capacity = $month_capacity+$temp_array[$i]['total_capacity'];
            $month_sched_run = $month_sched_run+$temp_array[$i]['total_sched_run'];
            $month_run = $month_run+$temp_array[$i]['total_run'];
            $month_total_downtime = $month_total_downtime+$temp_array[$i]['total_down_time'];
            $month_nottrack_downtime = $month_nottrack_downtime+$temp_array[$i]['total_not_track_down_time'];
           
            $i++;

        }

        //exit;

        $objPercentaje  = new PercentajeCalculation(2);

        $objPercentaje->setNumerador($month_run);
        $temp = $month_total_downtime - $month_nottrack_downtime;
        $objPercentaje->setDenominador(($month_run + $temp));

        $temp_array[$i] = array(
                    'orden' => ($i+1),
                    'fecha' => 'Month Total',
                    'total_capacity'  => $month_capacity,
                    'total_sched_run'  => $month_sched_run,
                    'total_run'  => $month_run,
                    'total_down_time'  => $month_total_downtime,
                    'total_not_track_down_time'  => $month_nottrack_downtime,
                    'efficiency'  => $objPercentaje->getPercentaje(),
            );

        $form_selector->get('fecha_graph')->setValue($fecha); 
        $form          = $this->getHCMForm2();
        $form->get('molding')->setValue(2); 
        $this->layout('layout/layout');
        $lastDateArray = $this->getMoldingEfficenciyDao()->getLastDataDate();
        $lastDate = $lastDateArray->current();
        $objPicker = new MonthPickerLastDates($lastDate['fecha']);

        $view = new ViewModel(array(
                'formSelector' => $form_selector,
                'hcmForm' => $form,
                'titulo' => 'Monthly HCM Metrics',
                'action' => 'Hcmindex',
                'result' => $temp_array,
                'lastYear' =>$objPicker->getYear(),
                'monthArray' => json_encode($objPicker->getDisableMonthsArray()),
        )); 

       // $view->setTemplate('application/monthmetrics/index.phtml');
        return $view;
    }


    public function PropmonthmetricsAction()
    {
        $fecha = (string)$this->params()->fromRoute('id');
        $request    = $this->getRequest();
        $form_selector = $this->getMonthMetForm2();
        if($request->isPost())
        {
            $post = $request->getPost();
            $form_selector->setData($post);

            if($form_selector->isValid())
            {
                 //$post = $request->getPost();
            }
        }

        if(empty($fecha)){
            $fecha = (string)@$post['fecha_graph'];

        }
    
        if(empty($fecha))
        {
            $year = date('Y');
            $month = date('m');
        }else{
            list($year,$month) = explode("-",$fecha);
        }
        $semanas = new SemanasByMonth($year,$month);
        $vector_sem = $semanas->getSemanas();
        $i = 0;
        
        $month_capacity = 0;
        $month_sched_run = 0;
        $month_run = 0;
        $month_total_downtime = 0;
        $month_nottrack_downtime = 0;

        foreach($vector_sem as $vec)
        {
            $objeto_sql = $this->getMachTotalUsageDao()->getPropEficienciaBySemanas($vec['start'],$vec['end'],0);
            $objFechaIni = new ClassFecha($vec['start'],false," - ");
            $objFechaIni->setFormatoMDA();
            $objFechaIni->setTimeFormatAMPM();

            $objFechaFin = new ClassFecha($vec['end'],false," - ");
            $objFechaFin->setFormatoMDA();
            $objFechaFin->setTimeFormatAMPM();


            $temp_fechas = $objFechaIni->getFecha() . '  / ' . $objFechaFin->getFecha();
            $temp_array[$i]= $this->getHCMVectorSum($objeto_sql,$temp_fechas,($i+1));

            $month_capacity = $month_capacity+$temp_array[$i]['total_capacity'];
            $month_sched_run = $month_sched_run+$temp_array[$i]['total_sched_run'];
            $month_run = $month_run+$temp_array[$i]['total_run'];
            $month_total_downtime = $month_total_downtime+$temp_array[$i]['total_down_time'];
            $month_nottrack_downtime = $month_nottrack_downtime+$temp_array[$i]['total_not_track_down_time'];
           
          //  print_r($temp_array[$i]);
          //  echo '<br><br>';
            $i++;

        }

        $objPercentaje  = new PercentajeCalculation(2);

        $objPercentaje->setNumerador($month_run);
        $temp = $month_total_downtime - $month_nottrack_downtime;
        $objPercentaje->setDenominador(($month_run + $temp));

        $temp_array[$i] = array(
                    'orden' => ($i+1),
                    'fecha' => 'Month Total',
                    'total_capacity'  => $month_capacity,
                    'total_sched_run'  => $month_sched_run,
                    'total_run'  => $month_run,
                    'total_down_time'  => $month_total_downtime,
                    'total_not_track_down_time'  => $month_nottrack_downtime,
                    'efficiency'  => $objPercentaje->getPercentaje(),
            );

      
        $form_selector->get('fecha_graph')->setValue($fecha); 
        $form          = $this->getHCMForm2();
        $form->get('molding')->setValue(3); 
        $this->layout('layout/layout');
        $lastDateArray = $this->getMoldingEfficenciyDao()->getLastDataDate();
        $lastDate = $lastDateArray->current();
        $objPicker = new MonthPickerLastDates($lastDate['fecha']);

        $view = new ViewModel(array(
                'formSelector' => $form_selector,
                'hcmForm' => $form,
                'titulo' => 'Monthly Proprietary Metrics',
                'action' => 'Hcmindex',
                'result' => $temp_array,
                'lastYear' =>$objPicker->getYear(),
                'monthArray' => json_encode($objPicker->getDisableMonthsArray()),
        )); 

       // $view->setTemplate('application/monthmetrics/index.phtml');
        return $view;
    }


   
}

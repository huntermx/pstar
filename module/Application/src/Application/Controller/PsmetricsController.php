<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Application\Model\TimeClass\ClassDateOp;

use Application\Model\Entity\MoldingCapacity;
use Application\Model\Entity\MoldeoEficiencia;

use Application\Model\Metrics\UpdateMetrics;

class PsmetricsController extends AbstractApplicationController
{


    public function indexAction()
    {
        $arrayme = $this->getMoldingEfficenciyDao()->fetchAll();
        //$arrayme2 = $this->getMachDailyLog()->fetchAll();


        


        $temp = array();
        $form          = $this->getHCMForm();


        foreach($arrayme as $me)
        {
            $objCount = $this->getMachTotalUsageDao()->getMachNumberByDate($me->fecha);

            $number = $objCount->current();

            $temp[]= array(
                'fecha' => $me->fecha,
                'machines_util' => $number['num'],
                'total_uptime' => $me->total_uptime,
                'total_downtime' => $me->total_downtime,
                'setup_time' => $me->setup_time,
                'mantto_prev_time' =>$me->mantto_prev_time,
                'idle_time' =>$me->idle_time,
                'eng_sample_time' =>$me->eng_sample_time,
                'setup_delay_time' =>$me->setup_delay_time,
                
                'paro_fin_time' =>$me->eng_sample_time,
                'arranque_ini_time' =>$me->setup_delay_time,


                'sum1' => $me->total_uptime + $me->total_downtime + $me->idle_time,
                );
        }

        return new ViewModel(array(
                'moldingCap' => $temp,
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'titulo' => 'Daily Plantstar Metrics',
              
            )); 
    }








    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('psmetrics/default', array( 'action'=> 'index'));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('psmetrics/default', array( 'action'=> 'Hcmpsmetrics'));
                }
                return $this->redirect()->toRoute('psmetrics/default', array( 'action'=> 'Proppsmetrics'));
            }
        }
    }


    public function HcmpsmetricsAction()
    {
        $arrayme = $this->getMoldingEfficenciyDao()->getHcmMachinesMetrics();
        $temp = array();
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        foreach($arrayme as $me)
        {
           
             $temp[]= array(
                'fecha' => $me['fecha'],
                'machines_util' => $me['machines'],
                'total_uptime' => $me['up'],
                'total_downtime' => $me['down'],
                'mantto_prev_time' =>$me['mantto'],
                'idle_time' =>$me['idle'],
                'eng_sample_time' =>$me['eng_sample_time'],
                'setup_delay_time' =>$me['setup_delay_time'],
                'paro_fin_time' =>$me['paro_fin_time'],
                'arranque_ini_time' =>$me['arranque_ini_time'],

                'sum1' => $me['up'] + $me['down'] + $me['idle'],
                );

        }
        return new ViewModel(array(
                'moldingCap' => $temp,
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'titulo' => 'Daily HCM Plantstar Metrics',
              
            )); 
    }


    public function ProppsmetricsAction()
    {
        $arrayme = $this->getMoldingEfficenciyDao()->getPropMachinesMetrics();
        $temp = array();
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        foreach($arrayme as $me)
        {
           
             $temp[]= array(
                 'fecha' => $me['fecha'],
                'machines_util' => $me['machines'],
                'total_uptime' => $me['up'],
                'total_downtime' => $me['down'],
                'mantto_prev_time' =>$me['mantto'],
                'idle_time' =>$me['idle'],
                'eng_sample_time' =>$me['eng_sample_time'],
                'setup_delay_time' =>$me['setup_delay_time'],
                'paro_fin_time' =>$me['paro_fin_time'],
                'arranque_ini_time' =>$me['arranque_ini_time'],

                'sum1' => $me['up'] + $me['down'] + $me['idle'],
                );

        }
        return new ViewModel(array(
                'moldingCap' => $temp,
                'hcmForm' => $form,
                'action' => 'Hcmindex',
                'titulo' => 'Daily Proprietary Plantstar Metrics',
              
            )); 
    }



     public function updateMetAction()
    {
 		$today = date("Y-m-d");
        //$today = '2013-10-23';

        $objDate = new ClassDateOp("2012-03-29");
        $objMet = new UpdateMetrics($this->getPstarData(),$this->getMoldingEfficenciyDao(),$this->getMachUsageDao());
        $objMet->setObjUpdate($this->getServiceLocator()->get('UpdateMachUsage'));

        do
        {
            $objDate->getNextDay();
            $dateComp = $objDate->getShowDate("Y-m-d");
            // test
        
                if($dateComp == $today)
                {
                    break;
                }

                $objMet->setDate($dateComp);
                $validaReg= $objMet->getId();
                
                if($validaReg==false)
                {
                    $setup_time             =   $this->getDownTime($dateComp,'Setup');
                    $mantto_time            =   $this->getPstarData()->getPrevMantto($dateComp);
                    $paro_fin_time          =   $this->getDownTime($dateComp,'Paro fin semana');
                    $arranque_ini_time      =   $this->getDownTime($dateComp,'Arranqe Inic. seman');

                    $data_max = array(
                           
                            'fecha' => $dateComp,
                            'total_uptime' => $objMet->getUpTime(),
                            'total_downtime' => $objMet->getDownTime(),
                            'setup_time' => $setup_time,
                            'mantto_prev_time'  => $mantto_time,
                            'idle_time' => $objMet->getIdleTime(),
                            'eng_sample_time'  => $this->getPstarData()->getEngSamples($dateComp),
                            'setup_delay_time'  => $this->getPstarData()->getSetupDelay($dateComp),
                            'paro_fin_time' => $paro_fin_time,
                            'arranque_ini_time' => $arranque_ini_time,
                            'total_assist_time' => $objMet->getAssistTime(),
                    );




                    $meficiencia = new MoldeoEficiencia();
                    $meficiencia->exchangeArray($data_max);
                    $this->getMoldingEfficenciyDao()->agregar($meficiencia);
                    unset($meficiencia);
                    unset($data_max);

                }
            
        }while($dateComp < $today);

         return $this->redirect()->toRoute('capacity',array(
                'controller' => 'index',
                 'action' =>  'updateReg'));

    }

}

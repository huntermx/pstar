<?php

namespace Application\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormHcmMachines
{
	/**
     * @Annotation\Type("Zend\Form\Element\Hidden")
     * @Annotation\Required(false)
     *
     */
    public $id;

    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"Hcm Machine Name"})
     * @Annotation\Attributes({"class":"form-control"})
     */
    public $mach_name;

    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Attributes({"id":"fechacaptura"})
     * @Annotation\Required({"required":"true"})
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"class":"form-control"})
     * 
     */
    public $fecha_track;

     /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;


}
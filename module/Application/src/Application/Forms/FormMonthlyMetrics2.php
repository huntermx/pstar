<?php

namespace Application\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("selectName")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormMonthlyMetrics2
{
	
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required({"required":"true" })
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Attributes({"placeholder":"YYYY-mm"})
     * @Annotation\Attributes({"class":"fecha_input input-small"})
      * @Annotation\Attributes({"id":"default_widget"})    
     */
    public $fecha_graph;

   
     /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Get Report"})
     * @Annotation\Attributes({"name":"submit_get"})
     */
    public $submit_get;


}
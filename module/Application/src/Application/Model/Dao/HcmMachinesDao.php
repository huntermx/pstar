<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Application\Model\Entity\HcmMachines;


class HcmMachinesDao
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
  
    
    public function agregar(HcmMachines $mach)
    {
        
        $data = array(
            'mach_name'             => trim($mach->mach_name),
            'fecha_track'           => $mach->fecha_track,
            'status'                => $mach->status,
        );

        $id = (int)$mach->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) 
            {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
     public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

   
    public function getNumberHcmMachinesByDate($date)
    {
         $sql = " SELECT COUNT(*) as rows FROM hcm_machines
                  WHERE  fecha_track <= '{$date}' ";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function eliminar($id)
    {
        $id  = (int) $id;
        $this->tableGateway->delete(array('id' => $id));
        
    }

}
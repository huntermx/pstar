<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Application\Model\Entity\MachUsageDTotal;


class MachUsageDTotalDao
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
        $this->tableGateway = $tableGateway;
        $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
  
    
    public function agregar(MachUsageDTotal $mach)
    {
        
        $data = array(
            'timestamp'             => date("Y-m-d g:i:s"),
            'fecha'                 => $mach->fecha,
            'mach_name'             => $mach->mach_name,
            'up_time'               => $mach->up_time,
            'down_time'             => $mach->down_time,
            'assist_time'           => $mach->assist_time,
            'idle_time'             => $mach->idle_time,
            'total_time'            => $mach->total_time,
            'prev_mantto_time'      => $mach->prev_mantto_time,    
        );

        $id = (int)$mach->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) 
            {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
     public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function checkRegistroByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return true;
    }

    public function getMachNumberByDate($fecha)
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select(array());
        $select->from('machine_usage_daily')
                ->columns(array('num' =>new \Zend\Db\Sql\Expression('COUNT(*)')),false);
        
       
        $where = new Where();
        $where->equalTo('fecha', $fecha);
        
        $select->where($where);
               
        $statement = $sql->prepareStatementForSqlObject($select);


        $result = $statement->execute();

        return $result;
       
    }

    public function checkRegByDateMach($date,$mach)
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select(array());
        $select->from('machine_usage_daily');
       
        $where = new Where();
        $where->equalTo('fecha', $date);
        $where->equalTo('mach_name', $mach);
        
        $select->where($where);
               
        $statement = $sql->prepareStatementForSqlObject($select);


        $result = $statement->execute();
       
      
        $row = $result->current();
        if (!$row) {
            return false;
        }
        return true;



    }


    public function getRegByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }else{
             $rowset = $this->tableGateway->select(array('fecha' => $date2));
        }
        return $rowset;
    }

    public function getHcmMachinesByDate($date)
    {

        $sql =  "  SELECT *
                    FROM machine_usage_daily
                    WHERE mach_name  IN
                    (
                        SELECT  * FROM
                        (
                            SELECT mach_name
                            FROM hcm_machines
                        ) AS subquery
                    )
                    AND fecha=  '{$date}'";
        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

      public function getPropMachinesByDate($date)
    {

        $sql =  "  SELECT *
                    FROM machine_usage_daily
                    WHERE mach_name NOT IN
                    (
                        SELECT  * FROM
                        (
                            SELECT mach_name
                            FROM hcm_machines
                        ) AS subquery
                    )
                    AND fecha=  '{$date}'";
        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getHCMEficienciaBySemanas($start, $end,$min =4500)
    {
        $sql = "SELECT  mdaily.fecha, mc.hours, 
                            SUM(mdaily.up_time) as uptime, 
                            SUM(mdaily.down_time) as downtime, 
                            SUM(mdaily.assist_time) as assist_time, 
                            SUM(mdaily.prev_mantto_time) as mantto_prev, 
                            SUM(mdaily.idle_time) as idle_time, 
                            SUM(mdaily.eng_sample_time) as eng_sample_time, 
                            SUM(mdaily.setup_delay_time) as setup_delay_time, 
                            SUM(mdaily.paro_fin_time) as paro_fin_time, 
                            SUM(mdaily.arranque_ini_time) as arranque_ini_time, 

                            COUNT(mdaily.mach_name) as machines
                FROM  machine_usage_daily as mdaily
                INNER JOIN molding_capacity as mc ON mdaily.fecha=mc.fecha
                WHERE mdaily.mach_name IN
                        (
                            SELECT * FROM
                            (
                            SELECT mach_name
                            FROM hcm_machines
                            ) AS subquery
                        )
                AND (mdaily.fecha BETWEEN '{$start}' AND  '{$end}' AND  mdaily.up_time >= '{$min}')
                GROUP BY mdaily.fecha
                ORDER BY mdaily.fecha ASC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

      public function getPropEficienciaBySemanas($start, $end,$min =4500)
    {
        $sql = "SELECT  mdaily.fecha, mc.hours, 
                             SUM(mdaily.up_time) as uptime, 
                            SUM(mdaily.down_time) as downtime, 
                            SUM(mdaily.assist_time) as assist_time, 
                            SUM(mdaily.prev_mantto_time) as mantto_prev, 
                            SUM(mdaily.idle_time) as idle_time, 
                            SUM(mdaily.eng_sample_time) as eng_sample_time, 
                            SUM(mdaily.setup_delay_time) as setup_delay_time, 
                            SUM(mdaily.paro_fin_time) as paro_fin_time, 
                            SUM(mdaily.arranque_ini_time) as arranque_ini_time, 
                            COUNT(mdaily.mach_name) as machines
                FROM  machine_usage_daily as mdaily
                INNER JOIN molding_capacity as mc ON mdaily.fecha=mc.fecha
                WHERE mdaily.mach_name NOT IN
                        (
                            SELECT * FROM
                            (
                            SELECT mach_name
                            FROM hcm_machines
                            ) AS subquery
                        )
                AND (mdaily.fecha BETWEEN '{$start}' AND  '{$end}' AND  mdaily.up_time >= '{$min}')
                GROUP BY mdaily.fecha
                ORDER BY mdaily.fecha ASC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

}
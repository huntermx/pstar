<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Application\Model\Entity\MachUsageDaily;


class MachUsageDailyDao
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
    $this->tableGateway = $tableGateway;
    $this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
  
    
    public function agregar(MachUsageDaily $mach)
    {
        
        $data = array(
            'timestamp'             => date("Y-m-d g:i:s"),
            'fecha'                 => $mach->fecha,
            'mach_name'          => $mach->mach_name,
            'start_time'        => $mach->start_time,
            'end_time'            => $mach->end_time,
            'up_time'      => $mach->up_time,
            'down_time'             => $mach->down_time,
            'idle_time'       => $mach->idle_time,
            'assist_time'       => $mach->assist_time,
        );

        $id = (int)$mach->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) 
            {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
     public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function checkRegUnique($arrayData)
    {
        $sql = new Sql($this->adapter);
        $select= $sql->select(array());
        $select->from('log_mach_day');
       
        $where = new Where();
        $where->equalTo('fecha', $arrayData['fecha']);
        $where->equalTo('mach_name', $arrayData['mach_name']);
        $where->equalTo('up_time', $arrayData['up_time']);
        $where->equalTo('down_time', $arrayData['down_time']);
        $where->equalTo('idle_time', $arrayData['idle_time']);
        $select->where($where);
               
        $statement = $sql->prepareStatementForSqlObject($select);


        $result = $statement->execute();
       
      
        $row = $result->current();
        if (!$row) {
            return false;
        }
        return true;

    }


    public function checkRegistroByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return true;
    }

    public function getRegByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

}
<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Application\Model\Entity\MoldeoEficiencia;


class MoldeoEficienciaDao
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
		$this->tableGateway = $tableGateway;
		$this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
  
    
    public function agregar(MoldeoEficiencia $molding)
    {
        
        $data = array(
            'timestamp'             => date("Y-m-d g:i:s"),
            'fecha'                 => $molding->fecha,
            'total_uptime'          => $molding->total_uptime,
            'total_downtime'        => $molding->total_downtime,
            'total_assist_time'        => $molding->total_assist_time,
            'setup_time'            => $molding->setup_time,
            'mantto_prev_time'      => $molding->mantto_prev_time,
            'idle_time'             => $molding->idle_time,
            'eng_sample_time'       => $molding->eng_sample_time,
            'setup_delay_time'      => $molding->setup_delay_time,
            'paro_fin_time'         => $molding->paro_fin_time,
            'arranque_ini_time'     => $molding->arranque_ini_time,
        );



        $id = (int)$molding->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) 
            {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
     public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function checkRegistroByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return true;
    }

    public function getRegByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

     public function getEficienciaBySemanas($start, $end,$min =4500)
    {   
       /* $sql =  "SELECT mc.total_time, 
                        mc.nstatus,me.fecha,
                        me.total_uptime,
                        me.total_downtime,
                        me.total_assist_time,
                        me.setup_time,
                        me.mantto_prev_time,
                        me.idle_time,
                        me.eng_sample_time,
                        me.setup_delay_time,
                        me.paro_fin_time,
                        me.arranque_ini_time
                FROM moldeo_eficiencia as me
                LEFT JOIN molding_capacity as mc ON me.fecha=mc.fecha
                WHERE  (me.fecha BETWEEN '{$start}' AND  '{$end}' AND  me.total_uptime >= '{$min}')
                ORDER BY me.fecha ASC ";*/

        $sql ="SELECT  mc.total_time,
                       mc.nstatus,   
                       mdaily.fecha, mc.hours, 
                            SUM(mdaily.up_time) as total_uptime, 
                            SUM(mdaily.down_time) as total_downtime, 
                            SUM(mdaily.assist_time) as total_assist_time, 
                            SUM(mdaily.prev_mantto_time) as mantto_prev_time, 
                            SUM(mdaily.idle_time) as idle_time, 
                            SUM(mdaily.eng_sample_time) as eng_sample_time, 
                            SUM(mdaily.setup_delay_time) as setup_delay_time, 
                            SUM(mdaily.paro_fin_time) as paro_fin_time, 
                            SUM(mdaily.arranque_ini_time) as arranque_ini_time, 

                            COUNT(mdaily.mach_name) as machines
                FROM  machine_usage_daily as mdaily
                INNER JOIN molding_capacity as mc ON mdaily.fecha=mc.fecha
                WHERE  (mdaily.fecha BETWEEN '{$start}' AND  '{$end}' AND  mdaily.up_time >= '{$min}')
                GROUP BY mdaily.fecha
                ORDER BY mdaily.fecha ASC";


  
        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getLastDataDate()
    {
        $sql = "SELECT fecha FROM moldeo_eficiencia  ORDER BY fecha DESC LIMIT 1";
        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getECByDates($fecha_ini, $fecha_final,$limit=false)
    {
        // la fecha no puede ser hoy
        // fecha
        if($limit == false)
        {
            $string_limit = "";
        }else{
            $string_limit = " LIMIT {$limit}";
        }

        $sql =" SELECT mc.total_time, 
                      mc.nstatus,
                      me.fecha,
                      me.total_uptime,
                      me.total_downtime,me.total_assist_time,me.setup_time,me.mantto_prev_time,
                me.idle_time,me.eng_sample_time,me.setup_delay_time,me.paro_fin_time,me.arranque_ini_time
                FROM moldeo_eficiencia as me
                LEFT JOIN molding_capacity as mc ON me.fecha=mc.fecha
                WHERE  (me.fecha BETWEEN '{$fecha_ini}' AND  '{$fecha_final}') AND mc.total_time
                ORDER BY me.fecha ASC ".$string_limit;

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }


    public function getHcmMachinesMetrics()
    {
        $sqlStatement = " SELECT fecha, 
                                 SUM(up_time) as up, 
                                 COUNT(mach_name) as machines, 
                                 SUM(down_time) as down,
                                 SUM(prev_mantto_time) as mantto, 
                                 SUM(total_time) as total,
                                 SUM(idle_time) as idle,
                                 SUM(eng_sample_time) as eng_sample_time,
                                 SUM(setup_delay_time) as setup_delay_time,
                                 SUM(paro_fin_time) as paro_fin_time,
                                 SUM(arranque_ini_time) as arranque_ini_time



                          FROM machine_usage_daily
                          WHERE mach_name IN
                        (
                            SELECT * FROM
                            (
                            SELECT mach_name
                            FROM hcm_machines
                            ) AS subquery
                        )
                        GROUP BY fecha
                        ORDER BY fecha DESC";

        $stmt = $this->adapter->query($sqlStatement);
        $result = $stmt->execute();
        return $result;
    }

    public function getPropMachinesMetrics()
    {
        $sqlStatement = " SELECT fecha, 
                                SUM(up_time) as up, 
                                 COUNT(mach_name) as machines, 
                                 SUM(down_time) as down,
                                 SUM(prev_mantto_time) as mantto, 
                                 SUM(total_time) as total,
                                 SUM(idle_time) as idle,
                                 SUM(eng_sample_time) as eng_sample_time,
                                 SUM(setup_delay_time) as setup_delay_time,
                                 SUM(paro_fin_time) as paro_fin_time,
                                 SUM(arranque_ini_time) as arranque_ini_time

                          FROM machine_usage_daily
                          WHERE mach_name NOT IN
                        (
                            SELECT * FROM
                            (
                            SELECT mach_name
                            FROM hcm_machines
                            ) AS subquery
                        )
                        GROUP BY fecha
                        ORDER BY fecha DESC";

        $stmt = $this->adapter->query($sqlStatement);
        $result = $stmt->execute();
        return $result;
    }


}
<?php

namespace Application\Model\Dao;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Application\Model\Entity\MoldingCapacity;


class MoldingCapacityDao
{

    protected $tableGateway;
    protected $adapter;

    public function __construct(TableGateway $tableGateway,Adapter $adapter)
    {
		$this->tableGateway = $tableGateway;
		$this->adapter = $adapter;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
  
    
    public function agregar(MoldingCapacity $molding)
    {
        
        $data = array(
            'fecha'                 => $molding->fecha,
            'nstatus'               => $molding->nstatus,
            'total_machines'        => $molding->total_machines,
            'hours'                 => $molding->hours,
            'total_time'            => abs($molding->hours * $molding->total_machines),
        );


        $data = $this->checkNstatus($data);

        $id = (int)$molding->id;
        
        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getRegistro($id)) 
            {
                $this->tableGateway->update($data, array('id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }
    
    public function getRegistro($id)
    {
        $id  = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    private function checkNstatus($data)
    {
        if($data['nstatus']=="Holiday")
        {
            $data['total_time'] = 0;
        }
        return $data;
    }

    
  


    public function checkRegistroByDate($date2)
    {
        $date2  = (string) $date2;
        $rowset = $this->tableGateway->select(array('fecha' => $date2));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return true;
    }

    public function getTonnagebyMachine($machine)
    {
        $machine  = (string) $machine;

        $rowset = $this->tableGateway->select(array('machine_num' => $machine));
        $row = $rowset->current();
        if (!$row) {
            $row = new MachineTonnage();
            $row->exchangeArray(array());

           // throw new \Exception("Could not find Machine $machine");
        }
        return $row;
    }


        public function getDataByDates($fecha_ini, $fecha_final,$limit=false)
    {
        // la fecha no puede ser hoy
        // fecha
        if($limit == false)
        {
            $string_limit = "";
        }else{
            $string_limit = " LIMIT {$limit}";
        }

        $sql ="SELECT *
            FROM molding_capacity 
            WHERE  (fecha BETWEEN '{$fecha_ini}' AND  '{$fecha_final}' ) 
            ORDER BY fecha DESC ".$string_limit;

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;
    }

}
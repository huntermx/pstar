<?php


namespace Application\Model\Entity;

class HcmMachines
{
    public $id;
    public $mach_name;
    public $fecha_track;
    public $status;
   
    public function exchangeArray($data)
    {
	    $this->id               	    = (isset($data['id'])) ? $data['id'] : 0;
	    $this->mach_name                = (isset($data['mach_name'])) ? $data['mach_name'] : null;
	    $this->fecha_track        		= (isset($data['fecha_track'])) ? $data['fecha_track'] : null;
        $this->status		            = (isset($data['status'])) ? $data['status'] : 1;
     }

}
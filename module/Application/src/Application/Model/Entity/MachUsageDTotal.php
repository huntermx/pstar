<?php


namespace Application\Model\Entity;

class MachUsageDTotal
{
    public $id;
    public $timestamp;
    public $fecha;
    public $mach_name;
    public $up_time;
    public $down_time;
    public $idle_time;
    public $total_time;    
    public $assist_time;
	public $prev_mantto_time;

    public function exchangeArray($data)
    {
	    $this->id               	       = (isset($data['id'])) ? $data['id'] : 0;
	    $this->timestamp                = (isset($data['timestamp'])) ? $data['timestamp'] : null;
	    $this->fecha        		         = (isset($data['fecha'])) ? $data['fecha'] : null;
        $this->mach_name		             = (isset($data['mach_name'])) ? $data['mach_name'] : null;
        $this->up_time                  = (isset($data['up_time'])) ? $data['up_time'] : null;
        $this->down_time                = (isset($data['down_time'])) ? $data['down_time'] : null;
        $this->assist_time                = (isset($data['assist_time'])) ? $data['assist_time'] : null;
        $this->idle_time                = (isset($data['idle_time'])) ? $data['idle_time'] : null;
        $this->total_time                = (isset($data['total_time'])) ? $data['total_time'] : null;        
        $this->prev_mantto_time                = (isset($data['prev_mantto_time'])) ? $data['prev_mantto_time'] : null;     
     }

}

<?php


namespace Application\Model\Entity;

class MachUsageDaily
{
    public $id;
    public $timestamp;
    public $fecha;
    public $mach_name;
    public $start_time;
    public $end_time;
    public $up_time;
    public $down_time;
    public $idle_time;
    public $assist_time;
	
    public function exchangeArray($data)
    {
	     $this->id               	       = (isset($data['id'])) ? $data['id'] : 0;
	     $this->timestamp                = (isset($data['timestamp'])) ? $data['timestamp'] : null;
	     $this->fecha        		         = (isset($data['fecha'])) ? $data['fecha'] : null;
         $this->mach_name		             = (isset($data['mach_name'])) ? $data['mach_name'] : null;
         $this->start_time               = (isset($data['start_time'])) ? $data['start_time'] : null;
         $this->end_time                 = (isset($data['end_time'])) ? $data['end_time'] : null;
         $this->up_time                  = (isset($data['up_time'])) ? $data['up_time'] : null;
         $this->down_time                = (isset($data['down_time'])) ? $data['down_time'] : null;
         $this->idle_time                = (isset($data['idle_time'])) ? $data['idle_time'] : null;
         $this->assist_time                = (isset($data['assist_time'])) ? $data['assist_time'] : null;
     }

}

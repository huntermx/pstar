<?php


namespace Application\Model\Entity;

class MoldeoEficiencia
{
    public $id;
    public $timestamp;
    public $fecha;
    public $total_uptime;
    public $total_downtime;
    public $setup_time;
    public $mantto_prev_time;
    public $idle_time;
    public $eng_sample_time;
    public $paro_fin_time;

	
    public function exchangeArray($data)
    {
	    $this->id               	    = (isset($data['id'])) ? $data['id'] : 0;
        $this->timestamp                = (isset($data['timestamp'])) ? $data['timestamp'] : null;
	    $this->fecha                    = (isset($data['fecha'])) ? $data['fecha'] : null;
	    $this->total_uptime        		= (isset($data['total_uptime'])) ? $data['total_uptime'] : null;
        $this->total_downtime		    = (isset($data['total_downtime'])) ? $data['total_downtime'] : null;
        $this->setup_time               = (isset($data['setup_time'])) ? $data['setup_time'] : null;
        $this->mantto_prev_time         = (isset($data['mantto_prev_time'])) ? $data['mantto_prev_time'] : null;
        $this->idle_time                = (isset($data['idle_time'])) ? $data['idle_time'] : null;      
        $this->eng_sample_time          = (isset($data['eng_sample_time'])) ? $data['eng_sample_time'] : null;   
        $this->setup_delay_time         = (isset($data['setup_delay_time'])) ? $data['setup_delay_time'] : null;   
        $this->paro_fin_time            = (isset($data['paro_fin_time'])) ? $data['paro_fin_time'] : null;      
        $this->arranque_ini_time        = (isset($data['arranque_ini_time'])) ? $data['arranque_ini_time'] : null;    
        $this->total_assist_time        = (isset($data['total_assist_time'])) ? $data['total_assist_time'] : null;   
     }

}

<?php


namespace Application\Model\Entity;

class MoldingCapacity
{
    public $id;
    public $fecha;
    public $nstatus;
    public $total_machines;
    public $hours;
    public $total_time;
    
	
    public function exchangeArray($data)
    {
	   $this->id               	       = (isset($data['id'])) ? $data['id'] : 0;
	   $this->fecha                    = (isset($data['fecha'])) ? $data['fecha'] : null;
	   $this->nstatus        		   = (isset($data['nstatus'])) ? $data['nstatus'] : null;
       $this->total_machines		   = (isset($data['total_machines'])) ? $data['total_machines'] : null;
       $this->hours                    = (isset($data['hours'])) ? $data['hours'] : null;
       $this->total_time              = (isset($data['total_time'])) ? $data['total_time'] : null;
       
     }

}

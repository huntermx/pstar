<?php


namespace Application\Model\Metrics;

use Application\Model\Dao\MachUsageDTotalDao;
use Application\Model\Entity\MachUsageDTotal;


class UpdateMachUsage
{

    protected $arrayMachDaily=array();
    protected $objMachDaily;
    protected $temp;
    protected $sm;
    protected $pstarDao;
    
    public function __construct( $sm){
        $this->sm  = $sm;
    }

    

    public function setArrayMach(array $machDaily)
    {

        $this->arrayMachDaily[][$machDaily['mach_name']] = $machDaily;

    }


    public function splitArray()
    {
        $keyControl = false;
        $date       = false;
        $upTime     =0;
        $downTime   =0;
        $idleTime   =0;
        $assistTime   =0;

        foreach($this->arrayMachDaily as $val)
        {

       
            $hours = 0;

            foreach($val as $key => $value)
            {

                if($keyControl != $key)
                {
                    if($keyControl){
                        $this->saveTotals($keyControl,$upTime,$downTime,$idleTime,$assistTime,$date);
                    }
                    $date       = $value['fecha'];
                    $keyControl = $key;
                    $upTime     =0;
                    $downTime   =0;
                    $idleTime   =0;
                    $assistTime   =0;
            }


             $temp_day = $value['up_time'] + $value['down_time'] +  $value['idle_time'];

            
            $hours = $temp_day / 3600;

          //  print_r($hours);
           // echo '<br><br>';

           // if($hours>5.5)
            //{
            //         print_r($val);
            //echo '<br><br>';
                // Agregar validacion para Start and End Time
                $upTime = $upTime + $value['up_time'];
                $downTime = $downTime + $value['down_time'];
                $idleTime = $idleTime + $value['idle_time'];
                $assistTime   = $assistTime + $value['assist_time'];

           // }
            
   
            }
        }

          if($keyControl){
                $this->saveTotals($keyControl,$upTime,$downTime,$idleTime,$assistTime,$date);
          }
    }   


    private function saveTotals($key,$up,$down,$idle,$assist,$fecha)
    {
        if($fecha || $key)
        {
            $objMachTotalDaily = new MachUsageDTotal();
            $objMachTotalDaily->exchangeArray(array(
                        'fecha' => $fecha,
                        'mach_name' => $key,
                        'up_time' => $up,
                        'down_time' => $down,
                        'idle_time' => $idle,
                        'total_time' => ($up+$down+$idle),
                        'assist_time' => $assist,
                        'prev_mantto_time' => $this->getPstarData()->getPrevManttoByMach($fecha,$key),
                ));  
        }
        
        $flagDay = $this->getMachTotalUsageDao()->checkRegByDateMach($fecha,$key);
        if($flagDay==FALSE)
        {
               $this->getMachTotalUsageDao()->agregar($objMachTotalDaily);
        }
     
    }


    public function getMachTotalUsageDao()
    {
        if (!$this->objMachDaily) {
            $this->objMachDaily = $this->sm->get('Application\Model\Dao\MachUsageDTotalDao');
        }
       return $this->objMachDaily;
    }

     private function getPstarData()
    {
        if (!$this->pstarDao) {
            $this->pstarDao = $this->sm->get('Application\Model\PStarDb\PlantstarData');
        }
       return $this->pstarDao;
    }

}
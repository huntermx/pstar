<?php


namespace Application\Model\Metrics;

use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Entity\MachUsageDaily;




class UpdateMetrics
{
    public $results;
    public $concepts;
    protected $objPStar;
    protected $objEfficiency;
    protected $objMachDaily;
    protected $objUpdate;
    protected $date;
    protected $upTime;
    protected $downTime;
    protected $idleTime;
    protected $assistTime;
    public $id;

    public function __construct($objPStar, $objMet, $objMachDaily, $date=false)
    {
        $this->objPStar = $objPStar;
        $this->objEfficiency = $objMet;
        $this->objMachDaily = $objMachDaily;
        $this->id   = false;
        if($date)
        {
          $this->setDate($date);
        }

    }


    public function setObjUpdate($obj)
    {
        $this->objUpdate = $obj;
    }

    public function setDate($date)
    {
        $this->date = $date;
        $this->id   = false;
        $this->upTime = false;
        $this->downTime = false;
        $this->idleTime = false;
        $this->assistTime = false;
    }


    private function queryPsDb()
    {
        if($this->date ==false)
        {
           throw new \Exception("Could not find valid Date $this->date");
        }

        $result_up = $this->objPStar->getEficiencia($this->date);
        $up = 0;
        $down = 0;
        $idle = 0;
        $assist = 0;
        $objMachTotalDay = $this->objUpdate;
        $objMach = new MachUsageDaily();
        


        foreach($result_up as $res)
        {


            $up = $up + $res['up_time'];
            $down = $down + $res['total_down_time'];
            $idle = $idle +  $res['idle_time'];
            $assist = $assist + $res['total_assist_time'];
           // }


            $arrayTemp = $this->getObjMach($res,$this->date);

            $temp_day = $res['up_time'] + $res['total_down_time'] +  $res['idle_time'];
            $hours = $temp_day / 3600;
            //print_r($hours);
            //echo '<br><br>';

           // if($hours>5.5)
           // {
                // print_r($res);
                //echo '<br><br>';
                
            $objMach->exchangeArray($arrayTemp);

            $flag = $this->objMachDaily->checkRegUnique($arrayTemp);
            if($flag == false){
              $this->objMachDaily->agregar($objMach);
            }
            

            $objMachTotalDay->setArrayMach($arrayTemp);
        }
        $objMachTotalDay->splitArray();
        $this->upTime = $up;
        $this->downTime = $down;
        $this->idleTime = $idle;
        $this->assistTime = $assist;



        
    }


    private function getObjMach($arrayData,$date)
    {
        $temp = array(
                       'fecha' => $date,
                       'mach_name' => $arrayData['mach_name'],
                       'start_time' => $arrayData['start_time'],
                       'end_time' => $arrayData['end_time'],
                       'up_time' => $arrayData['up_time'],
                       'down_time' => $arrayData['total_down_time'],
                       'idle_time' => $arrayData['idle_time'],
                       'assist_time' => $arrayData['total_assist_time'],
            );
        return $temp;
    }


    private function checkIdByDate()
    {
        $this->id = $this->objEfficiency->checkRegistroByDate($this->date);


       
    }

    public function getUpTime()
    {
        if(!$this->upTime)
        {
          $this->queryPsDb();
        }
        return $this->upTime;
    }

    public function getDownTime()
    {
        if(!$this->downTime)
        {
          $this->queryPsDb();
        }
        return $this->downTime;
    }

     public function getIdleTime()
    {
        if(!$this->idleTime)
        {
          $this->queryPsDb();
        }
        return $this->idleTime;
    }

    public function getAssistTime()
    {
        if(!$this->assistTime)
        {
          $this->queryPsDb();
        }
        return $this->assistTime;
    }

    public function getId()
    {
   
          $this->checkIdByDate();

        return $this->id;
    }

}
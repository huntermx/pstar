<?php

namespace Application\Model\PStarDb;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class PlantstarData
{


public function __construct(TableGateway $tableGateway,Adapter $adapter)
{
		$this->tableGateway = $tableGateway;
		$this->adapter = $adapter;
}


 public function getAllMachines()
{
        $sql="SELECT  mach_seq, mach_name
              FROM mach_def";

            
		$stmt = $this->adapter->query($sql);
		$result = $stmt->execute();
        return $result;
}


public function getAllMachineCurrentJobs()
{
	$sql = new Sql($this->adapter);
	$select= $sql->select();
    $select->from(array('a'=>'mach_def'));
    $select->join(array('b'=>'job_cur_base'),'a.mach_seq=b.mach_seq',array('job','product','user_text_4'));


    $statement = $sql->prepareStatementForSqlObject($select);
    $result = $statement->execute();
    return $result;

}



public function getHistoricJobs()
{

        $today = date("Y-m-d h:i:s");

        $sql =  "SELECT m.mach_seq, m.mach_name, ss.start_time,ss.end_time,ss.up_time,ss.total_down_time, ss.user_text_3,ss.product,ss.user_text_4
                FROM mach_def as m
                LEFT JOIN ss_hist_base as ss ON (ss.mach_seq=m.mach_seq)
                WHERE  ss.start_time > '2013-03-01 00:00:00' AND ss.start_time < '{$today}'
                ORDER BY ss.end_time DESC";

        $stmt = $this->adapter->query($sql);
        $result = $stmt->execute();
        return $result;

}


public function getEficiencia($fecha)
{
    // la fecha no puede ser hoy
    // fecha
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql ="SELECT DISTINCT ss.start_time, m.mach_seq, m.mach_name,ss.end_time,ss.up_time,ss.total_down_time,ss.idle_time,ss.total_assist_time
            FROM mach_def as m
            LEFT JOIN ss_hist_base as ss ON (ss.mach_seq=m.mach_seq)
            WHERE  ss.start_time >= '{$fecha_inicial}' AND ss.start_time < '{$fecha_final}'
            AND (m.mach_name <> '036' AND m.mach_name <> '050' AND m.mach_name <> '051' AND m.mach_name <> '052')
   
            ORDER BY m.mach_name";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;
}


public function getPrevManttoByMach($fecha,$mach)
{
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";
    $time = 0;

    $sql = "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                      sd.down_reason,sd.idx, sd.down_time
              FROM ss_hist_base AS ss
              INNER JOIN ss_hist_dar AS sd ON (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq)
              WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.start_time < '{$fecha_final}')) AND (sd.down_time >0)
              AND sd.idx='45' AND ss.mach_name = '{$mach}'";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
     foreach($result as $resetup)
    {
        $time = $time + $resetup['down_time'];
    }

    return $time;
   
}

public function getPrevMantto($fecha)
{
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";
    $time = 0;

    $sql = "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                   sd.down_reason,sd.idx, sd.down_time
              FROM ss_hist_base AS ss
              INNER JOIN ss_hist_dar AS sd ON (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq)
              WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.start_time < '{$fecha_final}')) AND (sd.down_time >0)
              AND sd.idx='45'";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
     foreach($result as $resetup)
    {
        $time = $time + $resetup['down_time'];
    }

    return $time;
   
}


public function getEngSamples($fecha)
{
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";
    $time_up = 0;
    $time_down = 0;
    $time_idle = 0;

    $sql = "SELECT  ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                     ss.user_text_3, ss.user_text_4,ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason,sd.idx, 
                     sd.down_time,ss.up_time,ss.total_down_time,ss.idle_time  
            FROM ss_hist_base AS ss
            LEFT JOIN ss_hist_dar AS sd ON (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq)
            WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.start_time < '{$fecha_final}')) 
            AND ss.job like '%ENGSAM%'   AND sd.idx='1'";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
     foreach($result as $resetup)
    {
        $time_up = $time_up + $resetup['up_time'];
        $time_down = $time_down + $resetup['total_down_time'];
        $time_idle = $time_idle + $resetup['idle_time'];
    }

    return ($time_up + $time_down + $time_idle);
   
}


public function getSetupDelay($fecha)
{
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";
    $time_up = 0;
    $time_down = 0;
    $time_idle = 0;

    $sql = "SELECT  ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                     ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason,sd.idx, 
                     sd.down_time,ss.up_time,ss.total_down_time,ss.idle_time  
            FROM ss_hist_base AS ss
            LEFT JOIN ss_hist_dar AS sd ON (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq)
            WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.start_time < '{$fecha_final}')) 
            AND ss.job like 'Setup Delay%'   AND sd.idx='1'";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
     foreach($result as $resetup)
    {
        $time_up = $time_up + $resetup['up_time'];
        $time_down = $time_down + $resetup['total_down_time'];
        $time_idle = $time_idle + $resetup['idle_time'];
    }

    return ($time_up + $time_down + $time_idle);
   
}




public function getDownTimeSetup($fecha)
{
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";
    $time = 0;

    $sql= "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                ss.user_text_3, ss.user_text_4,ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason, sd.down_time

                FROM ss_hist_base AS ss
                INNER JOIN ss_hist_dar AS sd ON 
                (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq and ss.child_idx=0 )
                WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.end_time<='{$fecha_final}')) AND (sd.down_time >0) AND job ='Setup Delay'
                ORDER BY sd.down_reason desc, CAST( ss.mach_name as signed)";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();

    return $result;
}





public function getDownTimeByDatReason($fecha,$reason)
{

    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql= "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                  sd.down_reason, sd.down_time

                  FROM ss_hist_base AS ss
                  INNER JOIN ss_hist_dar AS sd ON 
                  (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq and ss.child_idx=0 )
                  WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.start_time < '{$fecha_final}')) AND (sd.down_time >0) AND 
                  (sd.down_reason ='{$reason}') and job <> 'Setup Delay'";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;
}



public function getSetupDelayTime($fecha)
{

    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql= "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
                  ss.user_text_3, ss.user_text_4,ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason, sd.down_time

                  FROM ss_hist_base AS ss
                  INNER JOIN ss_hist_dar AS sd ON 
                  (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq and ss.child_idx=0 )
                  WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.end_time<='{$fecha_final}')) AND (sd.down_time >0) AND 
                (sd.down_reason ='Setup' OR sd.down_reason='Paro fin semana' OR sd.down_reason='Arranqe Inic. seman')
                  ORDER BY CAST( ss.mach_name as signed)";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;


}


public function getDownTimeSetupHCM($fecha)
{

$fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql= "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
            ss.user_text_3, ss.user_text_4,ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason, sd.down_time
            FROM ss_hist_base AS ss
            INNER JOIN ss_hist_dar AS sd ON 
            (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq and ss.child_idx=0 )
            WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.end_time<='{$fecha_final}')) AND (sd.down_time >0) 
            
            AND (ss.user_text_3 = '455900' OR ss.user_text_3 = '394016' OR ss.user_text_3 = '394014'
            OR ss.user_text_3 = '393107' OR ss.user_text_3 = '392408' OR ss.user_text_3 = '392385' OR ss.user_text_3 = '392379' OR ss.user_text_3 = '392378'
            OR ss.user_text_3 = '392377' OR ss.user_text_3 = '392376' OR ss.user_text_3 = '392375' OR ss.user_text_3 = '392374' OR ss.user_text_3 = '392373'
            OR ss.user_text_3 = '391995' OR ss.user_text_3 = '391637' OR ss.user_text_3 = '391636' OR ss.user_text_3 = '391635' OR ss.user_text_3 = '391613'
            OR ss.user_text_3 = '391609' OR ss.user_text_3 = '391178' OR ss.user_text_3 = '391175' OR ss.user_text_3 = '391174' OR ss.user_text_3 = '391151'
            OR ss.user_text_3 = '391150' OR ss.user_text_3 = '391149' OR ss.user_text_3 = '391148' OR ss.user_text_3 = '391139' OR ss.user_text_3 = '391126'
            OR ss.user_text_3 = '391125' OR ss.user_text_3 = '391124' OR ss.user_text_3 = '391123' OR ss.user_text_3 = '391122' OR ss.user_text_3 = '391121'
            OR ss.user_text_3 = '391120' OR ss.user_text_3 = '390943' OR ss.user_text_3 = '390940' OR ss.user_text_3 = '390905' OR ss.user_text_3 = '390904'
            OR ss.user_text_3 = '390903' OR ss.user_text_3 = '390902' OR ss.user_text_3 = '275000' )
             AND (sd.down_reason ='Setup' OR sd.down_reason='Paro fin semana' OR sd.down_reason='Arranqe Inic. seman')
            ORDER BY sd.down_reason desc, CAST( ss.mach_name as signed)";
    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;

}


public function getSetupDelayHCM($fecha)
{

$fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql= "SELECT ss.start_time, ss.end_time, ss.shift_id,ss.mach_name,cast(ss.mach_name as signed) as MachName,
            ss.user_text_3, ss.user_text_4,ss.job,ss.product, ss.tool, ss.mat_formula, sd.down_reason, sd.down_time
            FROM ss_hist_base AS ss
            INNER JOIN ss_hist_dar AS sd ON 
            (ss.mach_seq = sd.mach_seq and ss.ss_mseq = sd.ss_mseq and ss.child_idx=0 )
            WHERE ((ss.start_time>='{$fecha_inicial}') AND (ss.end_time<='{$fecha_final}')) AND (sd.down_time >0) 
            
            AND (ss.user_text_3 = '455900' OR ss.user_text_3 = '394016' OR ss.user_text_3 = '394014'
            OR ss.user_text_3 = '393107' OR ss.user_text_3 = '392408' OR ss.user_text_3 = '392385' OR ss.user_text_3 = '392379' OR ss.user_text_3 = '392378'
            OR ss.user_text_3 = '392377' OR ss.user_text_3 = '392376' OR ss.user_text_3 = '392375' OR ss.user_text_3 = '392374' OR ss.user_text_3 = '392373'
            OR ss.user_text_3 = '391995' OR ss.user_text_3 = '391637' OR ss.user_text_3 = '391636' OR ss.user_text_3 = '391635' OR ss.user_text_3 = '391613'
            OR ss.user_text_3 = '391609' OR ss.user_text_3 = '391178' OR ss.user_text_3 = '391175' OR ss.user_text_3 = '391174' OR ss.user_text_3 = '391151'
            OR ss.user_text_3 = '391150' OR ss.user_text_3 = '391149' OR ss.user_text_3 = '391148' OR ss.user_text_3 = '391139' OR ss.user_text_3 = '391126'
            OR ss.user_text_3 = '391125' OR ss.user_text_3 = '391124' OR ss.user_text_3 = '391123' OR ss.user_text_3 = '391122' OR ss.user_text_3 = '391121'
            OR ss.user_text_3 = '391120' OR ss.user_text_3 = '390943' OR ss.user_text_3 = '390940' OR ss.user_text_3 = '390905' OR ss.user_text_3 = '390904'
            OR ss.user_text_3 = '390903' OR ss.user_text_3 = '390902' OR ss.user_text_3 = '275000' )
             AND  ss.job ='Setup Delay'
            ORDER BY sd.down_reason desc, CAST( ss.mach_name as signed)";
    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;

}



public function getEficienciaHCM($fecha)
{
    // la fecha no puede ser hoy
    // fecha
    $fecha_final = $fecha . " 22:30:00";
    $fecha_temp = $this->getYesterday($fecha);
    $fecha_inicial = $fecha_temp . " 22:30:00";

    $sql ="SELECT DISTINCT ss.start_time, m.mach_seq, m.mach_name,ss.end_time,ss.up_time,ss.total_down_time, ss.user_text_3 
            FROM mach_def as m LEFT JOIN ss_hist_base as ss ON (ss.mach_seq=m.mach_seq) 
            WHERE ss.start_time >= '{$fecha_inicial}' AND ss.start_time < '{$fecha_final}' AND (m.mach_name <> '036' AND m.mach_name <> '050' 
            AND m.mach_name <> '051' AND m.mach_name <> '052') AND (ss.user_text_3 = '455900' OR ss.user_text_3 = '394016' OR ss.user_text_3 = '394014'
            OR ss.user_text_3 = '393107' OR ss.user_text_3 = '392408' OR ss.user_text_3 = '392385' OR ss.user_text_3 = '392379' OR ss.user_text_3 = '392378'
            OR ss.user_text_3 = '392377' OR ss.user_text_3 = '392376' OR ss.user_text_3 = '392375' OR ss.user_text_3 = '392374' OR ss.user_text_3 = '392373'
            OR ss.user_text_3 = '391995' OR ss.user_text_3 = '391637' OR ss.user_text_3 = '391636' OR ss.user_text_3 = '391635' OR ss.user_text_3 = '391613'
            OR ss.user_text_3 = '391609' OR ss.user_text_3 = '391178' OR ss.user_text_3 = '391175' OR ss.user_text_3 = '391174' OR ss.user_text_3 = '391151'
            OR ss.user_text_3 = '391150' OR ss.user_text_3 = '391149' OR ss.user_text_3 = '391148' OR ss.user_text_3 = '391139' OR ss.user_text_3 = '391126'
            OR ss.user_text_3 = '391125' OR ss.user_text_3 = '391124' OR ss.user_text_3 = '391123' OR ss.user_text_3 = '391122' OR ss.user_text_3 = '391121'
            OR ss.user_text_3 = '391120' OR ss.user_text_3 = '390943' OR ss.user_text_3 = '390940' OR ss.user_text_3 = '390905' OR ss.user_text_3 = '390904'
            OR ss.user_text_3 = '390903' OR ss.user_text_3 = '390902' OR ss.user_text_3 = '275000' )
            ORDER BY m.mach_name";

    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;
}







private function getYesterday($fecha)
{

    $date_conv = strtotime($fecha);
    $date_yest = strtotime("-1 day",$date_conv);

    return date("Y-m-d",$date_yest);

}




public function getSmedReport($limit=false,$dias=false)
{

    if(!$limit)
    {
        $sql_limit = '';
    }else{
        $limit = (int)$limit;
        $sql_limit = 'LIMIT ' . $limit;
    }


    if(!$dias){
        $duracion = '';
    }else{
        $dia = 86400 * $dias;
        $duracion = "and jts.est_duration < {$dia}";
    }


    $sql =  "SELECT  ss.shift_id, mach.mach_name,  cur.job as current_job, cur.product as current_product, cur.pieces_required as current_pieces,
                 cur.tool as current_tool,que.product as queue_product,
                 jts.net_pieces as jts_net_pieces, jts.est_duration as jts_est_duration,jts.up_time as jts_up_time,
                 jts.offline_state, jts.suspend_state, jts.down_state, jts.pvar_except_state, jts.prod_except_state, jts.assist_state, jts.slow_state, jts.fast_state,
                 que.que_position, que.job as queue_job,  que.product, que.mat_formula, que.tool,que.user_text_4,que.pieces_required, que.est_start_time, que.est_end_time, que.est_duration, que.prod_seq

            FROM mach_def AS mach
            LEFT JOIN job_cur_base AS cur ON (mach.mach_seq = cur.mach_seq and cur.child_idx=0)
            LEFT JOIN jts_cur_base AS jts ON (mach.mach_seq = jts.mach_seq and jts.child_idx=0)
            LEFT JOIN job_que_base AS que ON (mach.mach_seq = que.mach_seq and que.child_idx=0)
            LEFT JOIN ss_cur_base AS ss ON (mach.mach_seq = ss.mach_seq and ss.child_idx=0)
            where  que.que_position = 1 and que.prod_seq <> 16622 and que.prod_seq <> 35897 {$duracion}
            order by jts.prod_except_state desc, jts.est_duration asc {$sql_limit}";
        
    $stmt = $this->adapter->query($sql);
    $result = $stmt->execute();
    return $result;
}


}
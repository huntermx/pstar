<?php

namespace Application\Model\TimeClass;



class ClassDateOp
{

	protected $date2;

	public function __construct($date=false)
	{
		$this->setDate($date);
	}

	public function setDate($date)
	{
		if(!$date)
		{
			$this->date2 = date('Y-m-d');
		}
		$this->date2 = $date;
	}

	public function getToday()
	{
		return $this->date2;
	}



	protected function nextDay()
	{
		$date_conv = strtotime($this->date2);
		$temp_date = strtotime("1 day",$date_conv);
		$next = date("Y-m-d",$temp_date);
		$this->setDate($next);
        return $next;
	}

	protected function prevDay()
	{
		$date_conv = strtotime($this->date2);
		$temp_date = strtotime("-1 day",$date_conv);
		$prev = date("Y-m-d",$temp_date);
		$this->setDate($prev);
        return $prev;
	}

	protected function chckWeekDay()
	{
		$temp = strtotime($this->getToday());
        $week_day = date("D",$temp);
        return $week_day;
	}

	protected function maskdate($mask)
	{
		$date_conv = strtotime($this->date2);
		return date($mask,$date_conv);
	}


	public function getShowDate($mask)
	{
		return $this->maskdate($mask);
	}


	public function getWeekDay()
	{
		return $this->chckWeekDay();
	}


	public function getPrevDay()
	{
		return $this->prevDay();
	}

	public function getNextDay()
	{
		return $this->nextDay();
	}


}
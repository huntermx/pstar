<?php

namespace Application\Model\TimeClass;



class ClassMonthDates
{

	protected $date2;
	protected $year;
	protected $month;
	protected $iniDate;
	protected $endDate;
	protected $yearIniDate;
	protected $yearEndDate;


	public function __construct($year=false,$month=false)
	{
		$this->year = false;
		$this->month = false;
		$this->yearIniDate = false;
		$this->yearEndDate = false;

		if($year)
		{
			$this->setYear($year);
		}
		
		if($month)
		{
			$this->setMonth($month);
		}
	}

	public function setYear($year)
	{
		$this->year = (int) $year;
	}

	public function setMonth($month)
	{
		$this->month = (int) $month;
	}


	private function splitDates()
	{
		if($this->year==FALSE || $this->month==FALSE)
		{

		}
		$this->iniDate = "{$this->year}-{$this->month}-01";
		$temp = cal_days_in_month(CAL_GREGORIAN, $this->month, $this->year);
		$this->endDate = "{$this->year}-{$this->month}-{$temp}";

	}

	private function setYearDates()
	{

		$this->yearIniDate = "{$this->year}-01-01";
		$this->yearEndDate = "{$this->year}-12-31";
	}

	public function getMonthIniDate()
	{
		$this->splitDates();

		return $this->iniDate;

	}

	public function getMonthEndDate()
	{
		$this->splitDates();

		return $this->endDate;

	}


	public function getYearIniDate()
	{
		if(!$this->yearIniDate)
		{
			$this->setYearDates();
		}
		return $this->yearIniDate;
	}


	public function getYearEndDate()
	{
		if(!$this->yearEndDate)
		{
			$this->setYearDates();
		}
		return $this->yearEndDate;
	}

}
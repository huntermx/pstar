<?php

namespace Application\Model\TimeClass;



class ClassQuartDates
{

	protected $date2;
	protected $year;
	protected $month;
	protected $iniDate;
	protected $endDate;
	protected $yearIniDate;
	protected $yearEndDate;


	public function __construct($year=false, $quart=false)
	{
		$this->year = false;
		$this->quart = false;
		$this->yearIniDate = false;
		$this->yearEndDate = false;

		if($year)
		{
			$this->setYear($year);
		}
		
		if($quart)
		{
			$this->setMonth($quart);
		}
	}

	public function setYear($year)
	{
		$this->year = (int) $year;
	}

	public function setQuart($quart)
	{
		$this->quart = (int) $quart;
	}


	private function splitDates()
	{
		if($this->year==FALSE || $this->quart==FALSE)
		{
			throw new Exception('Error there is no valid Year or Quart');
		}
		if($this->quart == 1){
			$month_start = '01';
			$month_end = '03';

		}elseif($this->quart == 2){
			$month_start = '04';
			$month_end = '06';

		}elseif($this->quart == 3){
			$month_start = '07';
			$month_end = '09';
		}elseif($this->quart == 4){
			$month_start = '10';
			$month_end = '12';
		}


		$this->iniDate = "{$this->year}-{$month_start}-01";
		$int_month_end = (int)$month_end;
		$temp = cal_days_in_month(CAL_GREGORIAN, $int_month_end, $this->year);
		$this->endDate = "{$this->year}-{$month_end}-{$temp}";

	}

	private function setYearDates()
	{

		$this->yearIniDate = "{$this->year}-01-01";
		$this->yearEndDate = "{$this->year}-12-31";
	}

	public function getQuartIniDate()
	{
		$this->splitDates();

		return $this->iniDate;

	}

	public function getQuartEndDate()
	{
		$this->splitDates();

		return $this->endDate;

	}


	public function getYearIniDate()
	{
		if(!$this->yearIniDate)
		{
			$this->setYearDates();
		}
		return $this->yearIniDate;
	}


	public function getYearEndDate()
	{
		if(!$this->yearEndDate)
		{
			$this->setYearDates();
		}
		return $this->yearEndDate;
	}



}
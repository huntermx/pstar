<?php

namespace Application\Model\TimeClass;


class DisplayMenu extends MonthToDisplay
{
	protected $menu;
	protected $location;
	private $currentYear;

	public function __construct($year,$month=false,$monthIni=false,$location=false)
	{
		parent::__construct($year,$month,$monthIni);    
		$this->menu = false;
		$this->currentYear = date("Y");

	}


	public function setLocation($location)
	{
		if(!$location)
		{
			$this->location = 'TJ';
		}else{
			$this->location = $location;
		}

	}


	private function checkYears()
	{

		$temp = date("Y");

		for($i=2012; $i<=$this->currentYear; $i++)
		{
	    	 $this->setYear($i);
			if($i==2012)
			{
				$this->setMonthIni(3);
			}


			$this->menu[] = $this->getList();
		}



	}


	public function  getMenu()
	{
		
		if(!$this->menu)
		{
			$this->checkYears();
		}
		return $this->menu;
	}


}
	
<?php

namespace Application\Model\TimeClass;



class MonthPickerLastDates
{
	private $date;
	private $year;
	private $month;
	private $monthsDisableArray;

	public function __construct($date =FALSE)
	{
		$this->SetDate($date);
		$this->monthsDisableArray =FALSE;
	}

	public function SetDate($date)
	{
		$this->date = $date;
	}


	private function checkDate()
	{
		if(!$this->date)
		{
			$this->setDate(date('Y-m-d'));
		}
		list($this->year, $this->month,$temp) = explode('-',$this->date);
		$this->DisableMonthsArray();
	}


	private function DisableMonthsArray()
	{
		$this->monthsDisableArray = array();
		for($i=$this->month; $i<=12; $i++)
		{
			$this->monthsDisableArray[] = $i;
		}
	}

	public function getYear()
	{
		if($this->monthsDisableArray ==FALSE)
		{
			$this->checkDate();
			//$this->DisableMonthsArray();
		}
		return $this->year;
	}


	public function  getDisableMonthsArray()
	{
		if($this->monthsDisableArray ==FALSE)
		{
			$this->checkDate();
		//	$this->DisableMonthsArray();
		}
		return $this->monthsDisableArray;
	}

}

<?php

namespace Application\Model\TimeClass;


class MonthToDisplay
{
	
	protected $year;
	protected $current_month;
	protected $list;
	protected $monthIni;
	

	public function __construct($year = false,$month = false,$monthIni=false)
	{
	    
	    $this->setMonthIni($monthIni);
	    $this->setYear($year);
		$this->setMonth($month);
		$this->list = false;
	}


	public function setMonthIni($month=false)
	{
		if(!$month)
		{
			$this->monthIni = 1;
		}else{
			$this->monthIni = $month;
		}
	}


	public function setYear($year=false)
	{
		$this->list = false;
		$this->month = false;
		$this->monthIni = false;
		if(!$year)
		{
			$this->year = date("Y");
		}else{
			$this->year = $year;
		}
	}


	public function setMonth($month=false)
	{
		if(!$month)
		{
			$this->current_month = date("m");
		}else{
			$this->current_month = $month;
		}
	}

	protected function getMonth()
	{
		$year = date('Y');

		if($this->year == $year)
		{
			$month = (int)date('m');
			$list = $this->setList($this->monthIni,$month);

		}else{
			
			$list = $this->setList($this->monthIni,12);
		}

		$this->list = $list;

	}

	protected function setList($monthini=false,$month)
	{
		
		if(!$monthini)
		{
			$startMonth = 1;
		}else{
			$startMonth = $monthini;
		}
		

		$meses = array('1' => 'Ene',
					   '2' => 'Feb',
					   '3' => 'Mar',
					   '4' => 'Abr',
					   '5' => 'May',
					   '6' => 'Jun',
					   '7' => 'Jul',
					   '8' => 'Ago',
					   '9' => 'Sep',
					   '10' => 'Oct',
					   '11' => 'Nov',
					   '12' => 'Dec',
			);
		$list = array();
		for($i=$startMonth; $i<=12; $i++)
		{
			$list[$i.'-'.$this->year] = $meses[$i] . '/' . $this->year;

			if($month==$i)
			{
				break;
			}
		}
		return $list;
	}

	public function getList()
	{
		if(!$this->list){
			$this->getMonth();
		}
		return $this->list;
	}

}
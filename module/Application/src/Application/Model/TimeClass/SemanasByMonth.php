<?php

namespace Application\Model\TimeClass;


class SemanasByMonth
{
	
	protected $year;
	protected $current_month;
	protected $list;
	protected $flag_current=false;
	

	public function __construct($year = '2013',$month = false)
	{
		$this->setYear($year);

		$this->setMonth($month);
		// Si el mes actual es igual encender flag
		if($this->current_month == date('m') & $this->year == date("Y"))
		{
			$this->flag_current = true;
		}

		
		$this->list = false;
	}



	public function setYear($year)
	{
		if(!$year)
		{
			$this->year = date("Y");
		}else{
			$this->year = $year;
		}
		$this->flag_current = false;
		$this->list = false;
	}


	public function setMonth($month)
	{
		if(!$month)
		{
			$this->current_month = date("m");
		}else{
			$this->current_month = $month;
		}
		
	}


	public function getfirstSabado($year, $month)
	{

		$date_semanas 	= $year . '-' . $this->getMonthZero($month) . '-01';
		$weekday 		= date('w', strtotime($date_semanas));    // note: first arg to date() is lower-case L  
		

		if($weekday == 0)
		{
			return $date_semanas;
		}
		$i = 1;
		$date_conv = strtotime($date_semanas);
		
		while($weekday>0)
		{
			$weekday--;
            $date_yest = strtotime("-$i day",$date_conv);
			$temp = date('Y-m-d',$date_yest);
			$i++;
		}

		return $temp;
	}


	private function getMonthZero($month)
	{
		$month= (string)$month; 
		$temp = strlen($month);
		if($temp==1)
		{
			return '0' . $month;
		}
		return $month;

	}




	public function getSemanas()
	{

		$firstSunday = $this->getfirstSabado($this->year, $this->current_month);
		$next = $this->current_month + 1;
		$year = $this->year;
		if($next == 13)
		{
			$next = 1;
			$year = $this->year +1;
		}


		if(!$this->flag_current)
		{
	
			$end_date = $this->getfirstSabado($year, $next);
		}else{

			$end_date = date('Y-m-d');
		}
		

		return $this->getWeeks($firstSunday,$end_date);

	}



	public function getAllYearWeeks()
	{
		$firstSunday = $this->getfirstSabado($this->year, 1);
		$next = $this->current_month + 1;
		$year = $this->year;
		if($next == 13)
		{
			$next = 1;
			$year = $this->year +1;
		}

		$end_date = date('Y-m-d');

		return $this->getWeeks($firstSunday,$end_date);
	}
	


	protected function getWeeks($begin,$end)
	{
		$i = 0;
		$temp = $begin;

		do{

			$list[$i]['start'] = $temp;
			$date_conv = strtotime($temp);
			$date_temp= strtotime("+6 day",$date_conv);
			$date_next= strtotime("+7 day",$date_conv);
			$list[$i]['end'] = date('Y-m-d',$date_temp);
			$temp = date('Y-m-d',$date_next);

			$i++;
		}while($temp < $end );

		return $list;
	}


}
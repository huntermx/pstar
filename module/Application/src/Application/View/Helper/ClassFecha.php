<?php

namespace Application\View\Helper;


class ClassFecha
{
	protected $fecha=false;
	protected $time=false;
	protected $timestamp;
	protected $tempfecha;
	protected $schar;
	protected $temptime;
	protected $meses_lista=array('01' => 'Jan','02' => 'Feb',
			'03' => 'Mar','04' => 'Apr','05' => 'May','06' => 'Jun','07' => 'Jul',
			'08' => 'Aug','09' => 'Sep','10' => 'Oct','11' => 'Nov','12' => 'Dec');
	/*protected $meses_lista=array('01' => 'Ene','02' => 'Feb',
			'03' => 'Mar','04' => 'Abr','05' => 'May','06' => 'Jun','07' => 'Jul',
			'08' => 'Ago','09' => 'Sep','10' => 'Oct','11' => 'Nov','12' => 'Dic');*/
	
	
	public function __construct($timestamp, $selector = false,$schar = false)
	{		
		$this->timestamp = $timestamp;
		$this->validaTimestamp();
		if($schar)
		{
			$this->schar = $schar;
		}
		else
		{
			$this->schar = " ";
		}


		
	}
	
	private function validaTimestamp()
	{
		if (preg_match('(\d{4}-\d{2}-\d{2})', $this->timestamp) & strlen($this->timestamp)==10) {
			$this->tempfecha = $this->timestamp;
			$this->temptime = false;
		}else if (preg_match('(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})', $this->timestamp)) {
			list($this->tempfecha,$this->time) = explode(' ',$this->timestamp);
			$this->temptime = true;			
		} else {

			$this->tempfecha = true;
		}
	}


	public function setSeparador($char)
	{

	}
	
	public function setFormatoMDA()
	{
		if($this->tempfecha===true){
			$this->fecha='Fecha Invalida';
			return false;
		}		
		list($anio,$mes,$dia) = explode('-',$this->tempfecha);
		$this->fecha = "{$this->meses_lista[$mes]}" . "$this->schar" . "{$dia}" . "$this->schar" . "{$anio}";

	}
	
	public function setFormatoTime($var=true)
	{
		if($var == true && $this->tempfecha!==true)
		{
			$this->validaTimestamp();
		}else{
			$this->temptime = false;
		}
	}
	
	public function setTimeFormatAMPM()
	{
		if($this->temptime==false || $this->tempfecha===true)
		{
			return false;
		}
		$this->time= date('h:iA', strtotime($this->timestamp));
	}
	
	public function getFecha()
	{
		if($this->temptime)
		{
			$this->fecha= $this->fecha . ' ' . $this->time;
		}
		
		return $this->fecha;
		
	}
	
}

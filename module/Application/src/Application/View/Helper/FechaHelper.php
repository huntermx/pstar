<?php
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

class FechaHelper extends AbstractHelper
{

	public function __invoke($timestamp,$schar = false)
	{
		$fclass = new ClassFecha($timestamp,false,$schar);	
		$fclass->setFormatoMDA();
		$fclass->setTimeFormatAMPM();
		return $fclass->getFecha();
	}
	
}
<?php

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;

class HourFormatHelper extends AbstractHelper
{

	public function __invoke($second)
	{
			$hours= $second/3600;
			$tiempo = number_format($hours,2);
            return $tiempo;
	}
	
}
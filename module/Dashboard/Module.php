<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Dashboard;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Application\Model\MyAdapterFactory;


use Application\Model\Dao\MoldingCapacityDao;
use Application\Model\Dao\MoldeoEficienciaDao;
use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Dao\MachUsageDTotalDao;

use Application\Model\Metrics\UpdateMachUsage;
use Application\Model\PStarDb\PlantstarData;

use Application\Model\Entity\MoldingCapacity;
use Application\Model\Entity\MoldeoEficiencia;
use Application\Model\Entity\MachUsageDaily;
use Application\Model\Entity\MachUsageDTotal;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


 public function initConfig($e)
    {
        $application =  $e->getApplication();
        $services    =  $application->getServiceManager();
        
        $services->setFactory('ConfigIni', function($services){
            $reader = new Ini();
            $data   = $reader->fromFile(__DIR__ . '/config/config.ini');
            return $data;
        });
    } 

    public function getServiceConfig()
    {

        return array(
                'factories' => array(
                'myadapter2'        => new MyAdapterFactory('dbconfigkey2'),
               
                )
                );
    }


}

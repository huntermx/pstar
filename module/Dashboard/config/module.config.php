<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
        'controllers' => array(
        'invokables' => array(
            'Dashboard\Controller\Availabilityhc' => 'Dashboard\Controller\AvailabilityhcController',
            'Dashboard\Controller\Efficiencyhc' => 'Dashboard\Controller\EfficiencyhcController',
            'Dashboard\Controller\Utilizationhc' => 'Dashboard\Controller\UtilizationhcController',
            'Dashboard\Controller\Dashboardshc' => 'Dashboard\Controller\DashboardshcController',
            'Dashboard\Controller\Downtimehc' => 'Dashboard\Controller\DowntimehcController',
            'Dashboard\Controller\Machineusagehc' => 'Dashboard\Controller\MachineusagehcController',
        ),
    ),

    'router' => array(
               'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Dashboard\Controller\Availabilityhc',
                        'action'     => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'availability' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/availability',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Availabilityhc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action][/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),






 'efficiency' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/efficiency',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Efficiencyhc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),


 'utilization' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/utilization',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Utilizationhc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),


'machineusagehc' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/machineusagehc',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Machineusagehc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),


'dashboards' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/dashboards',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Dashboardshc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),


'dashboardshc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/dashboardshc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Dashboardshc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),



'downtime' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/downtime',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Dashboard\Controller',
                        'controller'    => 'Downtimehc',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),


'downt' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/downt',
                'defaults' => array(
                '__NAMESPACE__' => 'Dashboard\Controller',
                    'controller' => 'Downtimehc',
                    'action' => 'Daterange',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/downt[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),



'dashbs' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/dashbs',
                'defaults' => array(
                '__NAMESPACE__' => 'Dashboard\Controller',
                    'controller' => 'Dashboardshc',
                    'action' => 'Daterange',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/dashbs[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),


'dashbshc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                      'route'    => '/dashbshc2[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                        'controller' => 'Dashboard\Controller\Dashboardshc',
                        'action' => 'index',
                    ),
                    ),
      ),



'util' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/util',
                'defaults' => array(
                '__NAMESPACE__' => 'Dashboard\Controller',
                    'controller' => 'Utilizationhc',
                    'action' => 'Daterange',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/util[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),


'effic' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/effic',
                'defaults' => array(
                '__NAMESPACE__' => 'Dashboard\Controller',
                    'controller' => 'Efficiencyhc',
                    'action' => 'Daterange',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '[/:lang]/effic[/:action][/:id]',
                    'constraints' => array(
                        'lang'   => '[a-z]{2}(-[A-Z]{2}){0,1}',
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),
       



'downtimehc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/downtimehc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Downtimehc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),


'downtimehc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                      'route'    => '/downtimehc[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Downtimehc',
                        'action' => 'index',
                    ),
                    ),
      ),

'utilizationhc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/utilizationhc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Utilizationhc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),

'utilizationhc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                      'route'    => '/utilizationhc[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Utilizationhc',
                        'action' => 'index',
                    ),
                    ),
      ),

 //utilizationhc
'utilizationhc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/utilizationhc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Utilizationhc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),


 'utilizationhc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/utilizationhc[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                       

                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Utilizationhc',
                        'action' => 'index',
                    ),
                    ),
      ),


'efficiencyhc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/efficiencyhc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Efficiencyhc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),




   'efficiencyhc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/efficiencyhc[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                       

                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Efficiencyhc',
                        'action' => 'index',
                    ),
                    ),
      ),


    'availabilityhc' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/availabilityhc[/:action][/:id]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Availabilityhc',
                        'action' => 'Daterange',
                    ),
                    ),
      ),
     

    'availabilityhc2' => array(
               'type' => 'Segment',
               'options' => array(
              
                            'route'    => '/availabilityhc[/:action][/:id]/date[/:date]/date2[/:date2]',
                   // 'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                       

                        ),
                    'defaults' => array(
                          'controller' => 'Dashboard\Controller\Availabilityhc',
                        'action' => 'index',
                    ),
                    ),
      ),




    'avail' => array(
               'type' => 'Literal',
               'options' => array(
               'route' => '/avail',
                'defaults' => array(
                '__NAMESPACE__' => 'Dashboard\Controller',
                    'controller' => 'Availabilityhc',
                    'action' => 'daterange',
                ),
                ),
            'may_terminate' => true,
                'child_routes' => array(
                'default' => array(
                    'type' => 'Segment',
                        'options' => array(
                    'route'    => '/[:controller[/:action][/:id]]',
                    'constraints' => array(
                           'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                        ),
                    'defaults' => array(
                    ),
                    ),
      ),
     ),
     ),


        ),
    ),




    'service_manager' => array(
         'factories' => array(
           
            'Navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
           
            
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),

    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
       
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),


  

);

<?php

namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;

use Application\Controller\AbstractApplicationController;

use Dashboard\Forms\FormYearstoValidator;

use Dashboard\Forms\FormDataRange;
use Dashboard\Forms\FormMoldingHCM;
use Dashboard\Forms\FormYearstoDate;

abstract class AbstractDashboardController extends AbstractApplicationController
{
	protected $formDates;
    protected $formHCM;
    protected $moldEfficPropDao;
    protected $formYearlyQuart;

    public function getDatesForm()
    {
        if (!$this->formDates) {
            $temp            = new FormDataRange();
            $builder         = new AnnotationBuilder();
            $this->formDates    = $builder->createForm($temp);
        }
        return $this->formDates;
    }

    public function FormYearQuarts()
    {
         if (!$this->formYearlyQuart) 
         {
           $this->formYearlyQuart= new FormYearstoDate();
        }
        return $this->formYearlyQuart;
    }

    public function getHCMForm()
    {
        if (!$this->formHCM) {
            $temp            = new FormMoldingHCM();
            $builder         = new AnnotationBuilder();
            $this->formHCM    = $builder->createForm($temp);
        }
        return $this->formHCM;
    }

     public function getMoldingEfficenciyPropDao()
    {
        if (!$this->moldEfficPropDao) {
            $sm = $this->getServiceLocator();
            $this->moldEfficPropDao = $sm->get('Application\Model\Dao\MoldeoEficienciaPropDao');
        }
       return $this->moldEfficPropDao;
    }


     protected function getMinValue($objArray,$fixedValue = false)
    {

        if(!$fixedValue)
        {
            $fix = 0;
        }else{
            $fix = $fixedValue;
        }

        $temp = array();
        $objNumber = count($objArray);

        if($objNumber==1)
        {
             $temp[] = $objArray->getArrayMin();
        }
        else if($objNumber >1)
        {
            foreach($objArray as $obj)
            {
                $temp[] = $obj->getArrayMin();
            }
        }

        return min($temp) + $fix;
    }


    protected function getMaxValue($objArray,$fixedValue = false)
    {

        if(!$fixedValue)
        {
            $fix = 0;
        }else{
            $fix = $fixedValue;
        }

        $temp = array();
      $objNumber = count($objArray);

        if($objNumber==1)
        {
             $temp[] = $objArray->getArrayMax();
        }
        else if($objNumber >1)
        {
            foreach($objArray as $obj)
            {
                $temp[] = $obj->getArrayMax();
            }
        }

        return max($temp) + $fix;
    }



}
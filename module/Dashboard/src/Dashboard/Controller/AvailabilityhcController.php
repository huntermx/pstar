<?php

namespace Dashboard\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Dashboard\Model\Availability\AvailabilityFormat;
use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\Availability\YearMonthAvailability;

use Dashboard\Model\HighchartsWapper\HcAxisY;
use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;
use Dashboard\Model\Statics\RollingAverage;

use Application\Model\TimeClass\ClassMonthDates;





class AvailabilityhcController extends AbstractDashboardController
{


    public function indexAction()
    {
    		//Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $dias = 30;
    	$now_day = date("Y-m-d");
    	$yesterday = date("Y-m-d",strtotime('-1 day'));
        $form          = $this->getHCMForm();
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objAv = new AvailabilityFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#CF8A24");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail[0]));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmindex',
                            'avgAction' => 'index',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'index'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonth'),),
                ));
        $view->setTemplate('dashboard/availabilityhc/indexrolling2.phtml');
        return $view;
    }


    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'index','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'hcmgraph','id'=> 4));
                }
                return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'propgraph','id'=> 4));
            }
        }
    }


    public function HcmgraphAction()
    {
            //Last 30 Days  HCM
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(2); 
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getECByDates($pastime, $yesterday, 30);


        $objAv = new AvailabilityFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#CF8A24");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail[0]));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmindex',
                            'avgAction' => 'Hcmgraph',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'indexhcm'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonthhcm'),),
                ));
        $view->setTemplate('dashboard/availabilityhc/indexrolling2.phtml');
        return $view;
    }


    public function PropgraphAction()
    {
        //Last 30 Days  PROP
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(3); 
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($pastime, $yesterday, 30);


        $objAv = new AvailabilityFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#CF8A24");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail[0]));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'avgAction' => 'Propgraph',
                            'action' => 'Hcmindex',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'indexprop'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonthprop'),),
                ));
        $view->setTemplate('dashboard/availabilityhc/indexrolling2.phtml');
        return $view;

    }


   public function DaterangerollingAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
                
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new AvailabilityFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling ,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#46ABD1");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $form          = $this->getHCMForm();

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentage (%)',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmdaterange',
                            'avgAction' => 'daterangerolling',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'index'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonth'),),
        ));
        $view->setTemplate('dashboard/availabilityhc/indexdateRolling.phtml');
        return $view;
           
   }






    public function DaterangeAction()
    {

        $request    = $this->getRequest();
        $form       = $this->getDatesForm();

        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            
            if($form->isValid())
            {
                $array_post = $form->getData();
                $fechaIni = $array_post['fecha'];
                $fechaFin = $array_post['fecha2'];
                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objAv = new AvailabilityFormat($arrayTemp);
                
                $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
                $hcAvail[0]->setName("Molding");
                $hcAvail[0]->setColor("#CF8A24");
                $hcAvail[0]->setType("column");
                $hcAvail[0]->setColumnDataLabels(true);

                $hcSGlue = new HcSeriesGlue();
                $hcSGlue->setObjSeries($hcAvail[0]);

                $hcCateg = new HcCategories($objAv->getArrayDates());
                $arrayValues = $objAv->getArrayAvailability();
                $numElements = count($arrayValues);
                $objRolling = new RollingAverage($arrayValues,4,$numElements);

                $hcCateg = new HcCategories($objAv->getArrayDates());
                $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
                $hcAvail[1]->setName("Rolling Avg");
                $hcAvail[1]->setType("line");
                $hcAvail[1]->setColor("#46ABD1");
                $hcAvail[1]->setMarker(TRUE);
                $hcAvail[1]->setColumnDataLabels(false);
                $hcSGlue->setObjSeries($hcAvail[1]);
                $form          = $this->getHCMForm();

                $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentage (%)',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmdaterange',
                            'avgAction' => 'daterangerolling',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'index'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonth'),),
                ));

                $view->setTemplate('dashboard/availabilityhc/indexdateRolling.phtml');
                return $view;
            }
        }

         return new ViewModel(array(
             'subtitle' => "(Choose a Date Range)",
             'title' =>  "Availability",
             'form' => $form,
                
        ));
    }


    public function HcmdaterangeAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('availabilityhc2', array('controller'=> 'Availabilityhc', 'action'=> 'Daterangerolling','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('availabilityhc2', array('controller'=> 'Availabilityhc', 'action'=> 'Daterangerollinghcm','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                return $this->redirect()->toRoute('availabilityhc2', array('controller'=> 'Availabilityhc', 'action'=> 'Daterangerollingprop','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
            }
        }
    }


    public function DaterangerollinghcmAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $objAv = new AvailabilityFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

         $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentage (%)',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmdaterange',
                            'avgAction' => 'daterangerollinghcm',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'indexhcm'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonthhcm'),),
        ));
        $view->setTemplate('dashboard/availabilityhc/indexdateRolling.phtml');
        return $view;
    }

    public function DaterangerollingpropAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

      //  $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new AvailabilityFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Proprietary Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("column");
        $hcAvail[0]->setColumnDataLabels(true);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(false);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Availability Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentage (%)',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmdaterange',
                            'avgAction' => 'daterangerollingprop',
                            'filterData' =>  array("last30"     => array('url' => 'availability', 'controller' => 'Availabilityhc', 'action'=> 'indexprop'),
                                                  "monthly"    => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'avail/default','controller' => 'Availabilityhc', 'action'=> 'Yearmonthprop'),),
        ));
        $view->setTemplate('dashboard/availabilityhc/indexdateRolling.phtml');
        return $view;
    }



   public function HcmmontlyAction()
   {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Montlygraph','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Montlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Montlygraphprop','id'=> 4));
            }
        }
    }



    public function MontlygraphAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyDao(),"english");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Molding");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();

        $hcSGlue->setObjSeries($hcAvail);
        $form       = $this->getHCMForm();

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Monthly Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmontly',

                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }


    public function MontlygraphhcmAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Molding");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();

        $hcSGlue->setObjSeries($hcAvail);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Monthly Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                             'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmontly',

                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }


    public function MontlygraphpropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","PROP");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Molding");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();

        $hcSGlue->setObjSeries($hcAvail);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

         $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Monthly Plantstar Availability Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                             'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmontly',

                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }



    private function objToArray($obj)
    {
        $arrayTemp = array();
        while($row = $obj->current())
        {
            print_r($row);
            $arrayTemp[] =$row;
        }

        return $arrayTemp;
    }


    public function HcmyearlyAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearlygraph','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearlygraphprop','id'=> 4));
            }
        }
    }


    public function YearlygraphAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthAvailability($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }

        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $form       = $this->getHCMForm();

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,0));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmyearly',
                ));

                $view->setTemplate('dashboard/availabilityhc/index.phtml');
                return $view;
    }


    public function YearlygraphhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }

        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,0));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmyearly',
                ));

                $view->setTemplate('dashboard/availabilityhc/index.phtml');
                return $view;
    }


    public function YearlygraphpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }

        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,0));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Availability Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                             'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmyearly',
                ));

                $view->setTemplate('dashboard/availabilityhc/index.phtml');
                return $view;
    }


    public function YearmonthAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#005487','#DC4520','#46ABD1','#4B3F3B');
        $hcSGlue = new HcSeriesGlue();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthAvailability($i, $this->getMoldingEfficenciyDao(),"english");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);


        }

        $form       = $this->getHCMForm();
        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Availability',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }

    public function HcmmyearmonthAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearmonth','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearmonthhcm','id'=> 4));
                }
              
                return $this->redirect()->toRoute('availabilityhc', array( 'action'=> 'Yearmonthprop','id'=> 4));

            }
        }
    }


    public function YearmonthhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#005487','#DC4520','#46ABD1','#4B3F3B');
        $hcSGlue = new HcSeriesGlue();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);


        }

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 
        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }


    public function YearmonthpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#005487','#DC4520','#46ABD1','#4B3F3B');
        $hcSGlue = new HcSeriesGlue();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);

        }

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 
        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail,70));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Availability Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => $objHcAxisY,
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/availabilityhc/yearmonth.phtml');
        return $view;
    }



}
 /*$view = new ViewModel(array(
                            'title' => 'Monthly Plantstar Availability HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                             'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmontly',

                ));
*/
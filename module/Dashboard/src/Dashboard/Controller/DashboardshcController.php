<?php
namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;


use Dashboard\Model\Availability\AvailabilityFormat;
use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\Availability\YearMonthAvailability;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Utilization\UtilizationFormat;
use Dashboard\Model\Downtime\DowntimeFormat;

use Dashboard\Model\HighchartsWapper\HcAxisY;
use Dashboard\Model\HighchartsWapper\HcTitle;
use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;

use Dashboard\Model\Statics\RollingAverage;
use Dashboard\Model\Utilization\YearMonthUtilization;
use Dashboard\Model\Efficiency\YearMonthEfficiency;
use Dashboard\Model\Downtime\YearMonthDowntime;

use Application\Model\TimeClass\ClassMonthDates;



class DashboardshcController extends AbstractDashboardController
{


    public function indexAction()
    {
    		//Last 30 Days
        $dias          = 30;
    	$now_day       = date("Y-m-d");
    	$yesterday     = date("Y-m-d",strtotime('-1 day'));

        $date_conv     = strtotime($now_day);
        $date_yest     = strtotime("-{$dias} day",$date_conv);
        $pastime       = date("Y-m-d",$date_yest);
        $form          = $this->getHCMForm();

        $arrayTemp     = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $hcSGlue       = new HcSeriesGlue();

        $objAv         = new AvailabilityFormat($arrayTemp);
        $hcAvail[0]    = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);


        $arrayTemp     = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objEff        = new EfficiencyFormat($arrayTemp);
        $hcAvail[1]    = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);

        $arrayTemp     = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objUtil       = new UtilizationFormat($arrayTemp);
        $hcAvail[2]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);
        

        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objUtil       = new DowntimeFormat($arrayTemp);
        $hcAvail[3]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);

        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);

        //$objRolling = new RollingAverage($arrayValues,4,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));


        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboard',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmindex',
                ));

        $view->setTemplate('dashboard/dashboardshc/index.phtml');
        return $view;
    }


      public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'index','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'hcmgraph','id'=> 4));
                }
                return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'propgraph','id'=> 4));
            }
        }
    }


    public function HcmgraphAction()
    {
        $dias          = 30;
        $now_day       = date("Y-m-d");
        $yesterday     = date("Y-m-d",strtotime('-1 day'));

        $date_conv     = strtotime($now_day);
        $date_yest     = strtotime("-{$dias} day",$date_conv);
        $pastime       = date("Y-m-d",$date_yest);
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(2);  
        
       
        $hcSGlue       = new HcSeriesGlue();     
        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getECByDates($pastime, $yesterday, 30);
        $objAv = new AvailabilityFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(true);

        $arrayTemp     = $this->getMoldingEfficenciyHCMDao()->getECByDates($pastime, $yesterday, 30);
        $objEff        = new EfficiencyFormat($arrayTemp);
        $hcAvail[1]    = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);

        $arrayTemp     = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($pastime, $yesterday, 30);
        $objUtil       = new UtilizationFormat($arrayTemp);
        $hcAvail[2]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($pastime, $yesterday, 30);
        $objUtil       = new DowntimeFormat($arrayTemp);
        $hcAvail[3]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);


        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));


        $hcCateg = new HcCategories($objAv->getArrayDates());

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboard',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY ,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmindex',
                ));


        $view->setTemplate('dashboard/dashboardshc/index.phtml');
        return $view;

    }


    public function PropgraphAction()
    {
            //L
        $dias          = 30;
        $now_day       = date("Y-m-d");
        $yesterday     = date("Y-m-d",strtotime('-1 day'));

        $date_conv     = strtotime($now_day);
        $date_yest     = strtotime("-{$dias} day",$date_conv);
        $pastime       = date("Y-m-d",$date_yest);
        $form          = $this->getHCMForm();
        $form->get('molding')->setValue(3);  


        
        $hcSGlue       = new HcSeriesGlue();
        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($pastime, $yesterday, 30);
        $objAv = new AvailabilityFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(true);    



        $arrayTemp     = $this->getMoldingEfficenciyPropDao()->getECByDates($pastime, $yesterday, 30);
        $objEff        = new EfficiencyFormat($arrayTemp);
        $hcAvail[1]    = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);


        $arrayTemp     = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($pastime, $yesterday, 30);
        $objUtil       = new UtilizationFormat($arrayTemp);
        $hcAvail[2]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($pastime, $yesterday, 30);
        $objUtil       = new DowntimeFormat($arrayTemp);
        $hcAvail[3]    = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);

        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));


        $hcCateg = new HcCategories($objAv->getArrayDates());

         $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboard',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmindex',
                ));


        $view->setTemplate('dashboard/dashboardshc/index.phtml');
        return $view;
    }


    public function HcmdaterangeAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('dashbshc2', array('controller'=> 'Dashboardshc', 'action'=> 'Daterangerolling','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                if($post['molding']==2)



                {
                    return $this->redirect()->toRoute('dashbshc2', array('controller'=> 'Dashboardshc', 'action'=> 'Daterangerollinghcm','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                return $this->redirect()->toRoute('dashbshc2', array('controller'=> 'Dashboardshc', 'action'=> 'Daterangerollingprop','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
            }
        }
    }


    public function DaterangeAction()
    {

        $request    = $this->getRequest();
        $form       = $this->getDatesForm();

        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            

            if($form->isValid())
            {
                
                $array_post = $form->getData();
                $fechaIni = $array_post['fecha'];
                $fechaFin = $array_post['fecha2'];
                $hcSGlue = new HcSeriesGlue();
                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);

                $objAv = new AvailabilityFormat($arrayTemp);
                $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
                $hcAvail[0]->setName("Availability");
                $hcAvail[0]->setColor("#005487");
                $hcAvail[0]->setType("line");
                $hcAvail[0]->setColumnDataLabels(false);
                $hcSGlue->setObjSeries($hcAvail[0]);

                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objEff = new EfficiencyFormat($arrayTemp);
                $hcAvail[1] = new HcSerie($objEff->getArrayAvailability());
                $hcAvail[1]->setName("Efficiency");
                $hcAvail[1]->setColor("#DC4520");
                $hcAvail[1]->setType("line");
                $hcAvail[1]->setColumnDataLabels(false);
                $hcSGlue->setObjSeries($hcAvail[1]);

                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objUtil = new UtilizationFormat($arrayTemp);
                $hcAvail[2] = new HcSerie($objUtil->getArrayAvailability());
                $hcAvail[2]->setName("Utilization");
                $hcAvail[2]->setColor("#CF8A24");
                $hcAvail[2]->setType("line");
                $hcAvail[2]->setColumnDataLabels(false);
                $hcSGlue->setObjSeries($hcAvail[2]);

                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objUtil = new DowntimeFormat($arrayTemp);
                $hcAvail[3] = new HcSerie($objUtil->getArrayAvailability());
                $hcAvail[3]->setName("DownTime");
                $hcAvail[3]->setColor("#000");
                $hcAvail[3]->setType("line");
                $hcAvail[3]->setColumnDataLabels(false);
                $hcSGlue->setObjSeries($hcAvail[3]);


                $hcCateg = new HcCategories($objAv->getArrayDates());
                $arrayValues = $objAv->getArrayAvailability();
                $hcCateg = new HcCategories($objAv->getArrayDates());

                $objHcAxisY = new HcAxisY();
                $objHcAxisY->Title->value = 'Percentage (%)';
                $objHcAxisY->setProperty("minorTickInterval",10);
                $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
                $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
                $form          = $this->getHCMForm();
                $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboards',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'gYtitle' => $objHcAxisY,
                            'hcmForm' => $form,
                            'action' => "Hcmdaterange",   
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


                $view->setTemplate('dashboard/dashboardshc/indexDateRange.phtml');
                return $view;
            }
        }

         return new ViewModel(array(
             'subtitle' => "(Choose a Date Range)",
             'title' =>  "Dashboards",
             'form' => $form,
                
        ));
    }



   public function DaterangerollingAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        
        $hcSGlue = new HcSeriesGlue();
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new AvailabilityFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objEff = new EfficiencyFormat($arrayTemp);
        $hcAvail[1] = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objUtil = new UtilizationFormat($arrayTemp);
        $hcAvail[2] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[2]);

        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objUtil = new DowntimeFormat($arrayTemp);
        $hcAvail[3] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
      //  $objRolling = new RollingAverage($arrayValues,4,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
        $form          = $this->getHCMForm();
        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboards',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                             'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'gYtitle' => $objHcAxisY,
                            'hcmForm' => $form,
                             'action' => "Hcmdaterange",   
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/dashboardshc/indexDateRange.phtml');
        return $view;
   }


   public function DaterangerollinghcmAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  


        $hcSGlue = new HcSeriesGlue();
        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);

        $objAv = new AvailabilityFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getECByDates($fechaIni, $fechaFin, false);
        $objEff = new EfficiencyFormat($arrayTemp);
        $hcAvail[1] = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $objUtil = new UtilizationFormat($arrayTemp);
        $hcAvail[2] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[2]);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $objUtil = new DowntimeFormat($arrayTemp);
        $hcAvail[3] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $hcCateg = new HcCategories($objAv->getArrayDates());

        $arrayValues = $objAv->getArrayAvailability();
        $hcCateg = new HcCategories($objAv->getArrayDates());
        
        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
        $form          = $this->getHCMForm();
        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboards',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                             'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'hcmForm' => $form,
                             'action' => "Hcmdaterange",   
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
            ));
        $view->setTemplate('dashboard/dashboardshc/indexDateRange.phtml');
        return $view;
    }


    public function DaterangerollingpropAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

        $hcSGlue = new HcSeriesGlue();
        
        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new AvailabilityFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($fechaIni, $fechaFin, false);
        $objEff = new EfficiencyFormat($arrayTemp);
        $hcAvail[1] = new HcSerie($objEff->getArrayAvailability());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[1]);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($fechaIni, $fechaFin, false);
        $objUtil = new UtilizationFormat($arrayTemp);
        $hcAvail[2] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("line");
        $hcAvail[2]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[2]);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($fechaIni, $fechaFin, false);
        $objUtil = new DowntimeFormat($arrayTemp);
        $hcAvail[3] = new HcSerie($objUtil->getArrayAvailability());
        $hcAvail[3]->setName("DownTime");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("line");
        $hcAvail[3]->setColumnDataLabels(false);
        $hcSGlue->setObjSeries($hcAvail[3]);

        $hcCateg = new HcCategories($objAv->getArrayDates());

        $arrayValues = $objAv->getArrayAvailability();
        $hcCateg = new HcCategories($objAv->getArrayDates());
        
        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));
        $form          = $this->getHCMForm();
        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Dashboards',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                             'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'hcmForm' => $form,
                             'action' => "Hcmdaterange",   
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
            ));
        $view->setTemplate('dashboard/dashboardshc/indexDateRange.phtml');
        return $view;
   }



    public function HcmmontlyAction()
   {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Montlygraph','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Montlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Montlygraphprop','id'=> 4));
            }
        }
    }


    public function MontlygraphhcmAction()
    {
            $currentMonth = (int)date("n");
            $currentYear= (int)date("Y");
            $form       = $this->getHCMForm();
            $form->get('molding')->setValue(2); 
            $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[0] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[0]->setName("Availability");
            $hcAvail[0]->setColor("#005487");
            $hcAvail[0]->setType("column");
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());

            $hcSGlue = new HcSeriesGlue();
            $hcSGlue->setObjSeries($hcAvail[0]);

            $objMontlyEff = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[1] = new HcSerie($objMontlyEff->getMonthDataArray());
            $hcAvail[1]->setName("Efficiency");
            $hcAvail[1]->setColor("#DC4520");
            $hcAvail[1]->setType("column");
            $hcSGlue->setObjSeries($hcAvail[1]);

            $objMontlyUtil = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[2] = new HcSerie($objMontlyUtil->getMonthDataArray());
            $hcAvail[2]->setName("Utilization");
            $hcAvail[2]->setColor("#CF8A24");
            $hcAvail[2]->setType("column");
            $hcSGlue->setObjSeries($hcAvail[2]);


            $objMontlyDown = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[3] = new HcSerie($objMontlyDown->getMonthDataArray());
            $hcAvail[3]->setName("Down Time");
            $hcAvail[3]->setColor("#000");
            $hcAvail[3]->setType("column");
            $hcSGlue->setObjSeries($hcAvail[3]);


            $objHcAxisY = new HcAxisY();
            $objHcAxisY->Title->value = 'Percentage (%)';
            $objHcAxisY->setProperty("minorTickInterval",10);
            $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
            $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

            $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                            'action' => 'Hcmmontly',
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


            $view->setTemplate('dashboard/dashboardshc/index.phtml');
            return $view;
    }


    public function MontlygraphpropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);

        $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
        $hcAvail[0] = new HcSerie($objMontlyData->getMonthDataArray());
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");

        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $objMontlyEff = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyPropDao(),"english");
        $hcAvail[1] = new HcSerie($objMontlyEff->getMonthDataArray());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("column");
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objMontlyUtil = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
        $hcAvail[2] = new HcSerie($objMontlyUtil->getMonthDataArray());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("column");
        $hcSGlue->setObjSeries($hcAvail[2]);

        $objMontlyDown = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
        $hcAvail[3] = new HcSerie($objMontlyDown->getMonthDataArray());
        $hcAvail[3]->setName("Down Time");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("column");
        $hcSGlue->setObjSeries($hcAvail[3]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                            'action' => 'Hcmmontly',
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


            $view->setTemplate('dashboard/dashboardshc/index.phtml');
            return $view;
    }



    public function MontlygraphAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $objMontlyData = new YearMontlyFormat($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail[0] = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $objMontlyEff = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail[1] = new HcSerie($objMontlyEff->getMonthDataArray());
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("column");
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objMontlyUtil = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail[2] = new HcSerie($objMontlyUtil->getMonthDataArray());
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("column");

        $hcSGlue->setObjSeries($hcAvail[2]);
        $objMontlyDown = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail[3] = new HcSerie($objMontlyDown->getMonthDataArray());
        $hcAvail[3]->setName("Down Time");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("column");

        $hcSGlue->setObjSeries($hcAvail[3]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                            'action' => 'Hcmmontly',
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


                $view->setTemplate('dashboard/dashboardshc/index.phtml');
                return $view;
    }



    private function objToArray($obj)
    {
        $arrayTemp = array();
        while($row = $obj->current())
        {
            print_r($row);
            $arrayTemp[] =$row;
        }

        return $arrayTemp;
    }


    public function YearlygraphAction()
    {
        $firstYear = 2012;
        $form       = $this->getHCMForm();
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $hcSGlue = new HcSeriesGlue();



        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYearAv = new YearMonthAvailability($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArrayAv[] = $objYearAv->getYearData();

            
            $objYearEff = new YearMonthEfficiency($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArrayEff[] = $objYearEff->getYearData();


            $objYearUt = new YearMonthUtilization($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArrayUt[] = $objYearUt->getYearData();

            $objYearDown = new YearMonthDowntime($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArrayDown[] = $objYearDown->getYearData();

            $yearLabel[] = "'{$i}'";

        }

        $hcAvail[0] = new HcSerie($yearArrayAv);
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");



        $hcAvail[1] = new HcSerie($yearArrayEff);
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("column");


        $hcAvail[2] = new HcSerie($yearArrayUt);
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("column");

        $hcAvail[3] = new HcSerie($yearArrayDown);
        $hcAvail[3]->setName("Down TIme");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("column");

        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);


        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',

                ));


                $view->setTemplate('dashboard/dashboardshc/index.phtml');
                return $view;
    }


    public function HcmmyearmonthAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Yearlygraph','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Yearlygraphhcm','id'=> 4));
                }
              
                return $this->redirect()->toRoute('dashboardshc', array( 'action'=> 'Yearlygraphprop','id'=> 4));

            }
        }
    }



    public function YearlygraphhcmAction()
    {
        $firstYear = 2012;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);

        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $hcSGlue = new HcSeriesGlue();



        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYearAv = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArrayAv[] = $objYearAv->getYearData();

            
            $objYearEff = new YearMonthEfficiency($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArrayEff[] = $objYearEff->getYearData();


            $objYearUt = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArrayUt[] = $objYearUt->getYearData();

            $objYearDown = new YearMonthDowntime($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArrayDown[] = $objYearDown->getYearData();

            $yearLabel[] = "'{$i}'";

        }

        $hcAvail[0] = new HcSerie($yearArrayAv);
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");



        $hcAvail[1] = new HcSerie($yearArrayEff);
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("column");


        $hcAvail[2] = new HcSerie($yearArrayUt);
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("column");

        $hcAvail[3] = new HcSerie($yearArrayDown);
        $hcAvail[3]->setName("Down TIme");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("column");

        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);


        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Dashboards HCM',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',
                            
                ));


                $view->setTemplate('dashboard/dashboardshc/index.phtml');
                return $view;
    }

    public function YearlygraphpropAction()
    {
        $firstYear = 2012;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);

        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $hcSGlue = new HcSeriesGlue();



        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYearAv = new YearMonthAvailability($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArrayAv[] = $objYearAv->getYearData();

            
            $objYearEff = new YearMonthEfficiency($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArrayEff[] = $objYearEff->getYearData();


            $objYearUt = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArrayUt[] = $objYearUt->getYearData();

            $objYearDown = new YearMonthDowntime($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArrayDown[] = $objYearDown->getYearData();

            $yearLabel[] = "'{$i}'";

        }

        $hcAvail[0] = new HcSerie($yearArrayAv);
        $hcAvail[0]->setName("Availability");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("column");



        $hcAvail[1] = new HcSerie($yearArrayEff);
        $hcAvail[1]->setName("Efficiency");
        $hcAvail[1]->setColor("#DC4520");
        $hcAvail[1]->setType("column");


        $hcAvail[2] = new HcSerie($yearArrayUt);
        $hcAvail[2]->setName("Utilization");
        $hcAvail[2]->setColor("#CF8A24");
        $hcAvail[2]->setType("column");

        $hcAvail[3] = new HcSerie($yearArrayDown);
        $hcAvail[3]->setName("Down TIme");
        $hcAvail[3]->setColor("#000");
        $hcAvail[3]->setType("column");

        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcSGlue->setObjSeries($hcAvail[1]);
        $hcSGlue->setObjSeries($hcAvail[2]);
        $hcSGlue->setObjSeries($hcAvail[3]);


        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Dashboards Proprietary',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => '',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => 'Hcmmyearmonth',
                            
                ));


                $view->setTemplate('dashboard/dashboardshc/index.phtml');
                return $view;
    }

}

<?php
namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Dashboard\Model\Availability\AvailabilityFormat;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\Downtime\DowntimeFormat;
use Dashboard\Model\Downtime\YearMonthDowntime;

use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;
use Application\Model\TimeClass\ClassMonthDates;
use Dashboard\Model\Statics\RollingAverage;
use Dashboard\Model\Efficiency\YearMonthEfficiency;



class DowntimehcController extends AbstractDashboardController
{

    public function indexAction()
    {
    		//Last 30 Days
        $dias = 30;
        $rolling = (int)$this->params()->fromRoute('id', 4); 
    	$now_day = date("Y-m-d");
    	$yesterday = date("Y-m-d",strtotime('-1 day'));
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objAv = new DowntimeFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#CF8A24");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);
        $form       = $this->getHCMForm();

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar DownTime',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                ));
        $view->setTemplate('dashboard/downtimehc/indexrolling.phtml');
        return $view;
    }

     public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'index','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'hcmgraph','id'=> 4));
                }
                return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'propgraph','id'=> 4));
            }
        }
    }

    public function HcmgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($pastime, $yesterday, 30);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);

        $objAv = new DowntimeFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                'title' => 'Daily Plantstar DownTime',
                'subTitle' => '(% Percentage)',
                'gtitle' => 'Last 30 Days',
                'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                'gYtitle' => 'Percentage (%)',
                'obj' => $hcSGlue,
                'cat' => $hcCateg->getFormattedData(),
                'hcmForm' => $form,
                'avgAction' => 'hcmgraph',
                'filterData' =>  array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthhcm'),
                ),));
        $view->setTemplate('dashboard/downtimehc/indexrollingTemplate.phtml');
        return $view;
    }

    public function PropgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);
        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($pastime, $yesterday, 30);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  
        $objAv = new DowntimeFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Proprietary");
        $hcAvail[0]->setColor("#58A618");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar DownTime',
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',                         
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'avgAction' => 'propgraph',
                            'filterData' => array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthprop'),
                                                  )

                ));


           $view->setTemplate('dashboard/downtimehc/indexrollingTemplate.phtml');
        return $view;

    }

    public function HcmdaterangeAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('downtimehc2', array('controller'=> 'Downtimehc', 'action'=> 'Daterangerolling','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('downtimehc2', array('controller'=> 'Downtimehc', 'action'=> 'Daterangerollinghcm','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                return $this->redirect()->toRoute('downtimehc2', array('controller'=> 'Downtimehc', 'action'=> 'Daterangerollingprop','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
            }
        }
    }


   public function DaterangerollingAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);

        $objAv = new DowntimeFormat($arrayTemp);      
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#46ABD1");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);
        $form       = $this->getHCMForm();

        $view = new ViewModel(array(
                    'title' => 'Daily Plantstar DownTime',
                    'subTitle' => '(% Percentage)',
                    'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                    'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                    'gYtitle' => 'Percentage (%)',
                    'date' => $fechaIni,
                    'date2' => $fechaFin,
                    'obj' => $hcSGlue,
                    'cat' => $hcCateg->getFormattedData(),
                    'hcmForm' => $form,
                    'action' => "Hcmdaterange",   
                ));
        $view->setTemplate('dashboard/downtimehc/indexdaterolling.phtml');
        return $view;  
   }


   public function DaterangeAction()
    {

        $request    = $this->getRequest();
        $form       = $this->getDatesForm();

        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            
            if($form->isValid())
            {
                $array_post = $form->getData();
                $fechaIni = $array_post['fecha'];
                $fechaFin = $array_post['fecha2'];
                
                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objAv = new DowntimeFormat($arrayTemp);
                $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
                $hcAvail[0]->setName("Molding");
                $hcAvail[0]->setColor("#CF8A24");
                $hcAvail[0]->setType("line");
                $hcAvail[0]->setColumnDataLabels(false);

                $hcSGlue = new HcSeriesGlue();
                $hcSGlue->setObjSeries($hcAvail[0]);

                $hcCateg = new HcCategories($objAv->getArrayDates());
                $arrayValues = $objAv->getArrayAvailability();
                $numElements = count($arrayValues);
                $objRolling = new RollingAverage($arrayValues,4,$numElements);
                $hcCateg = new HcCategories($objAv->getArrayDates());

                $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
                $hcAvail[1]->setName("Rolling Avg");
                $hcAvail[1]->setType("line");
                $hcAvail[1]->setColor("#46ABD1");
                $hcAvail[1]->setMarker(TRUE);
                $hcAvail[1]->setColumnDataLabels(true);
                $hcAvail[1]->setLabelTextColor('#000');
                $hcSGlue->setObjSeries($hcAvail[1]);
                $form       = $this->getHCMForm();

                $view = new ViewModel(array(
                            'title' => 'Daily Plantstar DownTime',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => "Hcmdaterange",   
                ));
                $view->setTemplate('dashboard/downtimehc/indexdaterolling.phtml');
                return $view;
            }
        }
         return new ViewModel(array(
             'subtitle' => "(Choose a Date Range)",
             'title' =>  "Down Time",
             'form' => $form,
                
        ));
    }

    public function DaterangerollinghcmAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $objAv = new DowntimeFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                'title' => 'Daily Plantstar DownTime',
                'subTitle' => '(% Percentage)',
                'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                'gYtitle' => 'Percentage (%)',
                'hcmForm' => $form,
                'action' => "Hcmdaterange",
                'date' => $fechaIni,
                'date2' => $fechaFin,
                'obj' => $hcSGlue,
                'cat' => $hcCateg->getFormattedData(),
                'avgAction' => 'Daterangerollinghcm',
                'filterData' =>  array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthhcm'),
                ),
                ));

        $view->setTemplate('dashboard/downtimehc/indexrollingDateTemplate.phtml');
        return $view;
   }


    public function DaterangerollingpropAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($fechaIni, $fechaFin, false);
        $objAv = new DowntimeFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                'title' => 'Proprietary Daily Plantstar Down Time',
                'subTitle' => '(% Percentage)',
                'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
               'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                'gYtitle' => 'Percentage (%)',
                'hcmForm' => $form,
                'action' => "Hcmdaterange",
                'date' => $fechaIni,
                'date2' => $fechaFin,
                'obj' => $hcSGlue,
                'cat' => $hcCateg->getFormattedData(),
                'avgAction' => 'Daterangerollingprop',
               'filterData' => array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthprop'),
                                                  )
                ));

        $view->setTemplate('dashboard/downtimehc/indexrollingDateTemplate.phtml');
        return $view;
   }



    public function HcmmontlyAction()
   {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Montlygraph','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Montlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Montlygraphprop','id'=> 4));
            }
        }
    }



    public function MontlygraphAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $objMontlyData = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());
        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);
        $form       = $this->getHCMForm();

        $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'action' => "Hcmmontly",
                            'hcmForm' => $form,
                            'filterData' =>  array("last30"     => array('url' => 'downtime', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonth'),
                ),
                ));
        $view->setTemplate('dashboard/downtimehc/index.phtml');
        return $view;
    }


    public function MontlygraphhcmAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 
        $objMontlyData = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());
        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'HCM Montly Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' =>  array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthhcm'),
                                            ),
                ));


        $view->setTemplate('dashboard/downtimehc/index.phtml');
        return $view;
    }


  public function MontlygraphpropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 
        $objMontlyData = new YearMonthDowntime($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","PROP");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'Proprietary Montly Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' => array("last30"     => array('url' => 'downt/default', 'controller' => 'Downtimehc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonthprop'),
                                                  )
                ));


        $view->setTemplate('dashboard/downtimehc/index.phtml');
        return $view;
    }



    private function objToArray($obj)
    {
        $arrayTemp = array();
        while($row = $obj->current())
        {
            print_r($row);
            $arrayTemp[] =$row;
        }

        return $arrayTemp;
    }


    public function HcmyearlyAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearlygraph','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearlygraphprop','id'=> 4));
            }
        }
    }

    public function YearlygraphAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthDowntime($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }

        $form       = $this->getHCMForm();
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action'   => 'Hcmyearly',
                            'filterData' =>  array("last30"     => array('url' => 'downtime', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonth'),)


                ));


                $view->setTemplate('dashboard/downtimehc/yearly.phtml');
                return $view;
    }


    public function YearlygraphhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthDowntime($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'HCM Yearly Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "HCM All Years (Data)",
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'action'   => 'Hcmyearly',
                            'filterData' =>  array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
                                            ),
    
                ));

                $view->setTemplate('dashboard/downtimehc/yearly.phtml');
                return $view;
    }

    
    public function YearlygraphpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYear = new YearMonthDowntime($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

   
        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Proprietary Yearly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Proprietary All Years (Data)",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'action'   => 'Hcmyearly',
                            'filterData' => array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
                                                  )

                ));

        $view->setTemplate('dashboard/downtimehc/yearly.phtml');
        return $view;
    }


    public function HcmmyearmonthAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearmonth','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearmonthhcm','id'=> 4));
                }
              
                return $this->redirect()->toRoute('downtimehc', array( 'action'=> 'Yearmonthprop','id'=> 4));

            }
        }
    }


    public function YearmonthAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#005487','#DC4520','#46ABD1','#4B3F3B');
        $hcSGlue = new HcSeriesGlue();
        $form       = $this->getHCMForm();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthDowntime($i, $this->getMoldingEfficenciyDao(),"english");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }
    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Down Time',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Down Time = Down Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentaje %',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                             'action' => "Hcmmyearmonth",
                            'filterData' =>  array("last30"     => array('url' => 'downtime', 'controller' => 'Downtimehc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Montlygraph'),
                                                  "yearly"     => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearlygraph'),
                                                  "yearmonth"  => array('url' => 'downt/default','controller' => 'Downtimehc', 'action'=> 'Yearmonth'),)

                ));


        $view->setTemplate('dashboard/downtimehc/index.phtml');
        return $view;
    }






}

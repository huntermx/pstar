<?php


namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;

use Dashboard\Model\Availability\AvailabilityFormat;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Efficiency\EfficiencyFormatArrayGrid;

use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;
use Application\Model\TimeClass\ClassMonthDates;
use Dashboard\Model\Statics\RollingAverage;
use Dashboard\Forms\FormYearstoValidator;

use Dashboard\Model\Efficiency\YearQuartEfficiency;

use Dashboard\Model\Efficiency\YearMonthEfficiency;

class EfficiencyhcController extends AbstractDashboardController
{

    public function indexAction()
    {
    		//Last 30 Days
        $rolling       = (int)$this->params()->fromRoute('id', 4);  
        $dias          = 30;
    	$now_day       = date("Y-m-d");
    	$yesterday     = date("Y-m-d",strtotime('-1 day'));

        $date_conv     = strtotime($now_day);
        $date_yest     = strtotime("-{$dias} day",$date_conv);
        $pastime       = date("Y-m-d",$date_yest);
        $arrayTemp     = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $arrayTemp2     = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $form          = $this->getHCMForm();
        $objGrid      = new EfficiencyFormatArrayGrid($arrayTemp2);

        $objAv         = new EfficiencyFormat($arrayTemp);
        $hcAvail[0]    = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue       = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $arrayValues   = $objAv->getArrayAvailability();

      

        $numElements   = count($arrayValues);
        $objRolling    = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg       = new HcCategories($objAv->getArrayDates());
        $hcAvail[1]    = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view          = new ViewModel(array(
                            'title' => 'Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'grid_array' =>$objGrid->getResultArray()
                        ));

        $view->setTemplate('dashboard/efficiencyhc/indexrolling.phtml');
        return $view;
    }


    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'index','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'hcmgraph','id'=> 4));
                }

                return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'propgraph','id'=> 4));
              
            }
        }
    }


    public function PropgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));

        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);
        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($pastime, $yesterday, 30);
        $arrayTemp2 = $this->getMoldingEfficenciyPropDao()->getECByDates($pastime, $yesterday, 30);

        $objGrid      = new EfficiencyFormatArrayGrid($arrayTemp2);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

        $objAv = new EfficiencyFormat($arrayTemp);


        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Proprietary");
        $hcAvail[0]->setColor("#58A618");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);



        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);



        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());


        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);



        $view = new ViewModel(array(
                            'title' => 'Proprietary Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'grid_array' =>$objGrid->getResultArray()
                ));


        $view->setTemplate('dashboard/efficiencyhc/indexrollingprop.phtml');
        return $view;

    }


    public function HcmgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));

        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getECByDates($pastime, $yesterday, 30);
        $arrayTemp2 = $this->getMoldingEfficenciyHCMDao()->getECByDates($pastime, $yesterday, 30);
        $objGrid      = new EfficiencyFormatArrayGrid($arrayTemp2);

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $objAv = new EfficiencyFormat($arrayTemp);


        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                            'title' => 'HCM Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'grid_array' =>$objGrid->getResultArray()

                ));


        $view->setTemplate('dashboard/efficiencyhc/indexrollinghcm.phtml');
        return $view;
    }



    public function DaterangerollingAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  

        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new EfficiencyFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());


        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                    'title' => 'Daily Plantstar Efficiency',
                    'subTitle' => '(% Percentage)',
                    'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                    'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                    'gYtitle' => 'Percentage (%)',
                    'hcmForm' => $form,
                    'action' => "Hcmdaterange",
                    'date' => $fechaIni,
                    'date2' => $fechaFin,
                    'obj' => $hcSGlue,
                    'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/indexdaterolling.phtml');
        return $view;
   }


    public function DaterangerollinghcmAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  

        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new EfficiencyFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                            'title' => 'HCM Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                             'action' => "Hcmdaterange",
                              'date' => $fechaIni,
                             'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));

        $view->setTemplate('dashboard/efficiencyhc/indexdaterollinghcm.phtml');
        return $view;
   }


    public function DaterangerollingpropAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  

        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

        $arrayTemp = $this->getMoldingEfficenciyPropDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new EfficiencyFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#58A618");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $view = new ViewModel(array(
                            'title' => 'Proprietary Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                             'action' => "Hcmdaterange",
                              'date' => $fechaIni,
                             'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));

                $view->setTemplate('dashboard/efficiencyhc/indexdaterollingprop.phtml');
                return $view;
   }



    public function HcmdaterangeAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();

            $form->setData($post);

            if($form->isValid())
            {

                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc2', array('controller'=> 'Efficiencyhc', 'action'=> 'Daterangerolling','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc2', array('controller'=> 'Efficiencyhc', 'action'=> 'Daterangerollinghcm','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                return $this->redirect()->toRoute('efficiencyhc2', array('controller'=> 'Efficiencyhc', 'action'=> 'Daterangerollingprop','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
              
            }
        }
    }



    public function DaterangeAction()
    {

        $request    = $this->getRequest();
        $form       = $this->getDatesForm();

        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            
            if($form->isValid())
            {
                $array_post = $form->getData();
                $fechaIni = $array_post['fecha'];
                $fechaFin = $array_post['fecha2'];

                $form       = $this->getHCMForm();
                
                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objAv = new EfficiencyFormat($arrayTemp);
                
                $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
                $hcAvail[0]->setName("Molding");
                $hcAvail[0]->setColor("#005487");
                $hcAvail[0]->setType("line");
                $hcAvail[0]->setColumnDataLabels(false);

                $hcSGlue = new HcSeriesGlue();
                $hcSGlue->setObjSeries($hcAvail[0]);
                $hcCateg = new HcCategories($objAv->getArrayDates());
                $arrayValues = $objAv->getArrayAvailability();
                $numElements = count($arrayValues);
                $objRolling = new RollingAverage($arrayValues,4,$numElements);

                $hcCateg = new HcCategories($objAv->getArrayDates());


   
                $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
                $hcAvail[1]->setName("Rolling Avg");
                $hcAvail[1]->setType("line");
                $hcAvail[1]->setColor("#990026");
                $hcAvail[1]->setMarker(TRUE);
                $hcAvail[1]->setColumnDataLabels(true);
                $hcAvail[1]->setLabelTextColor('#000');
                $hcSGlue->setObjSeries($hcAvail[1]);



                $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'action' => "Hcmdaterange",
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


                $view->setTemplate('dashboard/efficiencyhc/indexdaterolling.phtml');
                return $view;
               
            }

        }

         return new ViewModel(array(
             'subtitle' => "(Choose a Date Range)",
             'title' =>  "Efficiency",
             'form' => $form,
                
        ));
    }


    public function HcmmontlyAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Montlygraph','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Montlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Montlygraphprop','id'=> 4));
              
            }
        }
    }


    public function MontlygraphAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $objMontlyData = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);
        $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'action' => "Hcmmontly",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/index.phtml');
        return $view;
    }

    public function YearquartsAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getDataArray());

        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);
        $view = new ViewModel(array(
                            'title' => 'Quarts Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'action' => "Hcmsinglequart",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/index.phtml');
        return $view;
    }


    public function HcmsinglequartAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();

            $form->setData($post);

            if($form->isValid())
            {

                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquarts','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquartshcm','id'=> 4));
                }

                return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquartsprop','id'=> 4));
              
            }
        }
    }

    public function YearquartshcmAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyHCMDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getDataArray());

        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);
        $view = new ViewModel(array(
                            'title' => 'Quarts Plantstar HCM Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'action' => "Hcmsinglequart",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/index.phtml');
        return $view;
    }

    public function YearquartspropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyPropDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);
        $view = new ViewModel(array(
                            'title' => 'Quarts Plantstar Proprietary Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'hcmForm' => $form,
                            'action' => "Hcmsinglequart",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/index.phtml');
        return $view;
    }


    public function Hcmsinglequart2Action()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();

            $form->setData($post);

            if($form->isValid())
            {

                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquarts','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquartshcm','id'=> 4));
                }

                return $this->redirect()->toRoute('efficiencyhc', array('controller'=> 'Efficiencyhc', 'action'=> 'Yearquartsprop','id'=> 4));
              
            }
        }
    }


    public function YearquartallAction()
    {
        $request    = $this->getRequest();
        $form       = $this->FormYearQuarts();
        $currentYear= (int)date("Y");

        if($request->isPost())
        {
            $post = $request->getPost();
            $currentYear = $post->id;          
        }


        //$firstYear = 2012;
        $columnColors= array('#BFBFBF','#005487','#CF8A24','#BFBFBF');
        $hcSGlue = new HcSeriesGlue();


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail[0] = new HcSerie($objMontlyData->getDataArray());
        $hcAvail[0]->setColumnDataLabels(true);
        $hcAvail[0]->setColor('#005487');
        $hcAvail[0]->setName("Total Molding");
        $hcAvail[0]->setType('column');
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());
        $hcSGlue->setObjSeries($hcAvail[0]);


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyPropDao(),"english");
        $hcAvail[1] = new HcSerie($objMontlyData->getDataArray());
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setColor('#58A618');
        $hcAvail[1]->setName("Proprietary");
        $hcAvail[1]->setType('column');
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());
        $hcSGlue->setObjSeries($hcAvail[1]);


        $objMontlyData = new YearQuartEfficiency($currentYear, $this->getMoldingEfficenciyHCMDao(),"english");
        $hcAvail[2] = new HcSerie($objMontlyData->getDataArray());
        $hcAvail[2]->setColumnDataLabels(true);
        $hcAvail[2]->setColor('#CF8A24');
        $hcAvail[2]->setName("HCM");
        $hcAvail[2]->setType('column');
        $hcCateg = new HcCategories($objMontlyData->getDataNamesArray());
        $hcSGlue->setObjSeries($hcAvail[2]);

    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Efficiency ' . $currentYear,
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Yearquartall",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/efficiencyhc/yearquarts.phtml');
        return $view;
    }


    public function MontlygraphhcmAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        $objMontlyData = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyHCMDao(),"english");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'HCM Montly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/efficiencyhc/indexhcm.phtml');
        return $view;
    }


 public function MontlygraphpropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        $objMontlyData = new YearMonthEfficiency($currentYear, $this->getMoldingEfficenciyPropDao(),"english");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'Proprietary Montly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/efficiencyhc/indexprop.phtml');
        return $view;
    }



    private function objToArray($obj)
    {
        $arrayTemp = array();
        while($row = $obj->current())
        {
            print_r($row);
            $arrayTemp[] =$row;
        }

        return $arrayTemp;
    }



    public function HcmyearlyAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearlygraph','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearlygraphprop','id'=> 4));
            }
        }
    }


    public function YearlygraphAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $form       = $this->getHCMForm();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthEfficiency($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }


        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));
        $view->setTemplate('dashboard/efficiencyhc/yearly.phtml');
        return $view;
    }


    public function YearlygraphhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYear = new YearMonthEfficiency($i, $this->getMoldingEfficenciyHCMDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'HCM Yearly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "HCM All Years (Data)",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));

                $view->setTemplate('dashboard/efficiencyhc/yearlyhcm.phtml');
                return $view;
    }


 public function YearlygraphpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYear = new YearMonthEfficiency($i, $this->getMoldingEfficenciyPropDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Proprietary Yearly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Proprietary All Years (Data)",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));

                $view->setTemplate('dashboard/efficiencyhc/yearlyprop.phtml');
                return $view;
    }


    public function HcmmyearmonthAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearmonth','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearmonthhcm','id'=> 4));
                }
              
                return $this->redirect()->toRoute('efficiencyhc', array( 'action'=> 'Yearmonthprop','id'=> 4));

            }
        }
    }



    public function YearmonthAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#46ABD1','#BFBFBF','#005487','#3F413C');
        $hcSGlue = new HcSeriesGlue();

        $form       = $this->getHCMForm();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthEfficiency($i, $this->getMoldingEfficenciyDao(),"english");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);


        }
    
        $view = new ViewModel(array(
                            'title' => 'Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Hcmmyearmonth",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/efficiencyhc/index.phtml');
        return $view;
    }

    public function YearmonthhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#F0C64F','#4B3F3B','#CF8A24');
        $hcSGlue = new HcSeriesGlue();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthEfficiency($i, $this->getMoldingEfficenciyHCMDao(),"english");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }
        
    

        $view = new ViewModel(array(
                            'title' => 'HCM Plantstar  Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Hcmmyearmonth",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),

                ));


        $view->setTemplate('dashboard/efficiencyhc/indexhcm.phtml');
        return $view;

    }

    public function YearmonthpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#5C2926','#DC4520','#707070','#58A618');
        $hcSGlue = new HcSeriesGlue();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthEfficiency($i, $this->getMoldingEfficenciyPropDao(),"english");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }
    
        $view = new ViewModel(array(
                            'title' => 'Proprietary Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Hcmmyearmonth",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                ));


        $view->setTemplate('dashboard/efficiencyhc/indexprop.phtml');
        return $view;

    }



}

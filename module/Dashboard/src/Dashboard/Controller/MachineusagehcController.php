<?php
namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;


use Dashboard\Model\Availability\AvailabilityFormat;
use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\Availability\YearMonthAvailability;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Utilization\UtilizationFormat;
use Dashboard\Model\Downtime\DowntimeFormat;

use Dashboard\Model\HighchartsWapper\HcAxisY;
use Dashboard\Model\HighchartsWapper\HcTitle;
use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;

use Dashboard\Model\Statics\RollingAverage;
use Dashboard\Model\Utilization\YearMonthUtilization;
use Dashboard\Model\Efficiency\YearMonthEfficiency;
use Dashboard\Model\Downtime\YearMonthDowntime;

use Application\Model\TimeClass\ClassMonthDates;



class MachineusagehcController extends AbstractDashboardController
{


    public function indexAction()
    {
    		//Last 30 Days
        $dias          = 30;
    	$now_day       = date("Y-m-d");
    	$yesterday     = date("Y-m-d",strtotime('-1 day'));

        $date_conv     = strtotime($now_day);
        $date_yest     = strtotime("-{$dias} day",$date_conv);
        $pastime       = date("Y-m-d",$date_yest);
       
        $arrayProp =$this->getMachineUsagePropDao()->getMachineDateRange('2014-03-01','2014-03-18' );
        echo "<br><br><br>";
        print_r($arrayProp->current());
    }



}

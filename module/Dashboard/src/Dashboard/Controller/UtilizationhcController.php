<?php

namespace Dashboard\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Config\Reader\Ini;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Utilization\UtilizationFormat;
use Dashboard\Model\Availability\YearMontlyFormat;
use Dashboard\Model\HighchartsWapper\HcSerie;
use Dashboard\Model\HighchartsWapper\HcCategories;
use Dashboard\Model\HighchartsWapper\HcSeriesGlue;

use Dashboard\Model\HighchartsWapper\HcAxisY;
use Dashboard\Model\HighchartsWapper\HcTitle;


use Application\Model\TimeClass\ClassMonthDates;
use Dashboard\Model\Statics\RollingAverage;
use Dashboard\Model\Utilization\YearMonthUtilization;
use Dashboard\Model\Efficiency\YearMonthEfficiency;


class UtilizationhcController extends AbstractDashboardController
{
    public function indexAction()
    {
    		//Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
    	$now_day = date("Y-m-d");
    	$yesterday = date("Y-m-d",strtotime('-1 day'));

        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($pastime, $yesterday, 30);
        $objAv = new UtilizationFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#005487");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#CF8A24");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);
        $form       = $this->getHCMForm();


        $objHcAxisY = new HcAxisY();

        $objHcAxisY->Title->value = 'Percentage (%)';
        //minorTickInterval: 10,
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));


        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                ));
        $view->setTemplate('dashboard/utilizationhc/indexrolling.phtml');
        return $view;
    }

    
    public function HcmindexAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'index','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'hcmgraph','id'=> 4));
                }
                return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'propgraph','id'=> 4));
            }
        }
    }


     public function HcmgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($pastime, $yesterday, 30);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  
        $objAv = new UtilizationFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);


        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));


        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar HCM Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY ,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'avgAction' => 'hcmgraph',
                            'filterData' =>  array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
                                            ),

                ));
        $view->setTemplate('dashboard/utilizationhc/indexrollingTemplate.phtml');
        return $view;
    }


 


    public function PropgraphAction()
    {
            //Last 30 Days
        $rolling = (int)$this->params()->fromRoute('id', 4);  
        $dias = 30;
        $now_day = date("Y-m-d");
        $yesterday = date("Y-m-d",strtotime('-1 day'));
        $date_conv = strtotime($now_day);
        $date_yest = strtotime("-{$dias} day",$date_conv);
        $pastime =  date("Y-m-d",$date_yest);
        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($pastime, $yesterday, 30);
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  
        $objAv = new UtilizationFormat($arrayTemp);

        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Proprietary");
        $hcAvail[0]->setColor("#58A618");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                            'title' => 'Daily Plantstar HCM Utilization',
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',                           
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Last 30 Days',
                            'gYtitle' => $objHcAxisY,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'avgAction' => 'propgraph',
                            'filterData' => array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
                                                  )

                ));

        $view->setTemplate('dashboard/utilizationhc/indexrollingTemplate.phtml');
        return $view;
    }



   public function DaterangerollingAction()
   {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
          
        $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
        $objAv = new UtilizationFormat($arrayTemp);
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);
        $hcCateg = new HcCategories($objAv->getArrayDates());

        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);

        $hcCateg = new HcCategories($objAv->getArrayDates());

        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#46ABD1");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);
        $form       = $this->getHCMForm();

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                    'title' => 'Daily Plantstar Utilization',
                    'subTitle' => '(% Percentage)',
                    'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                    'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                    'gYtitle' => $objHcAxisY,
                    'action' => "Hcmdaterange",
                    'date' => $fechaIni,
                    'date2' => $fechaFin,
                    'obj' => $hcSGlue,
                    'cat' => $hcCateg->getFormattedData(),
                    'hcmForm' => $form,
                    'avgAction' => 'daterangerolling',
               ));


        $view->setTemplate('dashboard/utilizationhc/indexdaterolling.phtml');
        return $view;
   }



    public function DaterangeAction()
    {

        $request    = $this->getRequest();
        $form       = $this->getDatesForm();
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);
            
            if($form->isValid())
            {
                $array_post = $form->getData();
                $fechaIni = $array_post['fecha'];
                $fechaFin = $array_post['fecha2'];
                
                $arrayTemp = $this->getMoldingEfficenciyDao()->getECByDates($fechaIni, $fechaFin, false);
                $objAv = new UtilizationFormat($arrayTemp);
                $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
                $hcAvail[0]->setName("Molding");
                $hcAvail[0]->setColor("#CF8A24");
                $hcAvail[0]->setType("line");
                $hcAvail[0]->setColumnDataLabels(false);
                $hcSGlue = new HcSeriesGlue();
                $hcSGlue->setObjSeries($hcAvail[0]);

                $hcCateg = new HcCategories($objAv->getArrayDates());
                $arrayValues = $objAv->getArrayAvailability();
                $numElements = count($arrayValues);
                $objRolling = new RollingAverage($arrayValues,4,$numElements);
                $hcCateg = new HcCategories($objAv->getArrayDates());
                $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
                $hcAvail[1]->setName("Rolling Avg");
                $hcAvail[1]->setType("line");
                $hcAvail[1]->setColor("#46ABD1");
                $hcAvail[1]->setMarker(TRUE);
                $hcAvail[1]->setColumnDataLabels(true);
                $hcAvail[1]->setLabelTextColor('#000');
                $hcSGlue->setObjSeries($hcAvail[1]);
                $form       = $this->getHCMForm();

                $objHcAxisY = new HcAxisY();
                $objHcAxisY->Title->value = 'Percentage (%)';
                $objHcAxisY->setProperty("minorTickInterval",10);
                $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
                $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

                $view = new ViewModel(array(
                            'title' => 'Daily Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => $objHcAxisY,
                            'date' => $fechaIni,
                            'date2' => $fechaFin,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => "Hcmdaterange",    
                ));
                $view->setTemplate('dashboard/utilizationhc/indexdaterolling.phtml');
                return $view;
            }
        }

         return new ViewModel(array(
             'subtitle' => "(Choose a Date Range)",
             'title' =>  "Utilization",
             'form' => $form,
                
        ));
    }


     public function HcmdaterangeAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
           $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('utilizationhc2', array('controller'=> 'Utilizationhc', 'action'=> 'Daterangerolling','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('utilizationhc2', array('controller'=> 'Utilizationhc', 'action'=> 'Daterangerollinghcm','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
                }

                return $this->redirect()->toRoute('utilizationhc2', array('controller'=> 'Utilizationhc', 'action'=> 'Daterangerollingprop','id'=> 4, 
                        'date' => $post['fecha_ini'], 'date2' => $post['fecha_fin'] ));
            }
        }
    }

  
    public function DaterangerollinghcmAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationHCM($fechaIni, $fechaFin, false);
        $objAv = new UtilizationFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                'title' => 'Daily Plantstar Utilization HCM',
                'subTitle' => '(% Percentage)',
                'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                'gYtitle' => $objHcAxisY,
                'hcmForm' => $form,
                'action' => "Hcmdaterange",
                'date' => $fechaIni,
                'date2' => $fechaFin,
                'obj' => $hcSGlue,
                'cat' => $hcCateg->getFormattedData(),
                'avgAction' => 'Daterangerollinghcm',
                'filterData' =>  array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
                                            ),
                ));

        $view->setTemplate('dashboard/utilizationhc/indexrollingDateTemplate.phtml');
        return $view;
   }


     public function DaterangerollingpropAction()
    {
        $rolling = (int)$this->params()->fromRoute('id', 4);   
        $rolling2 = $this->params()->fromRoute('date', 4); 
        $rolling3 = $this->params()->fromRoute('date2', 4);  
        $fechaIni = $rolling2;
        $fechaFin = $rolling3;
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3);  

        $arrayTemp = $this->getMoldingEfficenciyHCMDao()->getUtilizationProp($fechaIni, $fechaFin, false);
        $objAv = new UtilizationFormat($arrayTemp);
                
        $hcAvail[0] = new HcSerie($objAv->getArrayAvailability());
        $hcAvail[0]->setName("HCM Molding");
        $hcAvail[0]->setColor("#CF8A24");
        $hcAvail[0]->setType("line");
        $hcAvail[0]->setColumnDataLabels(false);

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail[0]);

        $hcCateg = new HcCategories($objAv->getArrayDates());
        $arrayValues = $objAv->getArrayAvailability();
        $numElements = count($arrayValues);
        $objRolling = new RollingAverage($arrayValues,$rolling,$numElements);
        $hcCateg = new HcCategories($objAv->getArrayDates());
   
        $hcAvail[1] = new HcSerie($objRolling->getArrayResult());
        $hcAvail[1]->setName("Rolling Avg");
        $hcAvail[1]->setType("line");
        $hcAvail[1]->setColor("#990026");
        $hcAvail[1]->setMarker(TRUE);
        $hcAvail[1]->setColumnDataLabels(true);
        $hcAvail[1]->setLabelTextColor('#000');
        $hcSGlue->setObjSeries($hcAvail[1]);

        $objHcAxisY = new HcAxisY();
        $objHcAxisY->Title->value = 'Percentage (%)';
        $objHcAxisY->setProperty("minorTickInterval",10);
        $objHcAxisY->setProperty("min",$this->getMinValue($hcAvail));
        $objHcAxisY->setProperty("max",$this->getMaxValue($hcAvail));

        $view = new ViewModel(array(
                'title' => 'Daily Plantstar Utilization Proprietary',
                'subTitle' => '(% Percentage)',
                'gtitle' => "Date Range ({$fechaIni} to {$fechaFin})",
                'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                'gYtitle' => $objHcAxisY,
                'hcmForm' => $form,
                'action' => "Hcmdaterange",
                'date' => $fechaIni,
                'date2' => $fechaFin,
                'obj' => $hcSGlue,
                'cat' => $hcCateg->getFormattedData(),
                'avgAction' => 'Daterangerollingprop',
                'filterData' => array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
                                                  )
                ));

        $view->setTemplate('dashboard/utilizationhc/indexrollingDateTemplate.phtml');
        return $view;
   }


   public function HcmmontlyAction()
   {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Montlygraph','id'=> 4));
                }
                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Montlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Montlygraphprop','id'=> 4));
            }
        }
    }

    public function MontlygraphAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $objMontlyData = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyDao(),"english");
        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'Montly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action' => "Hcmmontly",
                            'filterData' => array("last30"     => array('url' => '', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraph'),
                                                "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraph'),
                                                "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonth'),)
                ));
        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;
    }


    public function MontlygraphhcmAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 
        $objMontlyData = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","HCM");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());
        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());
        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'HCM Montly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' =>  array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
                                            ),
                ));


        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;
    }


    public function MontlygraphpropAction()
    {
        $currentMonth = (int)date("n");
        $currentYear= (int)date("Y");
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 
        $objMontlyData = new YearMonthUtilization($currentYear, $this->getMoldingEfficenciyHCMDao(),"english","PROP");

        $hcAvail = new HcSerie($objMontlyData->getMonthDataArray());

        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");
        $hcCateg = new HcCategories($objMontlyData->getMonthArray());

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $view = new ViewModel(array(
                            'title' => 'Proprietary Montly Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Year {$currentYear}",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                            'action' => "Hcmmontly",
                            'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' => array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
                                                  )
                ));


        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;
    }



    private function objToArray($obj)
    {
        $arrayTemp = array();
        while($row = $obj->current())
        {
            print_r($row);
            $arrayTemp[] =$row;
        }

        return $arrayTemp;
    }


   

    public function HcmyearlyAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearlygraph','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearlygraphhcm','id'=> 4));
                }

                return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearlygraphprop','id'=> 4));
            }
        }
    }


    public function YearlygraphAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthUtilization($i, $this->getMoldingEfficenciyDao(),"english");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }

        $form       = $this->getHCMForm();
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#005487");
        $hcAvail->setName("Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Yearly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "All Years (Data)",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => 'Percentage (%)',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                            'action'   => 'Hcmyearly',
                            'filterData' => array("last30"     => array('url' => '', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraph'),
                                                "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraph'),
                                                "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonth'),)
                ));

        $view->setTemplate('dashboard/utilizationhc/yearly.phtml');
        return $view;
    }

    public function YearlygraphhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();
        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objYear = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#CF8A24");
        $hcAvail->setName("HCM Molding");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'HCM Yearly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "HCM All Years (Data)",
                            'gsubtitle' => 'Utilization = Scheduled RunTime / Total Capacity',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'action'   => 'Hcmyearly',
                            'filterData' =>  array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
                                            ),
    
                ));

                $view->setTemplate('dashboard/utilizationhc/yearly.phtml');
                return $view;
    }

     public function YearlygraphpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $yearArray = array();
        $yearLabel = array();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {

            $objYear = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $yearArray[] = $objYear->getYearData();
            $yearLabel[] = "'{$i}'";
        }
        $hcAvail = new HcSerie($yearArray);
        $hcAvail->setColor("#58A618");
        $hcAvail->setName("Proprietary");
        $hcAvail->setType("column");

        $hcSGlue = new HcSeriesGlue();
        $hcSGlue->setObjSeries($hcAvail);

   
        $hcCateg = new HcCategories($yearLabel); 
        $view = new ViewModel(array(
                            'title' => 'Proprietary Yearly Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => "Proprietary All Years (Data)",
                            'gsubtitle' => 'Efficiency = Run Time / (Run Time + Down Time)',
                            'gYtitle' => 'Percentage (%)',
                             'hcmForm' => $form,
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'action'   => 'Hcmyearly',
                            'filterData' => array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                                                  "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                                                  "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                                                  "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
                                                  )

                ));

        $view->setTemplate('dashboard/utilizationhc/yearly.phtml');
        return $view;
    }


    public function HcmmyearmonthAction()
    {
        $request    = $this->getRequest();
        $form       = $this->getHCMForm();
        
        if($request->isPost())
        {
            $post = $request->getPost();
            $form->setData($post);

            if($form->isValid())
            {
                if($post['molding']==1)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearmonth','id'=> 4));
                }

                if($post['molding']==2)
                {
                    return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearmonthhcm','id'=> 4));
                }
              
                return $this->redirect()->toRoute('utilizationhc', array( 'action'=> 'Yearmonthprop','id'=> 4));

            }
        }
    }



    public function YearmonthAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#005487','#DC4520','#46ABD1','#4B3F3B');
        $hcSGlue = new HcSeriesGlue();
        $form       = $this->getHCMForm();

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthUtilization($i, $this->getMoldingEfficenciyDao(),"english",false);
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }
        $filterData = array("last30"     => array('url' => '', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                            "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraph'),
                            "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraph'),
                            "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonth'),
            );
        $view = new ViewModel(array(
                            'title' => 'Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'hcmForm' => $form,
                             'action' => "Hcmmyearmonth",
                             'filterData' => $filterData,
                ));


        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;
    }

    public function YearmonthhcmAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#F0C64F','#4B3F3B','#CF8A24');
        $hcSGlue = new HcSeriesGlue();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(2); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","HCM");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }

        $filterData = array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'hcmgraph'),
                            "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphhcm'),
                            "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphhcm'),
                            "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthhcm'),
            );
    
        $view = new ViewModel(array(
                           'title' => 'Plantstar Utilization',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Hcmmyearmonth",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' => $filterData,
                ));


        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;

    }

    public function YearmonthpropAction()
    {
        $firstYear = 2012;
        $currentYear= (int)date("Y");
        $columnColors= array('#5C2926','#DC4520','#707070','#58A618');
        $hcSGlue = new HcSeriesGlue();

        $form       = $this->getHCMForm();
        $form->get('molding')->setValue(3); 

        for($i=$firstYear; $i<=$currentYear; $i++)
        {
            $objMontlyData = new YearMonthUtilization($i, $this->getMoldingEfficenciyHCMDao(),"english","PROP");
            $hcAvail[$i] = new HcSerie($objMontlyData->getMonthDataArray());
            $hcAvail[$i]->setColumnDataLabels(true);
            $hcAvail[$i]->setColor(array_pop($columnColors));
            $hcAvail[$i]->setName($i);
            $hcAvail[$i]->setType('column');
            $hcCateg = new HcCategories($objMontlyData->getMonthArray());
            $hcSGlue->setObjSeries($hcAvail[$i]);
        }
        
        $filterData = array("last30"     => array('url' => 'util/default', 'controller' => 'Utilizationhc', 'action'=> 'propgraph'),
                            "monthly"    => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Montlygraphprop'),
                            "yearly"     => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearlygraphprop'),
                            "yearmonth"  => array('url' => 'util/default','controller' => 'Utilizationhc', 'action'=> 'Yearmonthprop'),
        );

        $view = new ViewModel(array(
                            'title' => 'Proprietary Plantstar Efficiency',
                            'subTitle' => '(% Percentage)',
                            'gtitle' => 'Monthly by Year',
                            'gsubtitle' => 'Availability = Capacity - Scheduled DownTime',
                            'gYtitle' => 'Percentaje %',
                            'hcmForm' => $form,
                            'action' => "Hcmmyearmonth",
                            'obj' => $hcSGlue,
                            'cat' => $hcCateg->getFormattedData(),
                            'filterData' => $filterData,
                ));


        $view->setTemplate('dashboard/utilizationhc/index.phtml');
        return $view;

    }


}

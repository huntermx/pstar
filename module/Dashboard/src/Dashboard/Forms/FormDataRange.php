<?php

namespace Dashboard\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-horizontal"})
 */
class FormDataRange
{

	
    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"inputDate"})
     * @Annotation\Attributes({"class":"input-small"})
     * @Annotation\Filter({"name":"StripTags"})
     * 
     */
    public $fecha;

    /**
     * @Annotation\Type("Zend\Form\Element\Text")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"inputDate2"})
     * @Annotation\Attributes({"class":"input-small"})
     * @Annotation\Filter({"name":"StripTags"})
     * 
     */
    public $fecha2;
    
    

    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-primary"})
     * @Annotation\Attributes({"value":"Submit"})
     */
    public $submit;
}
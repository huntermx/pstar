<?php

namespace Dashboard\Forms;

use Zend\Form\Annotation;

/**
 * @Annotation\Hydrator("Zend\Stdlib\Hydrator\ObjectProperty")
 * @Annotation\Name("Usuario")
 * @Annotation\Attributes({"class":"form-inline"})
 * @Annotation\Options({"role":"form"})
 */
class FormMoldingHCM
{	
    /**
     * @Annotation\Type("Zend\Form\Element\Radio")
     * @Annotation\Required(false)
     * @Annotation\Attributes({"id":"inputType"})
     * @Annotation\Attributes({"class":"temptype"})
     * @Annotation\Attributes({"class":"temptype input-small"})
     * @Annotation\Filter({"name":"StripTags"})
     * @Annotation\Options({
     *                      "value_options" : {"1":"Molding Operation","2":"HCM Operations","3":"Proprietary Operations"}})
     * @Annotation\Validator({"name":"InArray",
     *                        "options":{"haystack":{"1","2","3"},
     *                              "messages":{"notInArray":"Gender is not valid"}}})
     * @Annotation\Attributes({"value":"1"})
     * 
     */
    public $molding;
    

    /**
     * @Annotation\Type("Zend\Form\Element\Submit")
     * @Annotation\Attributes({"class":"btn btn-default"})
     * @Annotation\Attributes({"value":"Go!"})
     */
    public $submit;
}
<?php

namespace Dashboard\Forms;

use Zend\Captcha; 
use Zend\Form\Element; 
use Zend\Form\Form; 

class FormYearstoDate  extends Form 
{ 
    public function __construct($name = null) 
    { 
        parent::__construct(''); 
        
        $this->setAttribute('method', 'post'); 
        
        $this->add(array( 
            'name' => 'id', 
            'type' => 'Zend\Form\Element\Select', 
            'attributes' => array( 
                'required' => 'required',
                'class' => 'dropdown input-sm',
            ), 
            'options' => array( 
                'label' => 'Drop Down: ', 
                'value_options' => $this->getYearArray(),
            ), 
        )); 
 

         $this->add(array(
             'name' => 'submit',
             'attributes' => array(
                 'type' => 'submit',
                 'value' => 'Send',
                 'class' => 'btn btn-default',

             ),
         ));
    }

    private function getYearArray()
    {
    	$startYear = 2012;
        $currentYear= (int)date("Y");
        $drop_options[0] = 'Select a Year';
        for($startYear; $startYear <= $currentYear; $startYear++)
        {
        	$drop_options[$startYear] = 'Year ' . (string)$startYear;
        }
        return $drop_options;
    }

} 
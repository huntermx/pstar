<?php


namespace Dashboard\Model\AbstractClasses;

use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Entity\MachUsageDaily;


use Application\Model\Dao\MoldeoEficienciaDao;

Abstract class AbstractDataGraphs
{
    protected $year;
    protected $objEfficiency;
    protected $arrayPeriod=false;
    protected $arrayPeriodNames;
    protected $languaje;
    protected $yearSum=false;


    public function __construct($year=false, $obj=falsem,$languaje=false)
    {
       $this->setYear($year);
       
       if($obj)
       {
            $this->setObjEfficiency($obj);
       }

       $this->setLanguaje($languaje);

    }

    public function setLanguaje($lang='english')
    {
        if($lang=="english")
        {
            $this->languaje = 0;
        }

        if($lang=="spanish")
        {
            $this->languaje = 1;
        }
    }


    public function setObjEfficiency( $obj)
    {
        $this->objEfficiency = $obj;
    }

    public function setYear($year=false)
    {
        if($year == false)
        {
            $this->year = date("Y");
        }else{

            $this->year = (int)$year;
        }

    }



    //abstract function checkPeriodData();

    abstract public function getPeriodName($period);
   

/*
    protected function sumYear()
    {
        $temp = $this->getMonthDataArray();
        $this->yearSum = array_sum($temp);
    }


    public function getYearSum()
    {
         if(!$this->yearSum)
        {
            $this->sumYear();
        }
        return $this->yearSum;
    }


    public function getMonthDataArray()
    {
         if(!$this->arrayMonthlyval)
        {
            $this->checkMonthData();
        }
        return $this->arrayMonthlyval;
    }

    public function getMonthArray()
    {
         if(!$this->arrayDates)
        {
            $this->checkMonthData();
        }
        return $this->arrayDates;
    }
*/
}
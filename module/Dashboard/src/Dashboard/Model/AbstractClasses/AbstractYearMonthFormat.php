<?php


namespace Dashboard\Model\AbstractClasses;

use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Entity\MachUsageDaily;


use Application\Model\Dao\MoldeoEficienciaDao;

Abstract class AbstractYearMonthFormat
{
    protected $year;
    protected $objEfficiency;
    protected $arrayMonthlyval=false;
    protected $arrayDates;
    protected $languaje;
    protected $yearSum=false;


    public function __construct($year=false, $obj=falsem,$languaje=false)
    {
       $this->setYear($year);
       
       if($obj)
       {
            $this->setObjEfficiency($obj);
       }

       $this->setLanguaje($languaje);

    }

    public function setLanguaje($lang='english')
    {
        if($lang=="english")
        {
            $this->languaje = 0;
        }

        if($lang=="spanish")
        {
            $this->languaje = 1;
        }
    }


    public function setObjEfficiency( $obj)
    {
        $this->objEfficiency = $obj;
    }

    public function setYear($year=false)
    {
        if($year == false)
        {
            $this->year = date("Y");
        }else{

            $this->year = (int)$year;
        }

    }

    abstract function checkMonthData();
    

    protected function getMonthName($numMonth)
    {
        $arrayEng = array('1' =>"January",'2'=> "February", '3' => "March", '4' =>"April",'5'=>"May", '6' => "June", '7'=>"July", '8'=>"August", 
                          '9'=>"September", '10'=>"October", '11'=>"November",'12'=> "December");
        $arraySpa = array('1' =>"Enero",'2'=> "Febrero", '3' => "Marzo", '4' =>"Abril",'5'=>"Mayo", '6' => "Junio", '7'=>"Julio", '8'=>"Agosto", 
                          '9'=>"Septiembre", '10'=>"Octubre", '11'=>"Noviembre",'12'=> "Diciembre");

        if($this->languaje==0){
            return $arrayEng[$numMonth];
        }

        if($this->languaje==1){
            return $arraySpa[$numMonth];
        }
    }


    protected function sumYear()
    {
        $temp = $this->getMonthDataArray();
        $this->yearSum = array_sum($temp);
    }


    public function getYearSum()
    {
         if(!$this->yearSum)
        {
            $this->sumYear();
        }
        return $this->yearSum;
    }


    public function getMonthDataArray()
    {
         if(!$this->arrayMonthlyval)
        {
            $this->checkMonthData();
        }
        return $this->arrayMonthlyval;
    }

    public function getMonthArray()
    {
         if(!$this->arrayDates)
        {
            $this->checkMonthData();
        }
        return $this->arrayDates;
    }

}
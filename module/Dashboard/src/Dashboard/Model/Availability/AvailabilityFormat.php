<?php

namespace Dashboard\Model\Availability;

use Dashboard\Model\TimeClass\SegToHoursClass;
use Dashboard\Model\Statics\PercentajeCalculation;

class AvailabilityFormat
{

   protected $objEfCap;
   protected $arrayDates=false;
   protected $arrayPercentaje=false;
   protected $arrayAvailHours=false;
   protected $arraySum = false;

   protected $arrayNumerador = false;
   protected $arrayDenominador = false;

   protected $objPercentaje;


    public function __construct($obj1=false)
    {
        if($obj1!=false)
        {
            $this->setObjEfCap($obj1);
        }
    }

    public function setObjEfCap($obj)
    {
        $this->objEfCap = $obj;
        $this->arrayDates = false;
        $this->arrayPercentaje = false;
        $this->arrayAvailHours = false;
        $this->arraySum = false;

    }
  

    private function checkInputData()
    {
        $rowNum = $this->objEfCap->count();
        if($rowNum == 0)
        {
            $this->resetToZero();
        }else{
            $this->splitArrays();
        }

    }

    private function resetToZero()
    {
        $this->arrayDates= array(0);
        $this->arrayAvailHours = array(0);
        $this->arraySum = 0;
    }


    private function splitArrays()
    {
        $objConvSegHours = new SegToHoursClass();
        $this->objPercentaje = new PercentajeCalculation(2);
        $objPercTemp = new PercentajeCalculation(2);

        while($row = $this->objEfCap->current())
        {
            if($row['nstatus']!= "Holiday")
            {
             
                $tempDate = strtotime($row['fecha']);  
                $tempFDate = (string)date("d-M",$tempDate);
                $this->arrayDates[] = "'{$tempFDate}'";

                $temp = ($row['mantto_prev_time'] + $row['eng_sample_time']);

                $temp2= ($row['total_time']*3600) - $temp ;

                
                $objPercTemp->setNumerador($temp2);
                $objPercTemp->setDenominador(($row['total_time']*3600));


                $this->arrayPercentaje[]  = $objPercTemp->getPercentaje();
                $this->arrayAvailHours[] =$objPercTemp->getPercentaje();
                

                $this->objPercentaje->setNumerador($temp2);
                $this->objPercentaje->setDenominador(($row['total_time']*3600));

            }
        }

    }


    private function sumArrayData()
    {

      $this->splitArrays(); 
        $this->arraySum = $this->objPercentaje->getPercentaje();
    }


    public function getArrayDates()
    {
        if(!$this->arrayDates)
        {
            $this->checkInputData();
        }
        return $this->arrayDates;
    }

    public function getArrayAvailability()
    {
        if(!$this->arrayAvailHours)
        {
            $this->checkInputData();
        }
        return $this->arrayAvailHours;
    }

    public function getMonthSum()
    {
         $this->sumArrayData();
        return $this->arraySum;
    }


}
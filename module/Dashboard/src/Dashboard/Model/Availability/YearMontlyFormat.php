<?php


namespace Dashboard\Model\Availability;

use Application\Model\Dao\MachUsageDailyDao;
use Application\Model\Entity\MachUsageDaily;

use Application\Model\TimeClass\ClassMonthDates;
use Application\Model\Dao\MoldeoEficienciaDao;

class YearMontlyFormat
{
    protected $year;
    protected $objEfficiency;
    protected $arrayMonthlyval=false;
    protected $arrayDates;
    protected $languaje;
    protected $yearSum=false;
    private $classAction = "";

    public function __construct($year=false, $obj=falsem,$languaje=false,$product=false)
    {
       $this->setYear($year);
       
       if($obj)
       {
            $this->setObjEfficiency($obj);
       }

       $this->setLanguaje($languaje);
       $this->setUtilizationProduct($product);

    }

     public function setUtilizationProduct($product)
    {  // HCM PROP, FALSE
        if($product ==false)
        {
            $this->classAction = "getECByDates";
        }

        if($product=="HCM")
        {
            $this->classAction = "getUtilizationHCM";
        }

        if($product=="PROP")
        {
            $this->classAction = "getUtilizationProp";
        }
    }

    public function setLanguaje($lang='english')
    {
        if($lang=="english")
        {
            $this->languaje = 0;
        }

        if($lang=="spanish")
        {
            $this->languaje = 1;
        }
    }


    public function setObjEfficiency( $obj)
    {
        $this->objEfficiency = $obj;
    }

    public function setYear($year=false)
    {
        if($year == false)
        {
            $this->year = date("Y");
        }else{

            $this->year = (int)$year;
        }

    }

    private function checkMonthData()
    {

        $objDates = new ClassMonthDates();
        $objDates->setYear($this->year);
        $objAvail = new AvailabilityFormat();
        $temp2 =  $this->classAction;

        for($i=1; $i<=12; $i++)
        {
             $objDates->setMonth($i);
             $arrayTemp = $this->objEfficiency->$temp2($objDates->getMonthIniDate(), $objDates->getMonthEndDate(), false);
             $objAvail->setObjEfCap($arrayTemp);
             $this->arrayMonthlyval[] = $objAvail->getMonthSum();


             
             $temp = $this->getMonthName($i);
             $this->arrayDates[] = "'{$temp}'";


        }

    }

    private function getMonthName($numMonth)
    {
        $arrayEng = array('1' =>"January",'2'=> "February", '3' => "March", '4' =>"April",'5'=>"May", '6' => "June", '7'=>"July", '8'=>"August", 
                          '9'=>"September", '10'=>"October", '11'=>"November",'12'=> "December");
        $arraySpa = array('1' =>"Enero",'2'=> "Febrero", '3' => "Marzo", '4' =>"Abril",'5'=>"Mayo", '6' => "Junio", '7'=>"Julio", '8'=>"Agosto", 
                          '9'=>"Septiembre", '10'=>"Octubre", '11'=>"Noviembre",'12'=> "Diciembre");

        if($this->languaje==0){
            return $arrayEng[$numMonth];
        }

        if($this->languaje==1){
            return $arraySpa[$numMonth];
        }
    }


    private function sumYear()
    {
        $temp = $this->getMonthDataArray();
        $this->yearSum = array_sum($temp);
    }


    public function getYearSum()
    {
         if(!$this->yearSum)
        {
            $this->sumYear();
        }
        return $this->yearSum;
    }


    public function getMonthDataArray()
    {
         if(!$this->arrayMonthlyval)
        {
            $this->checkMonthData();
        }
        return $this->arrayMonthlyval;
    }

    public function getMonthArray()
    {
         if(!$this->arrayDates)
        {
            $this->checkMonthData();
        }
        return $this->arrayDates;
    }

}
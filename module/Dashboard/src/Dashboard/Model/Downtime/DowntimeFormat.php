<?php

namespace Dashboard\Model\Downtime;

use Dashboard\Model\TimeClass\SegToHoursClass;
use Dashboard\Model\Statics\PercentajeCalculation;

class DowntimeFormat
{

   protected $objEfCap;
   protected $arrayDates=false;
   protected $arrayPercentaje=false;
   protected $arrayAvailHours=false;
   protected $arraySum = false;

   protected $arrayNumerador = false;
   protected $arrayDenominador = false;

    protected $objPercentaje;


    public function __construct($obj1=false)
    {
        if($obj1!=false)
        {
            $this->setObjEfCap($obj1);
        }
    }

    public function setObjEfCap($obj)
    {
         $this->objEfCap = $obj;
         $this->arrayDates = false;
         $this->arrayPercentaje = false;
         $this->arrayAvailHours = false;
         $this->arraySum = false;
         $this->arrayNumerador = false;
         $this->arrayDenominador = false;

    }
  

    private function checkInputData()
    {
        $rowNum = $this->objEfCap->count();
        if($rowNum == 0)
        {
            $this->resetToZero();
        }else{
            $this->splitArrays();
        }

    }

    private function resetToZero()
    {
        $this->arrayDates= array(0);
        $this->arrayAvailHours = array(0);
        $this->arraySum = 0;
    }


    private function splitArrays()
    {
        $objConvSegHours = new SegToHoursClass();
        $this->objPercentaje = new PercentajeCalculation(2);

        $objPercTemp = new PercentajeCalculation(2);


        while($row = $this->objEfCap->current())
        {
            if($row['nstatus']!= "Holiday")
            {
             
                $tempDate = strtotime($row['fecha']);  
                $tempFDate = (string)date("d-M",$tempDate);
                $this->arrayDates[] = "'{$tempFDate}'";
                $objPercTemp->setReset();
            

                $temp = $row['mantto_prev_time']+$row['eng_sample_time']+$row['setup_delay_time']+$row['paro_fin_time'] + $row['arranque_ini_time'];

                $effecntiveDownTime = $row['total_downtime'] - $temp;
           

                $objPercTemp->setNumerador($effecntiveDownTime);
                $objPercTemp->setDenominador(($effecntiveDownTime + $row['total_uptime']));

               

                $this->objPercentaje->setNumerador($effecntiveDownTime);
                $this->objPercentaje->setDenominador(($effecntiveDownTime + $row['total_uptime']));

                $this->arrayPercentaje[]  = $objPercTemp->getPercentaje();

                $this->arrayAvailHours[] =$objPercTemp->getPercentaje();
            }
        }

    }


    private function sumArrayData()
    {
        $this->splitArrays();
      
        $this->arraySum = $this->objPercentaje->getPercentaje();
    }


    public function getArrayDates()
    {
        if(!$this->arrayDates)
        {
            $this->checkInputData();
        }
        return $this->arrayDates;
    }

    public function getArrayAvailability()
    {
        if(!$this->arrayAvailHours)
        {
            $this->checkInputData();
        }
        return $this->arrayAvailHours;
    }

    public function getMonthSum()
    {
     
        $this->sumArrayData();
        
        return $this->arraySum;
    }


}
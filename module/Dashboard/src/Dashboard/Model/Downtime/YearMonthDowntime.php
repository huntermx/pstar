<?php

namespace Dashboard\Model\Downtime;

use Dashboard\Model\AbstractClasses\AbstractYearMonthFormat;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Dashboard\Model\Downtime\DowntimeFormat;
use Application\Model\TimeClass\ClassMonthDates;

class YearMonthDowntime extends AbstractYearMonthFormat
{
	private $yearData;
    private $classAction = "";

  	public function __construct($year=false, $obj=falsem,$languaje=false,$product=false)
    {
    	parent::__construct($year,$obj,$languaje);
    	$this->yearData = false;
        $this->setUtilizationProduct($product);
    }

     public function setUtilizationProduct($product)
    {  // HCM PROP, FALSE
        if($product ==false)
        {
            $this->classAction = "getECByDates";
        }

        if($product=="HCM")
        {
            $this->classAction = "getUtilizationHCM";
        }

        if($product=="PROP")
        {
            $this->classAction = "getUtilizationProp";
        }
    }



    private function checkYearData()
    {
    	$objAvail = new DowntimeFormat();
    	$objDates = new ClassMonthDates();
        $objDates->setYear($this->year);
        $temp2 =  $this->classAction;
   		$arrayTemp = $this->objEfficiency->$temp2($objDates->getYearIniDate(), $objDates->getYearEndDate(), false);
		$objAvail->setObjEfCap($arrayTemp);
		$this->yearData = $objAvail->getMonthSum();
    }

     public function checkMonthData()
     {

     	$objDates = new ClassMonthDates();
        $objDates->setYear($this->year);
        $objAvail = new DowntimeFormat();
        $temp2 =  $this->classAction;


        for($i=1; $i<=12; $i++)
        {
             $objDates->setMonth($i);
             $arrayTemp = $this->objEfficiency->$temp2($objDates->getMonthIniDate(), $objDates->getMonthEndDate(), false);
             

             $objAvail->setObjEfCap($arrayTemp);
            
             $this->arrayMonthlyval[] = $objAvail->getMonthSum();
             $temp = $this->getMonthName($i);
             $this->arrayDates[] = "'{$temp}'";

        }

     }


     public function getYearData()
     {
     	 if(!$this->yearData)
     	 {
     	 	$this->checkYearData();
     	 }
     	 return $this->yearData;
     }

}
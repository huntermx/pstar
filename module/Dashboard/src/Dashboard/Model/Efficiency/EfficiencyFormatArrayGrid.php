<?php

namespace Dashboard\Model\Efficiency;

use Dashboard\Model\TimeClass\SegToHoursClass;
use Dashboard\Model\Statics\PercentajeCalculation;

class EfficiencyFormatArrayGrid
{

	private $query_object;

 	public function __construct($obj1=false)
    {
        if($obj1!=false)
        {
            $this->setQueryObject($obj1);
        }
    }


    public function setQueryObject($obj)
    {
    	$this->query_object = $obj;
    }


    private function generateArray()
    {
    	$result_array = array();
    	$objPercTemp = new PercentajeCalculation(2);

    	$i =0;
        while($row = $this->query_object->current())
        {

            if($row['nstatus']!= "Holiday")
            {
                $objPercTemp->setReset();
            	$result_array[$i]['date'] = $row['fecha'];
            	$result_array[$i]['total_uptime'] = $row['total_uptime'];
            	$result_array[$i]['total_downtime'] = $row['total_downtime'];
            	$result_array[$i]['setup_delay'] = $row['setup_delay_time'];
            	$result_array[$i]['manntto'] = $row['mantto_prev_time'];
            	$result_array[$i]['engine_samples'] = $row['eng_sample_time'];
            	$result_array[$i]['paro_fin'] = $row['paro_fin_time'];
            	$result_array[$i]['arranque_ini'] = $row['arranque_ini_time'];
                $temp = $row['mantto_prev_time']+$row['eng_sample_time']+$row['setup_delay_time']+$row['paro_fin_time'] + $row['arranque_ini_time'];
            	$effecntiveDownTime = $row['total_downtime'] - $temp;
            	if($effecntiveDownTime<0)
                {
                    $effecntiveDownTime = 0;
                }
            	$result_array[$i]['effective_down'] = $effecntiveDownTime;
                $objPercTemp->setNumerador($row['total_uptime']);
                $objPercTemp->setDenominador(($effecntiveDownTime + $row['total_uptime']));
				$result_array[$i]['efficiency'] =  $objPercTemp->getPercentaje();
            	$i++;
            }
        }
        return $result_array;
    }


    public function getResultArray()
    {
    	return $this->generateArray();
    }
}
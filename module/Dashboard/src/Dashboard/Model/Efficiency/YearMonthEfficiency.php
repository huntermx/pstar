<?php

namespace Dashboard\Model\Efficiency;

use Dashboard\Model\AbstractClasses\AbstractYearMonthFormat;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Application\Model\TimeClass\ClassMonthDates;

class YearMonthEfficiency extends AbstractYearMonthFormat
{
	private $yearData;

  	public function __construct($year=false, $obj=falsem,$languaje=false)
    {
    	parent::__construct($year,$obj,$languaje);
    	$this->yearData = false;
    }





    private function checkYearData()
    {
    	$objAvail = new EfficiencyFormat();
    	$objDates = new ClassMonthDates();
        $objDates->setYear($this->year);
   		$arrayTemp = $this->objEfficiency->getECByDates($objDates->getYearIniDate(), $objDates->getYearEndDate(), false);
		$objAvail->setObjEfCap($arrayTemp);
		$this->yearData = $objAvail->getMonthSum();
    }



     public function checkMonthData()
     {

     	$objDates = new ClassMonthDates();
        $objDates->setYear($this->year);
        $objAvail = new EfficiencyFormat();


        for($i=1; $i<=12; $i++)
        {
             $objDates->setMonth($i);
             $arrayTemp = $this->objEfficiency->getECByDates($objDates->getMonthIniDate(), $objDates->getMonthEndDate(), false);
             

             $objAvail->setObjEfCap($arrayTemp);
            
             $this->arrayMonthlyval[] = $objAvail->getMonthSum();
             $temp = $this->getMonthName($i);
             $this->arrayDates[] = "'{$temp}'";

        }

     }


     public function getYearData()
     {
     	 if(!$this->yearData)
     	 {
     	 	$this->checkYearData();
     	 }
     	 return $this->yearData;
     }

}
<?php

namespace Dashboard\Model\Efficiency;

use Dashboard\Model\AbstractClasses\AbstractDataGraphs;
use Dashboard\Model\Efficiency\EfficiencyFormat;
use Application\Model\TimeClass\ClassQuartDates;

class YearQuartEfficiency extends AbstractDataGraphs
{
	private $yearData;
    private $arrayData;

  	public function __construct($year=false, $obj=falsem,$languaje=false)
    {
    	parent::__construct($year,$obj,$languaje);
    	$this->yearData = false;
    }

    private function checkYearData()
    {
    	$objAvail = new EfficiencyFormat();
    	$objDates = new ClassQuartDates();
        $objDates->setYear($this->year);
   		$arrayTemp = $this->objEfficiency->getECByDates($objDates->getYearIniDate(), $objDates->getYearEndDate(), false);
		$objAvail->setObjEfCap($arrayTemp);
		$this->yearData = $objAvail->getMonthSum();
    }


     public function checkQuartData()
     {

     	$objDates = new ClassQuartDates();
        $objDates->setYear($this->year);
        $objAvail = new EfficiencyFormat();


        for($i=1; $i<=4; $i++)
        {
             $objDates->setQuart($i);
             $arrayTemp = $this->objEfficiency->getECByDates($objDates->getQuartIniDate(), $objDates->getQuartEndDate(), false);
             $objAvail->setObjEfCap($arrayTemp);
             $this->arrayData[] = $objAvail->getMonthSum();
             $temp = $this->getPeriodName($i);
             $this->arrayPeriodNames[] = "'{$temp}'";

        }

     }


    

   // abstract function checkPeriodData();

    public function getPeriodName($period)
    {
        $year_quart = array(1=>'Quart 1',2=>'Quart 2',3=>'Quart 3',4=>'Quart 4');
        return $year_quart[$period];
    }

    
    public function getDataArray()
    {
        if(!$this->arrayData)
        {
            $this->checkQuartData();
        }
        return $this->arrayData;
        
    }


    public function getDataNamesArray()
    {
        if(!$this->arrayPeriodNames)
        {
            $this->checkQuartData();
        }
        return $this->arrayPeriodNames;
    }


}
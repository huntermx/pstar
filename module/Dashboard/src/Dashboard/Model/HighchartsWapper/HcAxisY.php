<?php

namespace Dashboard\Model\HighchartsWapper;



class HcAxisY
{
    public $categories=array();
    public $vector=array();
    public $properties;
    public $Title;
    public $propVec=false;
    public $newOption;
    
    
    public function __construct(){
        $this->Title = new HcTitle();
                $this->newOption = false;
    }
    
    
    public function setProperty($key,$value){
        $this->propVec[]=$key;
        $this->properties[$key]=$value;
    }
    
    
    
    
    public function setCategories($string){
            $clean_javascript =  addslashes($string);
            $this->vector[]= "'" . $clean_javascript . "'";
    }
    
    
        
        public function setNewOption($string=false){
            $this->newOption=$string;
        }
        
    public function setCategoriesVec(array $vec){
        $this->vector=$vec;
    }
    
    
    public function setCatVector(){
        if(empty($this->vector)){
            return false;
        }else{
            $temp = implode(',',$this->vector);
            return $temp;
        }   
    }
    
    
    public function getData(){
        $this->categories = $this->setCatVector();
        $temp = 'yAxis: {';
               
                
        if($this->categories===false){
                    
            $temp2 = '';
            if($this->propVec == true){
                            foreach($this->propVec as $row){
                $temp.= "{$row}: {$this->properties[$row]},";
                            }
                        }
            
        }else{
            $temp.= 'categories: [ '.$this->categories.' ]';
        }
        $temp.= $this->Title->getData();
        
                if($this->newOption){
                    $temp.= $this->newOption;
                }
                
                
                $temp.='},';
        
        return $temp;
        
        //  categories: ["
    }
    
    
    
}
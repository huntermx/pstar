<?php

namespace Dashboard\Model\HighchartsWapper;


class HcCategories
{

   protected $arrayData;
   protected $formatSData;
   


    public function __construct(array $arrayData)
    {
        $this->setArrayData($arrayData);
        
    }



    public function setArrayData(array $array)
    {
         $this->arrayData = $array;
         $this->formatSData = FALSE;
    }

    private function formatHc()
    {

        $comma_separated = implode(",",$this->arrayData);
        $this->formatSData = "categories: [" . $comma_separated . "]";
    }



    public function getFormattedData()
    {
        if(!$this->formatSData)
        {
            $this->formatHc();
        }
        return $this->formatSData;
    }





}
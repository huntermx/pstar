<?php

namespace Dashboard\Model\HighchartsWapper;


class HcChart
{

   protected $type;
   protected $margins = array();
   protected $properties = array();
  

   protected $name;
   protected $color;
   protected $type;
   protected $columnDataLabel;
   protected $marker;
   protected $rotation;



    public function __construct($type=false)
    {
      
        $this->setType($type);


        $this->name = false;
        $this->cdlabel = true;
        $this->marker = false;
        $this->rotation ="-90";

        
    }

    public function setType($type)
    {
        if(empty($type))
        {
            $this->type= "'column'";
        }else{
            $this->type = "'{$type}'";
        }

    }


    public function setMargin($key,$margin)
    {
        if(!empty($margin) || !empty($key))
        {
            $this->margins[$key] = $margin;
        }

    }

   
   public function setProperties($key,$value)
   {
        if(!empty($value) || !empty($key))
        {

            if(is_numeric($value))
            {
                $this->properties[$key] = $value;
            }else{
                $this->properties[$key] = "'{$value}'";
            }
        }
   }



  

    private function formatHc()
    {

        $comma_separated = implode(",",$this->arrayData);
        $this->formatSData = "data: [" . $comma_separated . "]";
    }

    public function getFormattedData()
    {
        if(!$this->formatSData)
        {
            $this->formatHc();
        }
        return $this->formatSData;
    }

    public function setMarker($boolean)
    {
        $this->marker = $boolean;
    }


    private function checkMarker()
    {

        $temp = "";
        if($this->marker==FALSE)
        {
            $temp= "";
        }else{

            $temp= "marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }";

        }
        return $temp;
    }


    private function checkColumnDl()
    {
        if($this->cdlabel==FALSE)
        {
            $this->columnDataLabel= "";
        }else{

            $this->columnDataLabel= " dataLabels: {
                    enabled: true,
                    rotation: {$this->rotation},
                    color: '#FFFFFF',
                    align: 'right',
                    x: 4,
                    y: 15,
                    style: {
                        fontSize: '100%',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 3px black'
                    }
                }";

        }
        return $this->columnDataLabel;
    }


    public function setColumnDataLabels($boolean)
    {
        if($boolean == true)
        {
            $this->cdlabel = $boolean;
        }else{
            $this->cdlabel = false;
        }

    }


    private function formattedSeries()
    {
        $temp = "{";
        $arrayFormatted = array();

        if($this->name)
        {
            $arrayFormatted[] =  " name: {$this->name}";
        }

        if($this->color)
        {
            $arrayFormatted[] = " color: {$this->color}";
        }

        if($this->type)
        {
            $arrayFormatted[] = " type: {$this->type}";
        }

        if($this->formatSData)
        {
            $arrayFormatted[] = $this->getFormattedData();
        }

        if($this->cdlabel)
        {
            $arrayFormatted[] = $this->checkColumnDl();
        }

        if($this->marker)
        {
            $arrayFormatted[] = $this->checkMarker();
        }

        $implode = implode(",",$arrayFormatted);    
        $temp = $temp  . $implode . "}";

        return $temp;

    }



     public function getData()
    {
        return $this->formattedSeries();
    }

}
<?php

namespace Dashboard\Model\HighchartsWapper;


class HcSerie
{

   protected $arrayData;
   protected $formatSData;
   protected $name;
   protected $color;
   protected $type;
   protected $columnDataLabel;
   protected $marker;
   protected $rotation;
   protected $labelTextColor;



    public function __construct(array $arrayData)
    {
        $this->setArrayData($arrayData);
        $this->color = "'#005487'";
        $this->type = false;
        $this->name = false;
        $this->cdlabel = true;
        $this->marker = false;
        $this->rotation ="-90";
        $this->labelTextColor = '#FFF';

        
    }

    public function getArrayMax()
    {
        return max($this->arrayData);
    }

    public function getArrayMin()
    {
        return min($this->arrayData);
    }

    public function setName($name)
    {
        if(!empty($name))
        {
            $this->name= "'{$name}'";
        }else{
            $this->name = false;
        }

    }

    public function setRotation($rotation)
    {
        if(empty($rotation))
        {
            $this->rotation= "-90";
        }else{
            $this->rotation = $rotation;
        }
    }

    public function setLabelTextColor($textColor)
    {
        if(empty($textColor))
        {
            $this->labelTextColor= "#FFF";
        }else{
            $this->labelTextColor = $textColor;
        }

    }

    public function setColor($color)
    {
        if(!empty($color))
        {
            $this->color= "'{$color}'";
        }else{
            $this->color = false;
        }

    }

    public function setType($type)
    {
        if(!empty($type))
        {
            $this->type= "'{$type}'";
        }else{
            $this->type = false;
        }

    }

    public function setArrayData(array $array)
    {
         $this->arrayData = $array;
         $this->formatHc();
    }

    private function formatHc()
    {

        $comma_separated = implode(",",$this->arrayData);
        $this->formatSData = "data: [" . $comma_separated . "]";
    }

    public function getFormattedData()
    {
        if(!$this->formatSData)
        {
            $this->formatHc();
        }
        return $this->formatSData;
    }

    public function setMarker($boolean)
    {
        $this->marker = $boolean;
    }


    private function checkMarker()
    {

        $temp = "";
        if($this->marker==FALSE)
        {
            $temp= "";
        }else{

            $temp= "marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }";

        }
        return $temp;
    }


    private function checkColumnDl()
    {
        if($this->cdlabel==FALSE)
        {
            $this->columnDataLabel= "";
        }else{

            $this->columnDataLabel= " dataLabels: {
                    enabled: true,
                    rotation: {$this->rotation},
                    color: '{$this->labelTextColor}',
                    align: 'right',
                    x: 4,
                    y: 15,
                    style: {
                        fontSize: '100%',
                        fontFamily: 'Verdana, sans-serif',
                        textShadow: '0 0 2px black'
                    }
            
                }";

        }
        return $this->columnDataLabel;
    }


    public function setColumnDataLabels($boolean)
    {
        if($boolean == true)
        {
            $this->cdlabel = $boolean;
        }else{
            $this->cdlabel = false;
        }

    }


    private function formattedSeries()
    {
        $temp = "{";
        $arrayFormatted = array();

        if($this->name)
        {
            $arrayFormatted[] =  " name: {$this->name}";
        }

        if($this->color)
        {
            $arrayFormatted[] = " color: {$this->color}";
        }

        if($this->type)
        {
            $arrayFormatted[] = " type: {$this->type}";
        }

        if($this->formatSData)
        {
            $arrayFormatted[] = $this->getFormattedData();
        }

        if($this->cdlabel)
        {
            $arrayFormatted[] = $this->checkColumnDl();
        }

        if($this->marker)
        {
            $arrayFormatted[] = $this->checkMarker();
        }

        $implode = implode(",",$arrayFormatted);    
        $temp = $temp  . $implode . "}";

        return $temp;

    }



     public function getData()
    {
        return $this->formattedSeries();
    }

}
<?php


namespace Dashboard\Model\HighchartsWapper;

class HcTitle
{
	public $value;
	
	public function __construct(){
		
	}
	
	
	public function getData(){
                $clean_javascript =  addslashes($this->value);
		$temp ="title: {
			text: '{$clean_javascript}' 
		},";
		return $temp;
	}
	
} 
<?php

namespace Dashboard\Model\Statics;


class PercentajeCalculation
{

	protected $numerador;
	protected $denominador;
	protected $decimals;
	protected $percentaje;


	public function __constructor($decimals=false)
	{
		$this->setDecimals($decimals);
	}

	public function setDecimals($decimals)
	{
		if(!$decimals)
		{
			$this->decimals = 2;
		}else{
			$this->decimals = $decimals;
		}

	}

	public function setReset()
	{
		$this->numerador = false;
		$this->denominador = false;
		$this->resetPercentaje();
	}

	private function resetPercentaje()
	{
		if($this->percentaje)
		{	
			$this->percentaje = false;
		}
	}

	public function setNumArray(array $temp)
	{
		if(empty($temp))
		{
			throw new exception("Array Empty");
		}

		$this->numerdor = $temp;
		$this->resetPercentaje();
	}


	public function setDenArray(array $temp)
	{
		if(empty($temp))
		{
			throw new exception("Array Empty");
		}
		
		$this->denominador = $temp;
		$this->resetPercentaje();
	}


	public function setNumerador($number)
	{
		if(!is_numeric($number))
		{
			throw new exception("Not a Number = {$number}");
		}

		$this->numerador[] = $number;
		$this->resetPercentaje();
	}

	public function setDenominador($number)
	{
		if(!is_numeric($number))
		{
			throw new exception("Not a Number = {$number}");
		}
		
		$this->denominador[] = $number;
		$this->resetPercentaje();
	}


	private function checkPercentaje()
	{

		$tempNum = $this->sumArray($this->numerador);
		$tempDen = $this->sumArray($this->denominador);

		if($tempDen<=0)
		{
			$tempRes = 0;

		}else{

			$tempRes = ($tempNum/$tempDen) * 100;
		}
		
		 $this->percentaje = number_format($tempRes,2);

	}


	private function sumArray($tempArray)
	{
		if(empty($tempArray))
		{
			return 0;
		}
		return array_sum($tempArray);
	}


	public function getPercentaje()
	{
		if(!$this->percentaje)
		{
			$this->checkPercentaje();
		}
		return $this->percentaje;
	}


}
<?php

namespace Dashboard\Model\Statics;



class RollingAverage
{

	protected $dataArray;
	protected $numberAverage;
	protected $arrayResult;
	protected $arrayNum;
	protected $f_date;
	protected $numElements;
	protected $rollingArray;

	public function __construct($data,$avg=4,$elements)
	{

		$this->setDataArray($data);
		$this->setNumberAverage($avg);
		$this->resetArray();

		$this->setNumElements($elements);

	}

	public function setArrayNum($num=false)
	{
		$this->arrayNum = $num;
	}

	public function setDataArray($data)
	{
		$this->dataArray = $data;
		$this->resetArray();
	}

 	public function setNumElements($temp)
 	{
 		$this->numElements = $temp;
 	}

	public function setNumberAverage($avg)
	{
		$this->numberAverage= $avg;
		$this->resetArray();
	}

	public function resetArray()
	{
		$this->arrayResult = false;
	}

	protected function generateArray()
	{
		$upTime 	= array();

		$downTime 	= array();
		$arrayResult = array();
		$i= 1;
		$j = 1;

		$arrayValue = array();

		foreach($this->dataArray as $value)
		{
			
			array_push($arrayValue, $value);

			if($i >= $this->numberAverage)
			{


				$tempSum = array_sum($arrayValue);
				array_shift($arrayValue);
				$valTemp = ($tempSum/$this->numberAverage);
				$valTemp= number_format($valTemp,2,'.','');
				$this->rollingArray[] = $valTemp;

			}
			$i++;
		}

		$this->normalizeArray();
	}


	protected function normalizeArray()
	{
		$arrayElements = count($this->rollingArray);
		$temp = $arrayElements -$this->numElements;

		$temp = abs($temp);
//	echo '<br><br>';
	//	print_r($temp);
	//	echo '<br><br>';

		if($arrayElements < $this->numElements)
		{
			//ArrayElements tiene menos elementos
		
			$this->addZeros($temp);
		}

		if($arrayElements > $this->numElements)
		{
			//ArrayElements tiene mas elementos
	
			$this->takeOutElements($temp);
		}


		if($arrayElements == $this->numElements)
		{
			//ArrayElements tiene mas elementos
		}


	}

	protected function takeOutElements($numElem)
	{
		for($i=0; $i<$numElem; $i++)
		{
			array_shift($this->rollingArray);
		}
	
	}

	protected function addZeros($numElem)
	{

		//print_r($numElem);
		$temp = array_reverse($this->rollingArray);
		for($i=0; $i<$numElem; $i++)
		{
			
			array_push($temp,0);
		}


		$this->rollingArray = array_reverse($temp);
	}


	public function getArrayResult()
	{
		if($this->arrayResult==false)
		{
			$this->arrayResult= $this->generateArray();
		}

		return $this->rollingArray;
	}


}
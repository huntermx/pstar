<?php

namespace Dashboard\Model\TimeClass;


class SegToHoursClass
{

   protected $seconds;
   protected $decimals;
   protected $hours;
   protected $seconds2;
   protected $hoursDec;


    public function __construct($seconds=false,$decimals=false)
    {
        if($seconds){
             $this->setSeconds($seconds);
         }
       

        if(!$decimals)
        {
            $this->decimals = 2;
        }else{
            $this->decimals = $decimals;
        }
        
    }

    public function setSeconds($seconds)
    {
         $this->seconds = $seconds;
         $this->hours = false;
         $this->seconds2 = false;
         $this->hoursDec = false;
    }
  

    private function splitArrays()
    {
        $temp = $this->seconds/3600;
        $tempDec = $temp -floor($temp);
        $segCalc = $tempDec * 60;

        $this->hoursDec = number_format($temp,$this->decimals,'.','');
        $this->hours = floor($temp);
        $this->seconds2 = $segCalc;


    }


    public function getHours()
    {
        if(!$this->hours)
        {
            $this->splitArrays();
        }
        return $this->hours;
    }

   public function getHoursDec()
    {
        if(!$this->hoursDec)
        {
            $this->splitArrays();
        }
        return $this->hoursDec;
    }

    public function getSeconds()
    {
        if(!$this->seconds2)
        {
            $this->splitArrays();
        }
        return $this->seconds2;
    }

}
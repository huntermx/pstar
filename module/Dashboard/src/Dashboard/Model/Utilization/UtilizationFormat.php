<?php

namespace Dashboard\Model\Utilization;

use Dashboard\Model\TimeClass\SegToHoursClass;
use Dashboard\Model\Statics\PercentajeCalculation;

class UtilizationFormat
{
   protected $objEfCap;
   protected $arrayDates=false;
   protected $arrayPercentaje=false;
   protected $arrayAvailHours=false;
   protected $arraySum = false;
   protected $arrayNumerador = false;
   protected $arrayDenominador = false;
   protected $objPercentaje;


    public function __construct($obj1=false)
    {
        if($obj1!=false)
        {
            $this->setObjEfCap($obj1);
        }
    }

    public function setObjEfCap($obj)
    {
         $this->objEfCap = $obj;
         $this->arrayDates = false;
         $this->arrayPercentaje = false;
         $this->arrayAvailHours = false;
         $this->arraySum = false;
         $this->arrayNumerador = false;
         $this->arrayDenominador = false;

    }
  
    private function checkInputData()
    {
        $rowNum = $this->objEfCap->count();
        if($rowNum == 0)
        {
            $this->resetToZero();
        }else{
            $this->splitArrays();
        }

    }

    private function resetToZero()
    {
        $this->arrayDates= array(0);
        $this->arrayAvailHours = array(0);
        $this->arraySum = 0;
    }


    private function splitArrays()
    {
        $objConvSegHours = new SegToHoursClass();
        $this->objPercentaje = new PercentajeCalculation(2);
        $objPercTemp = new PercentajeCalculation(2);

        while($row = $this->objEfCap->current())
        {
            if($row['nstatus']!= "Holiday")
            { 
                $tempDate = strtotime($row['fecha']);  
                $tempFDate = (string)date("d-M",$tempDate);
                $this->arrayDates[] = "'{$tempFDate}'";
                $objPercTemp->setReset();
            
                $schedRunTime = $row['total_uptime'] + $row['total_downtime'];
                $totalCapacity = $row['total_time'] * 3600;
                $objPercTemp->setNumerador($schedRunTime);
                $objPercTemp->setDenominador($totalCapacity);

                $this->objPercentaje->setNumerador($schedRunTime);
                $this->objPercentaje->setDenominador($totalCapacity);

                $this->arrayPercentaje[]  = $objPercTemp->getPercentaje();
                $this->arrayAvailHours[] =$objPercTemp->getPercentaje();
            }
        }

    }


    private function sumArrayData()
    {
        $this->splitArrays();
      
        $this->arraySum = $this->objPercentaje->getPercentaje();
    }


    public function getArrayDates()
    {
        if(!$this->arrayDates)
        {
            $this->checkInputData();
        }
        return $this->arrayDates;
    }

    public function getArrayAvailability()
    {
        if(!$this->arrayAvailHours)
        {
            $this->checkInputData();
        }
        return $this->arrayAvailHours;
    }

    public function getMonthSum()
    {
     
        $this->sumArrayData();
        
        return $this->arraySum;
    }


}